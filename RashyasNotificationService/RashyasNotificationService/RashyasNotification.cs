﻿using RashaysWebService.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace RashyasNotificationService
{
    partial class RashyasNotification : ServiceBase
    {
        public RashyasNotification()
        {
            InitializeComponent();
        }
        private System.Threading.Thread _thread;
        protected override void OnStart(string[] args)
        {
            try
            {
                // TODO: Add code here to start your service.
                _thread = new System.Threading.Thread(DoWork);

                // Start the thread.
                _thread.Start();
                EventLog.WriteEntry("Successful start.", EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(ex.ToString());
            }
        }
        public void DoWork()
        {
            try
            {
                SetTimer();
                timer2.Enabled = true;
            }
            catch (Exception ee)
            {
                EventLog.WriteEntry(ee.ToString());
            }
        }

        public void SetTimer()
        {
            // int msUntilTwelve = 60000;
            // int msUntilTwelve = 300000;
            int msUntilTwelve = 240000;
            timer2.Interval = msUntilTwelve;
        }
        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
        }

        private void timer2_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timer2.Enabled = false;
            getNotification();
            SetTimer();
            timer2.Enabled = true;
        }

        //public void getNotification()
        //{
        //    try
        //    {
        //        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["RashyasConnectionstring"]);
        //        SqlCommand cmd = new SqlCommand();
        //        DataTable dt = new DataTable();
        //        SqlDataAdapter da = new SqlDataAdapter(cmd);
        //        string query = "select * from Notification";
        //        cmd.CommandText = query;
        //        cmd.Connection = conn;
        //        cmd.CommandType = CommandType.Text;
        //        conn.Open();
        //        da.Fill(dt);
        //        conn.Close();
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            int minutes = Convert.ToInt32(dt.Rows[0]["minutes"]);
        //            int count = Convert.ToInt32(dt.Rows[0]["count"]);
        //            var a = SpGetData.GetDataFromSP("GetMinutewiseFeedback", new object[,] { { "minutes", "count" }, { minutes, count } });
        //            // if (Convert.ToBoolean(a.Rows[0]["Column1"]) == false)
        //            if (a.Rows.Count > count)
        //            {
        //                // sendmail("Rashyas Test", "testing for Rashyas");
        //                try
        //                {

        //                    string str = "";
        //                    str = "<table border=1 bordercolor=#000000 style=font-size: 10pt; font-family: Arial>";
        //                    //str += "<tr><td colspan=3>Store</td>";
        //                    //str += "<td colspan=4>" + Username + "</td></tr>";
        //                    str += "<tr><td>Store</td><td>Location</td><td>Question</td></tr>";
        //                    str += "<tr><td>" + Convert.ToString(a.Rows[0]["store"]) + "</td><td>" + Convert.ToString(a.Rows[0]["location"]) + "</td><td>" + Convert.ToString(a.Rows[0]["question"]) + "</td></tr>";
        //                    str += "</table>";
        //                    string server = System.Configuration.ConfigurationSettings.AppSettings["hostName"];
        //                    string port = System.Configuration.ConfigurationSettings.AppSettings["port"];
        //                    string username = System.Configuration.ConfigurationSettings.AppSettings["acctname"];
        //                    string pwd = System.Configuration.ConfigurationSettings.AppSettings["password"];
        //                    System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage("rlogicaltestmail@gmail.com", Convert.ToString(a.Rows[i]["email1"]));
        //                    // System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage("rlogicaltestmail@gmail.com", "rlogicaltestmail@gmail.com");
        //                    if (Convert.ToString(a.Rows[i]["email2"]) != "")
        //                    {
        //                        Email.CC.Add(Convert.ToString(a.Rows[i]["email2"]));
        //                    }
        //                    if (Convert.ToString(a.Rows[i]["email3"]) != "")
        //                    {
        //                        Email.CC.Add(Convert.ToString(a.Rows[i]["email3"]));
        //                    }
        //                    Email.Subject = "Rashyas Test";
        //                    Email.Body = str;
        //                    System.Net.Mail.SmtpClient mailClient = new System.Net.Mail.SmtpClient();
        //                    mailClient.EnableSsl = true;
        //                    mailClient.Host = server;
        //                    mailClient.Port = Convert.ToInt32(port);
        //                    Email.IsBodyHtml = true;
        //                    mailClient.UseDefaultCredentials = false;
        //                    System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential(username, pwd);
        //                    mailClient.Credentials = basicAuthenticationInfo;
        //                    try
        //                    {
        //                        mailClient.Send(Email);
        //                        Email = null;
        //                    }
        //                    catch (Exception ex1)
        //                    {
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                }
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        public void getNotification()
        {
            ProcessLog("getNotification", "start", "RashyasNotificationService");
            SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["RashyasConnectionstring"]);
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            string query = "select * from Notification";
            cmd.CommandText = query;
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            conn.Open();
            da.Fill(dt);
            conn.Close();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int minutes = Convert.ToInt32(dt.Rows[i]["minutes"]);
                int count = Convert.ToInt32(dt.Rows[i]["count"]);
                int sId = Convert.ToInt32(dt.Rows[i]["storeId"]);
                int lId = Convert.ToInt32(dt.Rows[i]["locationId"]);
                var a = SpGetData.GetDataFromSP("GetMinutewiseFeedback", new object[,] { { "minutes", "count", "sId", "lId" }, { minutes, count, sId, lId } });
                if (a.Rows.Count > count)
                {
                    //sendmail("Rashyas Test","testing for Rashyas");
                    SqlCommand cmd2 = new SqlCommand();
                    DataTable dt2 = new DataTable();
                    SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                    string query2 = "select * from feedBackMailSend where sid = " + a.Rows[i]["Id"] + " and lid = " + a.Rows[i]["lId"] + " and fid = " + a.Rows[i]["fId"] + " and qid = " + a.Rows[i]["questionid"] + "  and minutes = '" + minutes + "'and count = " + count + "";
                    cmd2.CommandText = query2;
                    cmd2.Connection = conn;
                    cmd2.CommandType = CommandType.Text;
                    conn.Open();
                    da2.Fill(dt2);
                    conn.Close();

                    if (dt2.Rows.Count <= 0)
                    {
                        ProcessLog("getNotification", "Insert feedBackMailSend", "RashyasNotificationService");
                        #region mailinsert
                        SqlCommand cmd1 = new SqlCommand();
                        SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                        string query1 = "Insert into feedBackMailSend(sId,lId,fId,qId,minutes,count) values (" + a.Rows[i]["Id"] + "," + a.Rows[i]["lId"] + "," + a.Rows[i]["fId"] + "," + a.Rows[i]["questionid"] + ",'" + minutes + "', " + count + ")";
                        cmd1.CommandText = query1;
                        cmd1.Connection = conn;
                        cmd1.CommandType = CommandType.Text;
                        conn.Open();
                        cmd1.ExecuteNonQuery();
                        conn.Close();
                        #endregion
                        try
                        {
                            string str = "";
                            str = "<table border=1 bordercolor=#000000 style=font-size: 10pt; font-family: Arial>";
                            //str += "<tr><td colspan=3>Store</td>";
                            //str += "<td colspan=4>" + Username + "</td></tr>";
                            str += "<tr><td>Store</td><td>Location</td><td>Question</td></tr>";
                            str += "<tr><td>" + Convert.ToString(a.Rows[0]["store"]) + "</td><td>" + Convert.ToString(a.Rows[0]["location"]) + "</td><td>" + Convert.ToString(a.Rows[0]["question"]) + "</td></tr>";
                            str += "</table>";
                            string server = System.Configuration.ConfigurationSettings.AppSettings["hostName"];
                            string port = System.Configuration.ConfigurationSettings.AppSettings["port"];
                            string username = System.Configuration.ConfigurationSettings.AppSettings["acctname"];
                            string pwd = System.Configuration.ConfigurationSettings.AppSettings["password"];
                            //System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage("rlogicaltestmail@gmail.com", "rlogicaltestmail@gmail.com");
                            System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage("noreply@rashays.com", Convert.ToString(a.Rows[0]["email1"]));
                            if (Convert.ToString(a.Rows[i]["email2"]) != "")
                            {
                                Email.CC.Add(Convert.ToString(a.Rows[i]["email2"]));
                            }
                            if (Convert.ToString(a.Rows[i]["email3"]) != "")
                            {
                                Email.CC.Add(Convert.ToString(a.Rows[i]["email3"]));
                            }
                            Email.Subject = "Rashyas Notification";
                            Email.Body = str;
                            System.Net.Mail.SmtpClient mailClient = new System.Net.Mail.SmtpClient();
                            mailClient.EnableSsl = true;
                            mailClient.Host = server;
                            mailClient.Port = Convert.ToInt32(port);
                            Email.IsBodyHtml = true;
                            mailClient.UseDefaultCredentials = false;
                            System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential(username, pwd);
                            mailClient.Credentials = basicAuthenticationInfo;
                            try
                            {
                                mailClient.Send(Email);
                                Email = null;
                            }
                            catch (Exception ex1)
                            {
                                ErrorLog("getNotification Email", ex1.ToString(), "RashyasNotificationService");
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorLog("getNotification dt", ex.ToString(), "RashyasNotificationService");
                        }
                    }
                }
                else
                {
                    SqlCommand cmd3 = new SqlCommand();
                    SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
                    string query3 = "Delete from feedBackMailSend where sid = " + sId + " and lid = " + lId + " and minutes = '" + minutes + "' and count = " + count + "";
                    cmd3.CommandText = query3;
                    cmd3.Connection = conn;
                    cmd3.CommandType = CommandType.Text;
                    conn.Open();
                    cmd3.ExecuteNonQuery();
                    conn.Close();
                }

            }

        }
        public void sendmail(string subject, string str1)
        {
            try
            {
                string server = System.Configuration.ConfigurationSettings.AppSettings["hostName"];
                string port = System.Configuration.ConfigurationSettings.AppSettings["port"];
                string username = System.Configuration.ConfigurationSettings.AppSettings["acctname"];
                string pwd = System.Configuration.ConfigurationSettings.AppSettings["password"];
                System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage("rlogicaltestmail@gmail.com", "rlogicaltestmail@gmail.com");
                Email.Subject = subject;
                Email.Body = str1;
                System.Net.Mail.SmtpClient mailClient = new System.Net.Mail.SmtpClient();
                mailClient.EnableSsl = true;
                mailClient.Host = server;
                mailClient.Port = Convert.ToInt32(port);
                Email.IsBodyHtml = true;
                mailClient.UseDefaultCredentials = false;
                System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential(username, pwd);
                mailClient.Credentials = basicAuthenticationInfo;
                try
                {
                    mailClient.Send(Email);
                    Email = null;
                }
                catch (Exception ex1)
                {
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static int ErrorLog(String MethodName, String ErrorMessage, String PageName)
        {
            SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["RashyasConnectionstring"]);
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    con.Open();
                    cmd.CommandText = "ErrorLogSp";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@MethodName", SqlDbType.VarChar, 100).Value = MethodName;
                    cmd.Parameters.Add("@pageName", SqlDbType.VarChar, 2000).Value = PageName;
                    cmd.Parameters.Add("@errorMessage", SqlDbType.VarChar, 2000).Value = ErrorMessage;
                    // cmd.Parameters.Add("@CodeSourceName", SqlDbType.VarChar, 50).Value = _sourceName;
                    // cmd.Parameters.Add("@opt", SqlDbType.Int, 1).Value = 1;
                    int _result = cmd.ExecuteNonQuery();
                    con.Close();
                    return _result;
                }
            }

        }
        public static int ProcessLog(String MethodName, String ProcessName, String PageName)
        {
            SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["RashyasConnectionstring"]);
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    con.Open();
                    cmd.CommandText = "processLogSp";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@MethodName", SqlDbType.VarChar, 100).Value = MethodName;
                    cmd.Parameters.Add("@ProcessName", SqlDbType.VarChar, 2000).Value = ProcessName;
                    cmd.Parameters.Add("@PageName", SqlDbType.VarChar, 2000).Value = PageName;
                    // cmd.Parameters.Add("@CodeSourceName", SqlDbType.VarChar, 50).Value = _sourceName;
                    // cmd.Parameters.Add("@opt", SqlDbType.Int, 1).Value = 1;
                    int _result = cmd.ExecuteNonQuery();
                    con.Close();
                    return _result;
                }
            }

        }
    }
}
