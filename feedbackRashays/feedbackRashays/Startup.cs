﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(feedbackRashays.Startup))]
namespace feedbackRashays
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
