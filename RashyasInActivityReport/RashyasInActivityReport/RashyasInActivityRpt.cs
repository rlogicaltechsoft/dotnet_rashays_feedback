﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace RashyasInActivityReport
{
    partial class RashyasInActivityRpt : ServiceBase
    {
        public RashyasInActivityRpt()
        {
            InitializeComponent();
        }
        private System.Threading.Thread _thread;
        protected override void OnStart(string[] args)
        {
            try
            {
                // TODO: Add code here to start your service.
                _thread = new System.Threading.Thread(DoWork);

                // Start the thread.
                _thread.Start();
                EventLog.WriteEntry("Successful start.", EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(ex.ToString());
            }
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
        }
        public void DoWork()
        {
            try
            {
                SetTimer();
                timer2.Enabled = true;
            }
            catch (Exception ee)
            {
                EventLog.WriteEntry(ee.ToString());
            }
        }

        public void SetTimer()
        {
            int msUntilTwelve = 50000;
            timer2.Interval = msUntilTwelve;
        }
        private void timer2_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timer2.Enabled = false;
            string time = DateTime.Now.ToString("hh:mm tt");
            if (time == "01:01 AM")
            {
                InactivityReport();
            }

            SetTimer();
            timer2.Enabled = true;
        }
        public void InactivityReport()
        {
            SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["RashyasConnectionstring"]);
            SqlCommand cmd = new SqlCommand();
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            string query = "select * from store";
            cmd.CommandText = query;
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            conn.Open();
            da.Fill(dt);
            conn.Close();
            ProcessLog("InactivityReport", "store", "RashyasInActivityReport");
            // DateTime Previousdate = DateTime.Now.AddDays(-1);
            //  DateTime todaydate = DateTime.Now;
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SqlCommand cmd2 = new SqlCommand();
                    DataTable dt2 = new DataTable();
                    SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                    string query2 = "select top 1 c.*, s.title, l.location from CustomerFeedback  as c inner join store as s on c.storeid = s.id inner join location as l on c.locationid = l.id where c.storeid = " + dt.Rows[i]["Id"] + "  and c.dateCreated = (select max(c.dateCreated) from CustomerFeedback as c where c.storeid = " + dt.Rows[i]["Id"] + ")";
                    cmd2.CommandText = query2;
                    cmd2.Connection = conn;
                    cmd2.CommandType = CommandType.Text;
                    conn.Open();
                    da2.Fill(dt2);
                    conn.Close();
                    DateTime datadate = Convert.ToDateTime(dt2.Rows[0]["dateCreated"]);
                    string date = datadate.ToString("yyyy/MM/dd 0:00:00");
                    DateTime parameterDate = Convert.ToDateTime(date);

                    DateTime Previousdate = DateTime.Now.AddDays(-1);
                    string Pdate = Previousdate.ToString("yyyy/MM/dd 0:00:00");
                    Previousdate = Convert.ToDateTime(Pdate);

                    //var todaysDate = DateTime.Today;
                    DateTime todaysDate = DateTime.Now.AddDays(1);
                    string Tdate = todaysDate.ToString("yyyy/MM/dd 0:00:00");
                    todaysDate = Convert.ToDateTime(Tdate);

                    string store = Convert.ToString(dt2.Rows[0]["title"]);
                    string location = Convert.ToString(dt2.Rows[0]["location"]);

                    if (dt2.Rows.Count > 0)
                    {
                        if (Previousdate == parameterDate)
                        {
                            if (parameterDate < todaysDate)
                            {
                                ProcessLog("InactivityReport", "parameterDate", "RashyasInActivityReport");
                                string str = "";
                                str += "Hello,</br> For below store and location there is a no any feddback from yesterday, please check. </br></br>";
                                str += "<table border=1 bordercolor=#000000 style=font-size: 10pt; font-family: Arial>";
                                //str += "<tr><td colspan=3>Store</td>";
                                //str += "<td colspan=4>" + Username + "</td></tr>";
                                str += "<tr><td>Store</td><td>Location</td></tr>";
                                str += "<tr><td>" + store + "</td><td>" + location + "</td></tr>";
                                str += "</table>";
                                var a = SpGetData.GetDataFromSP("InsertInactivityReport", new object[,] { { "store", "location" }, { store, location } });
                                sendmail("InactivityReport", str);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog("InactivityReport", ex.ToString(), "RashyasInActivityReport");
            }
        }
        public void sendmail(string subject, string str1)
        {
            try
            {
                string server = System.Configuration.ConfigurationSettings.AppSettings["hostName"];
                string port = System.Configuration.ConfigurationSettings.AppSettings["port"];
                string username = System.Configuration.ConfigurationSettings.AppSettings["acctname"];
                string pwd = System.Configuration.ConfigurationSettings.AppSettings["password"];
                //System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage("rlogicaltestmail@gmail.com", "feedbacksystem@rashays.com");
                System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage("noreply@rashays.com", "hanys@rashays.com");
                //System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage("noreply@rashays.com", "hilal@rashays.com");
                Email.Subject = subject;
                Email.Body = str1;
                System.Net.Mail.SmtpClient mailClient = new System.Net.Mail.SmtpClient();
                mailClient.EnableSsl = true;
                mailClient.Host = server;
                mailClient.Port = Convert.ToInt32(port);
                Email.IsBodyHtml = true;
                mailClient.UseDefaultCredentials = false;
                System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential(username, pwd);
                mailClient.Credentials = basicAuthenticationInfo;
                try
                {
                    mailClient.Send(Email);
                    Email = null;
                }
                catch (Exception ex1)
                {
                    ErrorLog("sendmail Email", ex1.ToString(), "RashyasInActivityReport");
                }
            }
            catch (Exception ex)
            {
                ErrorLog("sendmail", ex.ToString(), "RashyasInActivityReport");
            }
        }

        public static int ErrorLog(String MethodName, String ErrorMessage, String PageName)
        {
            SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["RashyasConnectionstring"]);
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    con.Open();
                    cmd.CommandText = "ErrorLogSp";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@MethodName", SqlDbType.VarChar, 100).Value = MethodName;
                    cmd.Parameters.Add("@pageName", SqlDbType.VarChar, 2000).Value = PageName;
                    cmd.Parameters.Add("@errorMessage", SqlDbType.VarChar, 2000).Value = ErrorMessage;
                    // cmd.Parameters.Add("@CodeSourceName", SqlDbType.VarChar, 50).Value = _sourceName;
                    // cmd.Parameters.Add("@opt", SqlDbType.Int, 1).Value = 1;
                    int _result = cmd.ExecuteNonQuery();
                    con.Close();
                    return _result;
                }
            }

        }
        public static int ProcessLog(String MethodName, String ProcessName, String PageName)
        {
            SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["RashyasConnectionstring"]);
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    con.Open();
                    cmd.CommandText = "processLogSp";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@MethodName", SqlDbType.VarChar, 100).Value = MethodName;
                    cmd.Parameters.Add("@ProcessName", SqlDbType.VarChar, 2000).Value = ProcessName;
                    cmd.Parameters.Add("@PageName", SqlDbType.VarChar, 2000).Value = PageName;
                    // cmd.Parameters.Add("@CodeSourceName", SqlDbType.VarChar, 50).Value = _sourceName;
                    // cmd.Parameters.Add("@opt", SqlDbType.Int, 1).Value = 1;
                    int _result = cmd.ExecuteNonQuery();
                    con.Close();
                    return _result;
                }
            }

        }
    }
}
