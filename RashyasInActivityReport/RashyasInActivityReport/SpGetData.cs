﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace RashyasInActivityReport
{
    public class SpGetData
    {
        public static DataTable GetDataFromSP(string StoredProcedureName, object[,] paramsArr)
        {
            var dt = new DataTable();
            dt.TableName = "Table1";
            SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["RashyasConnectionstring"]);
            // SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["RashyasConnectionstring"].ConnectionString);
            conn.Open();
            try
            {
                SqlCommand cmd = new SqlCommand(StoredProcedureName, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                for (int i = 0; i < paramsArr.Length / 2; i++)
                {
                    string paramName = "@" + paramsArr[0, i].ToString();
                    System.Type paramType = paramsArr[1, i].GetType();
                    cmd.Parameters.AddWithValue(paramName, Convert.ChangeType(paramsArr[1, i], paramType));
                }

                SqlDataAdapter myAdapter = new SqlDataAdapter(cmd);

                myAdapter.Fill(dt);
            }
            catch (Exception ex)
            {
                // var errorlog = SpGetData.GetDataFromSP("spInsertErrorLog", new object[,] { { "FunctionName", "ErrorDetail", "BidderId", "TaskId" }, { "SPGetData:File", Convert.ToString(ex.Message), StoredProcedureName, "" } });
            }
            finally
            {
                conn.Close();
            }

            return dt;
        }
    }
}
