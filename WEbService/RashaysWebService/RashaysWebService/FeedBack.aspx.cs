﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RashaysWebService.Model;
namespace RashaysWebService
{
    public partial class FeedBack : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                fillStore();
                //fillquestion();
            }
        }

        public void fillStore()
        {
            var a = SpGetData.GetDataFromSP("GetStoreListing", new object[,] { { }, { } });

            ddlStore1.DataSource = a;
            ddlStore1.DataTextField = "title";
            ddlStore1.DataValueField = "id";
            ddlStore1.DataBind();
            ddlStore1.Items.Insert(0, new ListItem("--Select Store--", "0"));
        }
        public void fillLocation(int storeId)
        {
            var a = SpGetData.GetDataFromSP("GetlocationListing", new object[,] { { "storeId" }, { storeId } });

            ddlLocation1.DataSource = a;
            ddlLocation1.DataTextField = "location";
            ddlLocation1.DataValueField = "id";
            ddlLocation1.DataBind();
            ddlLocation1.Items.Insert(0, new ListItem("--Select Location--", "0"));
        }

        public void bindGridview(int storeId, int locationId)
        {
            try
            {
                var a = SpGetData.GetDataFromSP("BindGridFeedback", new object[,] { { "storeId", "locationId" }, { storeId, locationId } });
                grdFeedBack.DataSource = a;
                grdFeedBack.DataBind();
            }
            catch (Exception ex)
            {

            }
        }
        //public void fillTimeSlot()
        //{
        //    var a = SpGetData.GetDataFromSP("BindGridTimeSlot", new object[,] { { }, { } });

        //    ddlTimeSlot1.DataSource = a;
        //    ddlTimeSlot1.DataTextField = "timeSlotName";
        //    ddlTimeSlot1.DataValueField = "id";
        //    ddlTimeSlot1.DataBind();
        //    ddlTimeSlot1.Items.Insert(0, new ListItem("--Select TimeSlot--", "0"));
        //}
        //public void fillquestion(int storeId, int LocationId)
        //{

        //    //int questionId = Convert.ToInt32(ddlQuestion.SelectedValue);

        //    var a = SpGetData.GetDataFromSP("BindQuestion", new object[,] { { "storeId", "LocationId" }, { storeId, LocationId} });

        //    ddlQuestion.DataSource = a;
        //    ddlQuestion.DataTextField = "question";
        //    ddlQuestion.DataValueField = "id";
        //    ddlQuestion.DataBind();
        //    ddlQuestion.Items.Insert(0, new ListItem("--Select Question--", "0"));
        //}
        protected void ddlStore1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int storeId = Convert.ToInt32(ddlStore1.SelectedValue);
            fillLocation(storeId);
        }

        //protected void ddlQuestion_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    int storeId = Convert.ToInt32(ddlStore1.SelectedValue);
        //    int locationId = Convert.ToInt32(ddlLocation1.SelectedValue);
        //   // int questionId = Convert.ToInt32(ddlQuestion.SelectedValue);
        //    bindGridview(storeId,locationId,questionId);
        //}

        protected void ddlLocation1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // int storeId = Convert.ToInt32(ddlStore1.SelectedValue);
            //int LocationId = Convert.ToInt32(ddlLocation1.SelectedValue);
            //fillquestion(storeId, LocationId);


            int storeId = Convert.ToInt32(ddlStore1.SelectedValue);
            int locationId = Convert.ToInt32(ddlLocation1.SelectedValue);
            // int questionId = Convert.ToInt32(ddlQuestion.SelectedValue);
            bindGridview(storeId, locationId);
        }

        protected void grdFeedBack_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdFeedBack.PageIndex = e.NewPageIndex;
            grdFeedBack.Page.DataBind();
            int storeId = Convert.ToInt32(ddlStore1.SelectedValue);
            int locationId = Convert.ToInt32(ddlLocation1.SelectedValue);
            bindGridview(storeId, locationId);
        }

       




    }
}