﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RashaysWebService.Model;
namespace RashaysWebService
{
    public partial class Location : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                fillStore();
                //BindLocation(1);
                btnCancel.Visible = false;
            }
        }
        public void fillStore()
        {
            var a = SpGetData.GetDataFromSP("GetStoreListing", new object[,] { { }, { } });
            ddlStore.DataSource = a;
            ddlStore.DataTextField = "title";
            ddlStore.DataValueField = "id";
            ddlStore.DataBind();
            ddlStore.Items.Insert(0, new ListItem("--Select Store--", "0"));

        }
        public void BindLocation(int storeId)
        {
            try
            {
                //int storeId = Convert.ToInt32(ddlStore.SelectedValue);
                var a = SpGetData.GetDataFromSP("BindGridLocation", new object[,] { { "storeId" }, { storeId } });
                grdLocation.DataSource = a;
                grdLocation.DataBind();

            }
            catch (Exception ex)
            {

            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (Convert.ToString(ViewState["id"]) == "")
                {
                    try
                    {
                        int storeId = Convert.ToInt32(ddlStore.SelectedValue);
                        string location = txtLocation.Text;
                        var a = SpGetData.GetDataFromSP("insertLocation", new object[,] { { "storeId", "location" }, { storeId, location } });
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "Loading", "javascript:saveMessage();", true);
                        BindLocation(storeId);
                        clearField();
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    try
                    {
                        int id = Convert.ToInt32(ViewState["id"]);
                        int storeId = Convert.ToInt32(ddlStore.SelectedValue);
                        string location = txtLocation.Text;

                        var a = SpGetData.GetDataFromSP("updateLocation", new object[,] { { "id", "storeId", "location" }, { id, storeId, location } });
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "Loading", "javascript:updateMessage();", true);
                        BindLocation(storeId);
                        clearField();
                        btnSave.Text = "Save";
                        ViewState["id"] = "";
                        btnCancel.Visible = false;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }
        public void clearField()
        {
            ddlStore.SelectedIndex = 0;
            txtLocation.Text = "";
        }
        protected void grdLocation_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int id = Convert.ToInt32(e.CommandArgument.ToString());
                ViewState["id"] = id;
                var a = SpGetData.GetDataFromSP("GetIdLocation", new object[,] { { "id" }, { id } });
                ddlStore.SelectedValue = Convert.ToString(a.Rows[0]["storeId"]);
                txtLocation.Text = Convert.ToString(a.Rows[0]["location"]);
                btnSave.Text = "Update";
                btnCancel.Visible = true;
            }
            else
            {
                //int storeId = Convert.ToInt32(ddlStore.SelectedValue);
                //int id = Convert.ToInt32(e.CommandArgument.ToString());
                //var a = SpGetData.GetDataFromSP("DeleteLocation", new object[,] { { "id" }, { id } });
                //BindLocation(storeId);
            }
        }

        protected void grdLocation_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void grdLocation_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void ddlStore_SelectedIndexChanged(object sender, EventArgs e)
        {
            int storeId = Convert.ToInt32(ddlStore.SelectedValue);
            BindLocation(storeId);
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            clearField();
            btnSave.Text = "Save";
            btnCancel.Visible = false;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int storeId = Convert.ToInt32(ddlStore.SelectedValue);
            int id = Convert.ToInt32(this.hdnId.Value);
            var a = SpGetData.GetDataFromSP("DeleteLocation", new object[,] { { "id" }, { id } });
            if (a.Rows.Count > 0)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "NoDelete()", true);
            }
            BindLocation(storeId);
        }
    }
}