﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RashaysWebService.Model;

namespace RashaysWebService
{
    public partial class AdminRoles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetUser();
            }
        }

        public void GetUser()
        {
            try
            {
                //int storeId = Convert.ToInt32(ddlStore.SelectedValue);
                var a = SpGetData.GetDataFromSP("BindGridUser", new object[,] { });
                grdUser.DataSource = a;
                grdUser.DataBind();

            }
            catch (Exception ex)
            {

            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (Convert.ToString(ViewState["id"]) == "")
                {
                    try
                    {
                        string userName = txtUsername.Text;
                        string Password = txtPassword.Text;
                        string Role = ddlRole.SelectedItem.Text;
                        var a = SpGetData.GetDataFromSP("InsertUser", new object[,] { { "userName", "Password", "Role" }, { userName, Password, Role } });
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "Loading", "javascript:saveMessage();", true);
                        GetUser();
                        clearField();
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    try
                    {
                        int id = Convert.ToInt32(ViewState["id"]);
                        string userName = txtUsername.Text;
                        string Password = txtPassword.Text;
                        string Role = ddlRole.SelectedItem.Text;
                        var a = SpGetData.GetDataFromSP("UpdateUser", new object[,] { { "id", "userName", "Password", "Role" }, { id, userName, Password, Role } });
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "Loading", "javascript:updateMessage();", true);
                        GetUser();
                        clearField();
                        btnSave.Text = "Save";
                        ViewState["id"] = "";
                        btnCancel.Visible = false;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }
        public void clearField()
        {
            txtUsername.Text = "";
            txtPassword.Text = "";
            ddlRole.SelectedItem.Text = "";
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            clearField();
            btnSave.Text = "Save";
            btnCancel.Visible = false;
        }

        protected void grdUser_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int id = Convert.ToInt32(e.CommandArgument.ToString());
                ViewState["id"] = id;
                var a = SpGetData.GetDataFromSP("GetIdUser", new object[,] { { "id" }, { id } });
                txtUsername.Text = Convert.ToString(a.Rows[0]["UserName"]);
                txtPassword.Text = Convert.ToString(a.Rows[0]["Password"]);
                ddlRole.SelectedItem.Text = Convert.ToString(a.Rows[0]["Role"]);
                btnSave.Text = "Update";
                btnCancel.Visible = true;
            }
        }

        protected void grdUser_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void grdUser_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(this.hdnId.Value);
            var a = SpGetData.GetDataFromSP("DeleteUSer", new object[,] { { "id" }, { id } });
            if (a.Rows.Count > 0)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "NoDelete()", true);
            }
            GetUser();
        }
    }
}