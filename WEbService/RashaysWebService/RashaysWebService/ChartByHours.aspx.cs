﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RashaysWebService.Model;


namespace RashaysWebService
{
    public partial class ChartByHours : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // fillLocation();
                // fillStore();
                //fillQuestion();
                //  Session["CountOfMax"] = 10;
            }
        }
        //[System.Web.Services.WebMethod]
        //public static List<store> fillStore()
        //{
        //    var a = SpGetData.GetDataFromSP("GetStoreListing", new object[,] { { }, { } });
        //    List<store> list = new List<store>();
        //    foreach (DataRow dr in a.Rows)
        //    {
        //        store objCount1 = new store();
        //        objCount1.title = Convert.ToString(dr[1].ToString());
        //        objCount1.id = Convert.ToInt32(dr[0].ToString());
        //        list.Add(objCount1);
        //        //var ab = SpGetData.GetDataFromSP("GetlocationChart", new object[,] { { "storeid"}, { objCount1.id } });
        //        //foreach( DataRow dr1 in ab.Rows)
        //        //{
        //        //    objCount1.location = Convert.ToString(dr1[1].ToString());
        //        //    objCount1.lid = Convert.ToInt32(dr1[0].ToString());
        //        //    list.Add(objCount1);
        //        //}

        //    }
        //    return list;
        //    //ddlStore.DataSource = a;
        //    //ddlStore.DataTextField = "title";
        //    //ddlStore.DataValueField = "id";
        //    //ddlStore.DataBind();
        //    //ddlStore.Items.Insert(0, new ListItem("--Select Store--", "0"));
        //    //ddlStore.Items.Insert(1, new ListItem("All", "-1"));
        //    //fillQuestion();

        //}

        [System.Web.Services.WebMethod]
        public static string fillStoreStr()
        {
            string returnString = "";
            var a = SpGetData.GetDataFromSP("GetStoreListing", new object[,] { { }, { } });
            returnString += string.Format("<option value = '{0}'>{1}</option>", "0", "--Select Store--");
            returnString += string.Format("<option value = '{0}'>{1}</option>", "-1", "All Store");
            foreach (DataRow dr in a.Rows)
            {
                returnString += string.Format("<option value = '{0}' data-flg = 's'>{1}</option>", Convert.ToInt32(dr[0].ToString()), Convert.ToString(dr[1].ToString()));
                store objCount1 = new store();
                objCount1.title = Convert.ToString(dr[1].ToString());
                objCount1.id = Convert.ToInt32(dr[0].ToString());

                var ab = SpGetData.GetDataFromSP("GetlocationChart", new object[,] { { "storeid" }, { objCount1.id } });
                foreach (DataRow dr1 in ab.Rows)
                {
                    returnString += string.Format("<option value = '{0}' data-flg = 'l'>{1}(location)</option>", Convert.ToInt32(dr1[0].ToString()), Convert.ToString(dr1[1].ToString()));
                }
            }
            return returnString;
        }


        //[System.Web.Services.WebMethod]
        //public static List<locations> fillLocation(int storeid)
        //{
        //    var a = SpGetData.GetDataFromSP("GetlocationChart", new object[,] { {"storeid" }, { storeid } });
        //    List<locations> list = new List<locations>();
        //    foreach (DataRow dr in a.Rows)
        //    {
        //        locations objCount1 = new locations();
        //        objCount1.location = Convert.ToString(dr[1].ToString());
        //        objCount1.id = Convert.ToInt32(dr[0].ToString());
        //        list.Add(objCount1);
        //    }
        //    return list;

        //}
        //public void fillLocation()
        //{
        //    var a = SpGetData.GetDataFromSP("GetlocationChart", new object[,] { { }, { } });
        //    ddlStore.DataSource = a;
        //    ddlStore.DataTextField = "location";
        //    ddlStore.DataValueField = "id";
        //    ddlStore.DataBind();
        //    ddlStore.Items.Insert(0, new ListItem("--Select Location--", "0"));
        //    ddlStore.Items.Insert(1, new ListItem("All", "-1"));
        //}
        //public void fillQuestion()
        //{
        //    var a = SpGetData.GetDataFromSP("getQuestionAll", new object[,] { { }, { } });
        //    ddlQuestionCmp.DataSource = a;
        //    ddlQuestionCmp.DataTextField = "Question";
        //    ddlQuestionCmp.DataValueField = "id";
        //    ddlQuestionCmp.DataBind();
        //    ddlQuestionCmp.Items.Insert(0, new ListItem("--Select Question--", "0"));
        //    //ddlQuestion.Items.Insert(1, new ListItem("All", "-1"));

        //}

        [System.Web.Services.WebMethod]
        public static string FillQuestionsComparison()
        {
            string returnString = "";
            var a = SpGetData.GetDataFromSP("BindQuestionComparison", new object[,] { { }, { } });
            returnString += string.Format("<option value = '{0}'>{1}</option>", "-1", "--Select Question--");

            foreach (DataRow dr in a.Rows)
            {
                returnString += string.Format("<option value = '{0}'>{1}</option>", 1, Convert.ToString(dr[0].ToString()));
                questions objCount1 = new questions();
                objCount1.question = Convert.ToString(dr[0].ToString());
                objCount1.id = 1;

            }
            return returnString;
        }

        [System.Web.Services.WebMethod]
        public static List<Count> GetComparisonData(string Question)
        {
            var b = SpGetData.GetDataFromSP("GetComparisonData", new object[,] { { "question" }, { Question } });
            List<Count> list = new List<Count>();
            List<Count> objcount = new List<Count>();

            for (int i = 0; i < b.Rows.Count; i++)
            {
                int QuestionId = Convert.ToInt32(b.Rows[i]["id"]);
                int StoreId = Convert.ToInt32(b.Rows[i]["storeid"]);

                var a = SpGetData.GetDataFromSP("GetComparisonChartData", new object[,] { { "QuestionId", "StoreId" }, { QuestionId, StoreId } });
                if (a.Rows.Count > 0)
                {
                    string[] columnNames = a.Columns.Cast<DataColumn>()
                                       .Select(x => x.ColumnName)
                                       .ToArray();
                    foreach (DataRow dr in a.Rows)
                    {
                        Count objCount1 = new Count();
                        objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                        objCount1.displayday = Convert.ToString(dr[1].ToString());
                        objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                        list.Add(objCount1);

                        DataRow[] drmain = a.Select("title='" + objCount1.displayday + "'");
                        objcount = list.Where(x => x.displayday == objCount1.displayday).ToList();
                        objcount.Count();
                        int listcount = objcount.Count();
                        DataRow[] dr1 = a.Select("title='" + objCount1.displayday + "' and feedback = '1'");
                        if (dr1.Length == 0)
                        {
                            object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                            if (obj != null)
                            {
                                if (drmain.Length == listcount)
                                {
                                    Count objCount0 = new Count();
                                    objCount0.displayCnt = 0;
                                    objCount0.displayday = Convert.ToString(dr[1].ToString());
                                    objCount0.feedback = 1;
                                    list.Add(objCount0);
                                }
                            }

                        }
                        DataRow[] dr2 = a.Select("title='" + objCount1.displayday + "' and feedback = '2'");
                        if (dr2.Length == 0)
                        {
                            object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                            if (obj != null)
                            {
                                //continue;
                                if (drmain.Length == listcount)
                                {
                                    Count objCount2 = new Count();
                                    objCount2.displayCnt = 0;
                                    objCount2.displayday = Convert.ToString(dr[1].ToString());
                                    objCount2.feedback = 2;
                                    list.Add(objCount2);
                                }
                            }

                        }
                        DataRow[] dr3 = a.Select("title='" + objCount1.displayday + "' and feedback = '3'");
                        if (dr3.Length == 0)
                        {
                            object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                            // if(objcnt)
                            if (obj != null)
                            {
                                //continue;
                                if (drmain.Length == listcount)
                                {
                                    Count objCount3 = new Count();
                                    objCount3.displayCnt = 0;
                                    objCount3.displayday = Convert.ToString(dr[1].ToString());
                                    objCount3.feedback = 3;
                                    list.Add(objCount3);
                                }
                            }


                        }

                    }
                }
            }
            return list;
        }

        [System.Web.Services.WebMethod]
        public static string FillQuestions(int storeid, string flag)
        {

            if (flag == "undefined")
            {
                flag = "A";
            }
            string returnString = "";
            var a = SpGetData.GetDataFromSP("BindQuestionchart", new object[,] { { "storeid", "flag" }, { storeid, flag } });
            returnString += string.Format("<option value = '{0}'>{1}</option>", "0", "--Select Question--");
            returnString += string.Format("<option value = '{0}'>{1}</option>", "-1", "All Question");
            foreach (DataRow dr in a.Rows)
            {
                returnString += string.Format("<option value = '{0}'>{1}</option>", Convert.ToInt32(dr[0].ToString()), Convert.ToString(dr[1].ToString()));
                questions objCount1 = new questions();
                objCount1.question = Convert.ToString(dr[1].ToString());
                objCount1.id = Convert.ToInt32(dr[0].ToString());

            }
            return returnString;
        }

        [System.Web.Services.WebMethod]
        public static string FillTime()
        {
            string returnString = "";
            returnString += string.Format("<option value = '{0}'>{1}</option>", "0", "--Select Time--");
            returnString += string.Format("<option value = '{0}'>{1}</option>", "1", "Today");
            returnString += string.Format("<option value = '{0}'>{1}</option>", "2", "Last Week");
            returnString += string.Format("<option value = '{0}'>{1}</option>", "3", "Last Month");
            returnString += string.Format("<option value = '{0}'>{1}</option>", "4", "Last Year");
            returnString += string.Format("<option value = '{0}'>{1}</option>", "5", "Choose Date");

            return returnString;
        }

        [System.Web.Services.WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static List<Count> GetcountData()
        {
            List<Count> list = new List<Count>();
            var a = SpGetData.GetDataFromSP("ChartGetCount", new object[,] { { }, { } });
            //DataTable dt = a;


            foreach (DataRow dr in a.Rows)
            {
                Count objCount1 = new Count();
                objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                objCount1.displayday = Convert.ToString(dr[1].ToString());
                objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                list.Add(objCount1);
            }



            //var happy = a.Select("feedBack = '1'");
            //int i = 1;
            //Count objCount = new Count();
            //foreach (DataRow dr in happy)
            //{
            //    objCount.displayCnthappy = Convert.ToInt32(dr[0].ToString());
            //}
            //list.Add(objCount);
            //i = 1;
            //var nuetral = a.Select("feedBack = '2'");

            //foreach (DataRow dr in nuetral)
            //{
            //    objCount.displayCntnuetral = Convert.ToInt32(dr[0].ToString());
            //}
            //list.Add(objCount);
            //i = 1;
            //var sad = a.Select("feedBack = '3'");

            //foreach (DataRow dr in sad)
            //{
            //    objCount.displayCntsad = Convert.ToInt32(dr[0].ToString());
            //}
            //list.Add(objCount);

            return list;
        }


        //protected void ddlStore_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    int sid = Convert.ToInt32(ddlStore.SelectedValue);
        //    GetcountData1(sid);
        //}

        // [System.Web.Services.WebMethod]
        [System.Web.Services.WebMethod(EnableSession = true)]
        //[ScriptMethod(UseHttpGet = true)]
        public static List<Count> GetcountDatastorewise(int storeid, string flag, int timeId, int questionId)
        {
            if (flag == "undefined")
            {
                flag = "s";
            }
            // string newid = storeid.Substring(0, storeid.Length - 1);
            // int storeid = Convert.ToInt32(newid);
            List<Count> list = new List<Count>();
            var a = SpGetData.GetDataFromSP("ChartGetCountStorewise", new object[,] { { "storeid", "flag", "timeId", "questionId" }, { storeid, flag, timeId, questionId } });
            List<Count> objcount = new List<Count>();
            string[] columnNames = a.Columns.Cast<DataColumn>()
                                      .Select(x => x.ColumnName)
                                      .ToArray();
            if (columnNames[1] == "displayhour")
            {
                foreach (DataRow dr in a.Rows)
                {
                    Count objCount1 = new Count();
                    objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                    objCount1.displayday = Convert.ToString(dr[1].ToString());
                    objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                    list.Add(objCount1);
                }
            }
            //else if (columnNames[1] == "displayyear")
            //{
            //    foreach (DataRow dr in a.Rows)
            //    {
            //        Count objCount1 = new Count();
            //        objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
            //        objCount1.displayday = Convert.ToString(dr[1].ToString());
            //        objCount1.feedback = Convert.ToInt32(dr[2].ToString());
            //        list.Add(objCount1);
            //    }
            //}
            else if (columnNames[1] == "displayyear")
            {
                foreach (DataRow dr in a.Rows)
                {
                    Count objCount1 = new Count();
                    objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                    objCount1.displayday = Convert.ToString(dr[1].ToString());
                    objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                    list.Add(objCount1);

                    DataRow[] drmain = a.Select("displayyear='" + objCount1.displayday + "'");
                    objcount = list.Where(x => x.displayday == objCount1.displayday).ToList();
                    objcount.Count();
                    int listcount = objcount.Count();
                    DataRow[] dr1 = a.Select("displayyear='" + objCount1.displayday + "' and feedback = '1'");
                    if (dr1.Length == 0)
                    {
                        object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                        if (obj != null)
                        {
                            // continue;
                            if (drmain.Length == listcount)
                            {
                                Count objCount0 = new Count();
                                objCount0.displayCnt = 0;
                                objCount0.displayday = Convert.ToString(dr[1].ToString());
                                objCount0.feedback = 1;
                                list.Add(objCount0);
                            }
                        }

                    }
                    DataRow[] dr2 = a.Select("displayyear='" + objCount1.displayday + "' and feedback = '2'");
                    if (dr2.Length == 0)
                    {
                        object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                        if (obj != null)
                        {
                            //continue;
                            if (drmain.Length == listcount)
                            {
                                Count objCount2 = new Count();
                                objCount2.displayCnt = 0;
                                objCount2.displayday = Convert.ToString(dr[1].ToString());
                                objCount2.feedback = 2;
                                list.Add(objCount2);
                            }
                        }

                    }
                    DataRow[] dr3 = a.Select("displayyear='" + objCount1.displayday + "' and feedback = '3'");
                    if (dr3.Length == 0)
                    {
                        object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                        // if(objcnt)
                        if (obj != null)
                        {
                            //continue;
                            if (drmain.Length == listcount)
                            {
                                Count objCount3 = new Count();
                                objCount3.displayCnt = 0;
                                objCount3.displayday = Convert.ToString(dr[1].ToString());
                                objCount3.feedback = 3;
                                list.Add(objCount3);
                            }
                        }


                    }
                }
            }
            else
            {
                if (timeId == 3)
                {
                    try
                    {
                        foreach (DataRow dr in a.Rows)
                        {
                            try
                            {
                                Count objCount1 = new Count();
                                objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                                string displayd = Convert.ToString(dr[3].ToString());
                                if (displayd.Contains(":"))
                                {
                                    displayd = displayd.Substring(0, displayd.IndexOf(":"));
                                    displayd = displayd.Substring(0, displayd.Length - 2).Trim();
                                }
                                objCount1.displayday = displayd;
                                objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                                list.Add(objCount1);

                                DataRow[] drmain = a.Select("displayday='" + objCount1.displayday + "'");
                                objcount = list.Where(x => x.displayday == objCount1.displayday).ToList();
                                objcount.Count();
                                int listcount = objcount.Count();
                                DataRow[] dr1 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '1'");
                                if (dr1.Length == 0)
                                {
                                    object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                                    if (obj != null)
                                    {
                                        // continue;
                                        if (drmain.Length == listcount)
                                        {
                                            Count objCount0 = new Count();
                                            objCount0.displayCnt = 0;
                                            string displayd1 = Convert.ToString(dr[3].ToString());
                                            if (displayd1.Contains(":"))
                                            {
                                                displayd1 = displayd1.Substring(0, displayd1.IndexOf(":"));
                                                displayd1 = displayd1.Substring(0, displayd1.Length - 2).Trim();
                                            }
                                            objCount0.displayday = displayd1;
                                            objCount0.feedback = 1;
                                            list.Add(objCount0);
                                        }
                                    }

                                }
                                DataRow[] dr2 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '2'");
                                if (dr2.Length == 0)
                                {
                                    object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                                    if (obj != null)
                                    {
                                        //continue;
                                        if (drmain.Length == listcount)
                                        {
                                            Count objCount2 = new Count();
                                            objCount2.displayCnt = 0;
                                            string displayd1 = Convert.ToString(dr[3].ToString());
                                            if (displayd1.Contains(":"))
                                            {
                                                displayd1 = displayd1.Substring(0, displayd1.IndexOf(":"));
                                                displayd1 = displayd1.Substring(0, displayd1.Length - 2).Trim();
                                            }
                                            objCount2.displayday = displayd1;
                                            objCount2.feedback = 2;
                                            list.Add(objCount2);
                                        }
                                    }

                                }
                                DataRow[] dr3 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '3'");
                                if (dr3.Length == 0)
                                {
                                    object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                                    // if(objcnt)
                                    if (obj != null)
                                    {
                                        //continue;
                                        if (drmain.Length == listcount)
                                        {
                                            Count objCount3 = new Count();
                                            objCount3.displayCnt = 0;
                                            string displayd1 = Convert.ToString(dr[3].ToString());
                                            if (displayd1.Contains(":"))
                                            {
                                                displayd1 = displayd1.Substring(0, displayd1.IndexOf(":"));
                                                displayd1 = displayd1.Substring(0, displayd1.Length - 2).Trim();
                                            }
                                            objCount3.displayday = displayd1;
                                            objCount3.feedback = 3;
                                            list.Add(objCount3);
                                        }
                                    }


                                }

                            }
                            catch (Exception)
                            {
                            }

                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    foreach (DataRow dr in a.Rows)
                    {
                        Count objCount1 = new Count();
                        objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                        objCount1.displayday = Convert.ToString(dr[1].ToString());
                        objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                        list.Add(objCount1);
                        //int index1 = list.FindIndex(f => f.feedback == 1);
                        //int index2 = list.FindIndex(f => f.feedback == 2);
                        //int index3 = list.FindIndex(f => f.feedback == 3);

                        DataRow[] drmain = a.Select("displayday='" + objCount1.displayday + "'");
                        objcount = list.Where(x => x.displayday == objCount1.displayday).ToList();
                        objcount.Count();
                        int listcount = objcount.Count();
                        DataRow[] dr1 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '1'");
                        if (dr1.Length == 0)
                        {
                            object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                            if (obj != null)
                            {
                                // continue;
                                if (drmain.Length == listcount)
                                {
                                    Count objCount0 = new Count();
                                    objCount0.displayCnt = 0;
                                    objCount0.displayday = Convert.ToString(dr[1].ToString());
                                    objCount0.feedback = 1;
                                    list.Add(objCount0);
                                }
                            }

                        }
                        DataRow[] dr2 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '2'");
                        if (dr2.Length == 0)
                        {
                            object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                            if (obj != null)
                            {
                                //continue;
                                if (drmain.Length == listcount)
                                {
                                    Count objCount2 = new Count();
                                    objCount2.displayCnt = 0;
                                    objCount2.displayday = Convert.ToString(dr[1].ToString());
                                    objCount2.feedback = 2;
                                    list.Add(objCount2);
                                }
                            }

                        }
                        DataRow[] dr3 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '3'");
                        if (dr3.Length == 0)
                        {
                            object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                            // if(objcnt)
                            if (obj != null)
                            {
                                //continue;
                                if (drmain.Length == listcount)
                                {
                                    Count objCount3 = new Count();
                                    objCount3.displayCnt = 0;
                                    objCount3.displayday = Convert.ToString(dr[1].ToString());
                                    objCount3.feedback = 3;
                                    list.Add(objCount3);
                                }
                            }


                        }

                    }
                }
            }
            //  HttpContext.Current.Session["CountOfMax"] = list.Count;

            return list;


        }


        [System.Web.Services.WebMethod]
        //[ScriptMethod(UseHttpGet = true)]
        public static List<Count> GetcountDataquestionwise(int questionId, int timeId, int storeId, string To, string From)
        {
            double days = 0.0;
            if (string.IsNullOrEmpty(To) == false)
            {
                //if (To != "")
                //{
                TimeSpan difference = Convert.ToDateTime(To) - Convert.ToDateTime(From);
                days = difference.TotalDays;
            }
            else
            {

                days = 0.0;
            }
            List<Count> list = new List<Count>();
            var a = SpGetData.GetDataFromSP("ChartGetCountQuestionwise", new object[,] { { "questionId", "timeId", "storeId", "days" }, { questionId, timeId, storeId, days } });
            //DataTable dt = a;

            if (a.Rows.Count > 0)
            {
                List<Count> objcount = new List<Count>();
                string[] columnNames = a.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToArray();
                //foreach (DataRow dr in a.Rows)
                //{
                //    Count objCount1 = new Count();
                //    objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                //    objCount1.displayday = Convert.ToString(dr[1].ToString());
                //    objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                //    list.Add(objCount1);
                //}
                if (columnNames[1] == "displayhour")
                {
                    foreach (DataRow dr in a.Rows)
                    {
                        Count objCount1 = new Count();
                        objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                        objCount1.displayday = Convert.ToString(dr[1].ToString());
                        objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                        list.Add(objCount1);
                    }
                }
                else if (columnNames[1] == "displayyear")
                {
                    foreach (DataRow dr in a.Rows)
                    {
                        Count objCount1 = new Count();
                        objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                        objCount1.displayday = Convert.ToString(dr[1].ToString());
                        objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                        list.Add(objCount1);

                        DataRow[] drmain = a.Select("displayyear='" + objCount1.displayday + "'");
                        objcount = list.Where(x => x.displayday == objCount1.displayday).ToList();
                        objcount.Count();
                        int listcount = objcount.Count();
                        DataRow[] dr1 = a.Select("displayyear='" + objCount1.displayday + "' and feedback = '1'");
                        if (dr1.Length == 0)
                        {
                            object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                            if (obj != null)
                            {
                                // continue;
                                if (drmain.Length == listcount)
                                {
                                    Count objCount0 = new Count();
                                    objCount0.displayCnt = 0;
                                    objCount0.displayday = Convert.ToString(dr[1].ToString());
                                    objCount0.feedback = 1;
                                    list.Add(objCount0);
                                }
                            }

                        }
                        DataRow[] dr2 = a.Select("displayyear='" + objCount1.displayday + "' and feedback = '2'");
                        if (dr2.Length == 0)
                        {
                            object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                            if (obj != null)
                            {
                                //continue;
                                if (drmain.Length == listcount)
                                {
                                    Count objCount2 = new Count();
                                    objCount2.displayCnt = 0;
                                    objCount2.displayday = Convert.ToString(dr[1].ToString());
                                    objCount2.feedback = 2;
                                    list.Add(objCount2);
                                }
                            }

                        }
                        DataRow[] dr3 = a.Select("displayyear='" + objCount1.displayday + "' and feedback = '3'");
                        if (dr3.Length == 0)
                        {
                            object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                            // if(objcnt)
                            if (obj != null)
                            {
                                //continue;
                                if (drmain.Length == listcount)
                                {
                                    Count objCount3 = new Count();
                                    objCount3.displayCnt = 0;
                                    objCount3.displayday = Convert.ToString(dr[1].ToString());
                                    objCount3.feedback = 3;
                                    list.Add(objCount3);
                                }
                            }


                        }
                    }
                }
                else
                {
                    foreach (DataRow dr in a.Rows)
                    {
                        Count objCount1 = new Count();
                        objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                        string displayd = Convert.ToString(dr[1].ToString());
                        string diplayd1 = displayd;
                        if (displayd.Contains(":"))
                        {
                            displayd = displayd.Substring(0, displayd.IndexOf(":"));
                            displayd = displayd.Substring(0, displayd.Length - 2).Trim();

                        }
                        objCount1.displayday = displayd;
                        objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                        list.Add(objCount1);

                        DataRow[] drmain = a.Select("displayday='" + diplayd1 + "'");
                        int listcount = 0;

                        objcount = list.Where(x => x.displayday == objCount1.displayday).ToList();
                        objcount.Count();
                        listcount = objcount.Count();

                        DataRow[] dr1 = a.Select("displayday='" + diplayd1 + "' and feedback = '1'");
                        if (dr1.Length == 0)
                        {
                            //SpGetData.GetDataFromSP("insertStore", new object[,] { { "title", "email1", "email2", "email3" }, { "test1", "kishan@rlogical.com", "", "" } });
                            object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                            if (obj != null)
                            {

                                // continue;
                                if (drmain.Length == listcount)
                                {

                                    Count objCount0 = new Count();
                                    objCount0.displayCnt = 0;
                                    string displayd1 = Convert.ToString(dr[1].ToString());
                                    if (displayd1.Contains(":"))
                                    {

                                        displayd1 = displayd1.Substring(0, displayd1.IndexOf(":"));
                                        displayd1 = displayd1.Substring(0, displayd1.Length - 2).Trim();
                                    }
                                    objCount0.displayday = displayd1;
                                    objCount0.feedback = 1;
                                    list.Add(objCount0);
                                }
                            }

                        }
                        DataRow[] dr2 = a.Select("displayday='" + diplayd1 + "' and feedback = '2'");
                        if (dr2.Length == 0)
                        {
                            object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                            if (obj != null)
                            {
                                //continue;
                                if (drmain.Length == listcount)
                                {
                                    Count objCount2 = new Count();
                                    objCount2.displayCnt = 0;
                                    string displayd1 = Convert.ToString(dr[1].ToString());
                                    if (displayd1.Contains(":"))
                                    {
                                        displayd1 = displayd1.Substring(0, displayd1.IndexOf(":"));
                                        displayd1 = displayd1.Substring(0, displayd1.Length - 2).Trim();
                                    }
                                    objCount2.displayday = displayd1;
                                    objCount2.feedback = 2;
                                    list.Add(objCount2);
                                }
                            }

                        }
                        DataRow[] dr3 = a.Select("displayday='" + diplayd1 + "' and feedback = '3'");
                        if (dr3.Length == 0)
                        {

                            object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                            // if(objcnt)
                            if (obj != null)
                            {
                                //continue;
                                if (drmain.Length == listcount)
                                {
                                    Count objCount3 = new Count();
                                    objCount3.displayCnt = 0;
                                    string displayd1 = Convert.ToString(dr[1].ToString());
                                    if (displayd1.Contains(":"))
                                    {
                                        displayd1 = displayd1.Substring(0, displayd1.IndexOf(":"));
                                        displayd1 = displayd1.Substring(0, displayd1.Length - 2).Trim();
                                    }
                                    objCount3.displayday = displayd1;
                                    objCount3.feedback = 3;
                                    list.Add(objCount3);
                                }
                            }


                        }

                    }

                }
            }
            return list;
        }
        [System.Web.Services.WebMethod]
        public static List<Count> GetcountDatatimewise(int timeId, int storeId, int questionId)
        {

            List<Count> list = new List<Count>();
            var a = SpGetData.GetDataFromSP("ChartGetCountTimewise", new object[,] { { "timeId", "storeId", "questionId" }, { timeId, storeId, questionId } });
            //DataTable dt = a;
            if (a.Rows.Count > 0)
            {
                List<Count> objcount = new List<Count>();
                string[] columnNames = a.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToArray();
                if (columnNames[1] == "displayhour")
                {
                    foreach (DataRow dr in a.Rows)
                    {
                        Count objCount1 = new Count();
                        objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                        objCount1.displayday = Convert.ToString(dr[1].ToString());
                        objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                        list.Add(objCount1);
                    }
                }
                else if (columnNames[1] == "displayyear")
                {
                    foreach (DataRow dr in a.Rows)
                    {
                        Count objCount1 = new Count();
                        objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                        objCount1.displayday = Convert.ToString(dr[1].ToString());
                        objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                        list.Add(objCount1);

                        DataRow[] drmain = a.Select("displayyear='" + objCount1.displayday + "'");
                        objcount = list.Where(x => x.displayday == objCount1.displayday).ToList();
                        objcount.Count();
                        int listcount = objcount.Count();
                        DataRow[] dr1 = a.Select("displayyear='" + objCount1.displayday + "' and feedback = '1'");
                        if (dr1.Length == 0)
                        {
                            object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                            if (obj != null)
                            {
                                // continue;
                                if (drmain.Length == listcount)
                                {
                                    Count objCount0 = new Count();
                                    objCount0.displayCnt = 0;
                                    objCount0.displayday = Convert.ToString(dr[1].ToString());
                                    objCount0.feedback = 1;
                                    list.Add(objCount0);
                                }
                            }

                        }
                        DataRow[] dr2 = a.Select("displayyear='" + objCount1.displayday + "' and feedback = '2'");
                        if (dr2.Length == 0)
                        {
                            object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                            if (obj != null)
                            {
                                //continue;
                                if (drmain.Length == listcount)
                                {
                                    Count objCount2 = new Count();
                                    objCount2.displayCnt = 0;
                                    objCount2.displayday = Convert.ToString(dr[1].ToString());
                                    objCount2.feedback = 2;
                                    list.Add(objCount2);
                                }
                            }

                        }
                        DataRow[] dr3 = a.Select("displayyear='" + objCount1.displayday + "' and feedback = '3'");
                        if (dr3.Length == 0)
                        {
                            object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                            // if(objcnt)
                            if (obj != null)
                            {
                                //continue;
                                if (drmain.Length == listcount)
                                {
                                    Count objCount3 = new Count();
                                    objCount3.displayCnt = 0;
                                    objCount3.displayday = Convert.ToString(dr[1].ToString());
                                    objCount3.feedback = 3;
                                    list.Add(objCount3);
                                }
                            }


                        }
                    }
                }
                else
                {
                    if (timeId == 3)
                    {
                        try
                        {
                            foreach (DataRow dr in a.Rows)
                            {
                                try
                                {
                                    Count objCount1 = new Count();
                                    objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                                    string displayd = Convert.ToString(dr[3].ToString());
                                    if (displayd.Contains(":"))
                                    {
                                        displayd = displayd.Substring(0, displayd.IndexOf(":"));
                                        displayd = displayd.Substring(0, displayd.Length - 2).Trim();
                                    }
                                    objCount1.displayday = displayd;
                                    objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                                    list.Add(objCount1);

                                    DataRow[] drmain = a.Select("displayday='" + objCount1.displayday + "'");
                                    objcount = list.Where(x => x.displayday == objCount1.displayday).ToList();
                                    objcount.Count();
                                    int listcount = objcount.Count();
                                    DataRow[] dr1 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '1'");
                                    if (dr1.Length == 0)
                                    {
                                        object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                                        if (obj != null)
                                        {
                                            // continue;
                                            if (drmain.Length == listcount)
                                            {
                                                Count objCount0 = new Count();
                                                objCount0.displayCnt = 0;
                                                string displayd1 = Convert.ToString(dr[3].ToString());
                                                if (displayd1.Contains(":"))
                                                {
                                                    displayd1 = displayd1.Substring(0, displayd1.IndexOf(":"));
                                                    displayd1 = displayd1.Substring(0, displayd1.Length - 2).Trim();
                                                }
                                                objCount0.displayday = displayd1;
                                                objCount0.feedback = 1;
                                                list.Add(objCount0);
                                            }
                                        }

                                    }
                                    DataRow[] dr2 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '2'");
                                    if (dr2.Length == 0)
                                    {
                                        object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                                        if (obj != null)
                                        {
                                            //continue;
                                            if (drmain.Length == listcount)
                                            {
                                                Count objCount2 = new Count();
                                                objCount2.displayCnt = 0;
                                                string displayd1 = Convert.ToString(dr[3].ToString());
                                                if (displayd1.Contains(":"))
                                                {
                                                    displayd1 = displayd1.Substring(0, displayd1.IndexOf(":"));
                                                    displayd1 = displayd1.Substring(0, displayd1.Length - 2).Trim();
                                                }
                                                objCount2.displayday = displayd1;
                                                objCount2.feedback = 2;
                                                list.Add(objCount2);
                                            }
                                        }

                                    }
                                    DataRow[] dr3 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '3'");
                                    if (dr3.Length == 0)
                                    {
                                        object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                                        // if(objcnt)
                                        if (obj != null)
                                        {
                                            //continue;
                                            if (drmain.Length == listcount)
                                            {
                                                Count objCount3 = new Count();
                                                objCount3.displayCnt = 0;
                                                string displayd1 = Convert.ToString(dr[3].ToString());
                                                if (displayd1.Contains(":"))
                                                {
                                                    displayd1 = displayd1.Substring(0, displayd1.IndexOf(":"));
                                                    displayd1 = displayd1.Substring(0, displayd1.Length - 2).Trim();
                                                }
                                                objCount3.displayday = displayd1;
                                                objCount3.feedback = 3;
                                                list.Add(objCount3);
                                            }
                                        }


                                    }

                                }
                                catch (Exception)
                                {
                                }

                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    else
                    {
                        foreach (DataRow dr in a.Rows)
                        {
                            Count objCount1 = new Count();
                            objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                            string displayd = Convert.ToString(dr[1].ToString());
                            if (displayd.Contains(":"))
                            {
                                displayd = displayd.Substring(0, displayd.IndexOf(":"));
                                displayd = displayd.Substring(0, displayd.Length - 1).Trim();
                            }
                            objCount1.displayday = displayd;
                            objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                            list.Add(objCount1);

                            DataRow[] drmain = a.Select("displayday='" + objCount1.displayday + "'");
                            objcount = list.Where(x => x.displayday == objCount1.displayday).ToList();
                            objcount.Count();
                            int listcount = objcount.Count();
                            DataRow[] dr1 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '1'");
                            if (dr1.Length == 0)
                            {
                                object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                                if (obj != null)
                                {
                                    // continue;
                                    if (drmain.Length == listcount)
                                    {
                                        Count objCount0 = new Count();
                                        objCount0.displayCnt = 0;
                                        string displayd1 = Convert.ToString(dr[1].ToString());
                                        if (displayd1.Contains(":"))
                                        {
                                            displayd1 = displayd1.Substring(0, displayd1.IndexOf(":"));
                                            displayd1 = displayd1.Substring(0, displayd1.Length - 1).Trim();
                                        }
                                        objCount0.displayday = displayd1;
                                        objCount0.feedback = 1;
                                        list.Add(objCount0);
                                    }
                                }

                            }
                            DataRow[] dr2 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '2'");
                            if (dr2.Length == 0)
                            {
                                object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                                if (obj != null)
                                {
                                    //continue;
                                    if (drmain.Length == listcount)
                                    {
                                        Count objCount2 = new Count();
                                        objCount2.displayCnt = 0;
                                        string displayd1 = Convert.ToString(dr[1].ToString());
                                        if (displayd1.Contains(":"))
                                        {
                                            displayd1 = displayd1.Substring(0, displayd1.IndexOf(":"));
                                            displayd1 = displayd1.Substring(0, displayd1.Length - 1).Trim();
                                        }
                                        objCount2.displayday = displayd1;
                                        objCount2.feedback = 2;
                                        list.Add(objCount2);
                                    }
                                }

                            }
                            DataRow[] dr3 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '3'");
                            if (dr3.Length == 0)
                            {
                                object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                                // if(objcnt)
                                if (obj != null)
                                {
                                    //continue;
                                    if (drmain.Length == listcount)
                                    {
                                        Count objCount3 = new Count();
                                        objCount3.displayCnt = 0;
                                        string displayd1 = Convert.ToString(dr[1].ToString());
                                        if (displayd1.Contains(":"))
                                        {
                                            displayd1 = displayd1.Substring(0, displayd1.IndexOf(":"));
                                            displayd1 = displayd1.Substring(0, displayd1.Length - 1).Trim();
                                        }
                                        objCount3.displayday = displayd1;
                                        objCount3.feedback = 3;
                                        list.Add(objCount3);
                                    }
                                }


                            }

                        }
                    }
                }
            }
            return list;
        }
        [System.Web.Services.WebMethod]
        public static List<Count> GetcountDatatimewiseChooseDate(int timeId, int storeId, int questionId, string To, string From)
        {
            DateTime dtto = Convert.ToDateTime(To);
            DateTime dttonew = dtto.AddDays(1);
            To = Convert.ToString(dttonew.ToString("yyyy-MM-dd"));
            List<Count> list = new List<Count>();
            var a = SpGetData.GetDataFromSP("ChartGetCountTimewiseChooseDate", new object[,] { { "timeId", "storeId", "questionId", "To", "From" }, { timeId, storeId, questionId, To, From } });
            //DataTable dt = a;
            if (a.Rows.Count > 0)
            {
                List<Count> objcount = new List<Count>();
                string[] columnNames = a.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToArray();
                foreach (DataRow dr in a.Rows)
                {
                    Count objCount1 = new Count();
                    objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                    objCount1.displayday = Convert.ToString(dr[1].ToString());
                    objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                    list.Add(objCount1);
                    //int index1 = list.FindIndex(f => f.feedback == 1);
                    //int index2 = list.FindIndex(f => f.feedback == 2);
                    //int index3 = list.FindIndex(f => f.feedback == 3);

                    DataRow[] drmain = a.Select("displayday='" + objCount1.displayday + "'");
                    objcount = list.Where(x => x.displayday == objCount1.displayday).ToList();
                    objcount.Count();
                    int listcount = objcount.Count();
                    DataRow[] dr1 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '1'");
                    if (dr1.Length == 0)
                    {
                        object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                        if (obj != null)
                        {
                            // continue;
                            if (drmain.Length == listcount)
                            {
                                Count objCount0 = new Count();
                                objCount0.displayCnt = 0;
                                objCount0.displayday = Convert.ToString(dr[1].ToString());
                                objCount0.feedback = 1;
                                list.Add(objCount0);
                            }
                        }

                    }
                    DataRow[] dr2 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '2'");
                    if (dr2.Length == 0)
                    {
                        object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                        if (obj != null)
                        {
                            //continue;
                            if (drmain.Length == listcount)
                            {
                                Count objCount2 = new Count();
                                objCount2.displayCnt = 0;
                                objCount2.displayday = Convert.ToString(dr[1].ToString());
                                objCount2.feedback = 2;
                                list.Add(objCount2);
                            }
                        }

                    }
                    DataRow[] dr3 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '3'");
                    if (dr3.Length == 0)
                    {
                        object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                        // if(objcnt)
                        if (obj != null)
                        {
                            //continue;
                            if (drmain.Length == listcount)
                            {
                                Count objCount3 = new Count();
                                objCount3.displayCnt = 0;
                                objCount3.displayday = Convert.ToString(dr[1].ToString());
                                objCount3.feedback = 3;
                                list.Add(objCount3);
                            }
                        }


                    }

                }
            }
            return list;
        }


        public class Count
        {
            public int displayCnt { get; set; }
            public string displayday { get; set; }
            public int feedback { get; set; }

        }
        public class store
        {
            public string title { get; set; }
            public int id { get; set; }
        }
        public class locations
        {
            public string location { get; set; }
            public int id { get; set; }
        }
        public class questions
        {
            public string question { get; set; }
            public int id { get; set; }
        }

    }
}
