﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Rashays.Master" AutoEventWireup="true" CodeBehind="Location.aspx.cs" Inherits="RashaysWebService.Location" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function validate() {

            var summary = "";
            summary += isvalidstore();
            summary += isvalidlocation();
            if (summary != "") {
                alert(summary);
                return false;
            }
            else {
                return true;
            }
        }
        function isvalidstore() {
            var result = document.getElementById('ContentPlaceHolder1_ddlStore').value;
            if (result == "0") {
                alert("Please Select Store");
            }
            else {
                //alert("Dropdownlist Selected Value is: " + result);
            }
        }
        function isvalidlocation() {
            var uid;
            var temp = document.getElementById("<%=txtLocation.ClientID %>");
            uid = temp.value;
            if (uid == "") {
                return ("Please Enter Location Name" + "\n");
            }
            else {
                return "";
            }
        }
        function DeleteAlert(objid) {
            $('#ContentPlaceHolder1_hdnId').val(objid);
            $('#modalAlert').modal('toggle');
            $('#modalAlert').modal('show');
            return false;
        }

        function NoDelete() {
            $('#NoDelete').modal('toggle');
            $('#NoDelete').modal('show');
        }
    </script>
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet" />
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- Chart.js -->
        <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
        <!-- gauge.js -->
        <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
        <!-- bootstrap-progressbar -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="col-md-6">
        <div class="x_panel">
            <div class="form-horizontal form-label-left">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Select Store
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:DropDownList ID="ddlStore" runat="server" CssClass="select2_single form-control" OnSelectedIndexChanged="ddlStore_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic" ErrorMessage="Please select store" ForeColor="Red" ValidationGroup="abc" ControlToValidate="ddlStore" EnableClientScript="false" InitialValue="0"></asp:RequiredFieldValidator>
                    </div>

                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Location 
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:TextBox ID="txtLocation" runat="server" class="form-control col-md-7 col-xs-12"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="Please enter location" ForeColor="Red" ValidationGroup="abc" ControlToValidate="txtLocation" EnableClientScript="false"></asp:RequiredFieldValidator>
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" ValidationGroup="abc" class="btn btn-primary" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" class="btn btn-primary" />
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="x_panel">
            <div class="x_content">
                <asp:GridView ID="grdLocation" runat="server" AutoGenerateColumns="false" OnRowCommand="grdLocation_RowCommand" OnRowDeleting="grdLocation_RowDeleting" OnRowEditing="grdLocation_RowEditing" GridLines="None" CssClass="table table-striped">
                    <Columns>
                        <asp:TemplateField HeaderText="id" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblId" runat="server" Text='<%# Eval("id")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Store">
                            <ItemTemplate>
                                <asp:Label ID="lbltitle" runat="server" Text='<%# Eval("title")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location">
                            <ItemTemplate>
                                <asp:Label ID="lbllocation" runat="server" Text='<%# Eval("location")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnedit" runat="server" CommandName="Edit" CommandArgument='<%# Eval("id") %>' CssClass="btn btn-primary">Edit</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-primary" data-toggle="modal" OnClientClick='<%# String.Format("javascript:return DeleteAlert(\"{0}\")", Eval("id").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>

    <div id="modalAlert" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Delete Confirmation</h4>
                </div>
                <div class="modal-body">
                    <h4>Are you sure want to Delete this location?</h4>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnDelete" runat="server" Text="Yes" OnClick="btnDelete_Click" class="btn btn-primary" />
                    <asp:Button ID="btnCancelDelete" runat="server" Text="Cancel" class="btn btn-primary" />
                </div>

            </div>
        </div>
    </div>
    <div id="NoDelete" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel1">Delete</h4>
                </div>
                <div class="modal-body">
                    <h4>this Location have already question, please delete question first, thanks.</h4>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="Button2" runat="server" Text="Ok" class="btn btn-primary" />
                </div>

            </div>
        </div>
    </div>
     
    <div>
        <asp:HiddenField ID="hdnId" runat="server" />
    </div>

</asp:Content>
