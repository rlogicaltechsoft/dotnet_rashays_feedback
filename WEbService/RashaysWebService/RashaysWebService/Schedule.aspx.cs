﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RashaysWebService.Model;
namespace RashaysWebService
{
    public partial class Schedule : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                fillStore();
                BindGridSchedule();
            }
        }

        public void fillStore()
        {
            var a = SpGetData.GetDataFromSP("GetStoreListing", new object[,] { { }, { } });
            ddlStore.DataSource = a;
            ddlStore.DataTextField = "title";
            ddlStore.DataValueField = "id";
            ddlStore.DataBind();
            //ddlStore.Items.Insert(0, new ListItem("--Select Store--", "0"));

        }

        public void BindGridSchedule()
        {
            try
            {
                var a = SpGetData.GetDataFromSP("GetScheduleData", new object[,] { { }, { } });
                grdSchedule.DataSource = a;
                grdSchedule.DataBind();

            }
            catch (Exception ex)
            {

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string EmailId = hdnEmailId.Value;
            if (Convert.ToString(ViewState["id"]) == "")
            {
                try
                {
                    int storeId = 0;
                    string frequency = Convert.ToString(ddlFrequency.SelectedItem.Text);
                    string Repeats = Convert.ToString(ddlRepeats.SelectedItem.Text);
                    string runTime = ddlFrom.SelectedItem.Text;
                    string ampm = ddlFromampm.SelectedItem.Text;
                    runTime = runTime + " " + ampm;
                    foreach (ListItem item in ddlStore.Items)
                    {
                        storeId = 0;
                        if (item.Selected)
                        {
                            storeId = Convert.ToInt32(item.Value);
                            var a = SpGetData.GetDataFromSP("insertSchedule", new object[,] { { "storeId", "frequency", "Repeats", "runTime", "EmailId" }, { storeId, frequency, Repeats, runTime, EmailId } });
                        }

                    }
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "Loading", "javascript:saveMessage();", true);
                    BindGridSchedule();
                    clearField();
                    fillStore();
                }
                catch (Exception ex)
                {

                }
            }
            else
            {
                try
                {
                    int id = Convert.ToInt32(ViewState["id"]);
                    int storeId = 0;
                    string frequency = Convert.ToString(ddlFrequency.SelectedItem.Text);
                    string Repeats = Convert.ToString(ddlRepeats.SelectedItem.Text);
                    string runTime = ddlFrom.SelectedItem.Text;
                    string ampm = ddlFromampm.SelectedItem.Text;
                    runTime = runTime + " " + ampm;
                    foreach (ListItem item in ddlStore.Items)
                    {
                        storeId = 0;
                        if (item.Selected)
                        {
                            storeId = Convert.ToInt32(item.Value);
                            var a = SpGetData.GetDataFromSP("updateSchedule", new object[,] { { "id", "storeId", "frequency", "Repeats", "runTime", "EmailId" }, { id, storeId, frequency, Repeats, runTime, EmailId } });
                        }
                    }
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "Loading", "javascript:updateMessage();", true);
                    BindGridSchedule();
                    clearField();
                    fillStore();
                    btnSave.Text = "Save";
                    ViewState["id"] = "";
                    btnCancel.Visible = false;
                }
                catch (Exception ex)
                {

                }
            }

        }
        public void clearField()
        {
            //ddlStore.SelectedIndex = 0;
            ddlStore.Items.Clear();
            ddlFrequency.SelectedValue = Convert.ToString(0);
            ddlRepeats.SelectedValue = Convert.ToString(0);
            ddlFrom.SelectedValue = Convert.ToString(0);
            ddlFromampm.SelectedValue = Convert.ToString(0);
        }
        protected void grdSchedule_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int id = Convert.ToInt32(e.CommandArgument.ToString());
                ViewState["id"] = id;
                var a = SpGetData.GetDataFromSP("GetIdSchedule", new object[,] { { "id" }, { id } });
                string EmaiId = Convert.ToString(a.Rows[0]["EmailId"]);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "displayEmailValue('" + EmaiId + "')", true);
                ddlStore.SelectedValue = Convert.ToString(a.Rows[0]["storeId"]);
              // ddlStore.Items.Add = Convert.ToString(a.Rows[0]["storeId"]);
          
                ddlFrequency.SelectedValue = returnIdOnValueFrequency(Convert.ToString(a.Rows[0]["frequency"]));
                ddlRepeats.SelectedValue = returnIdOnValueRepeat(Convert.ToString(a.Rows[0]["repeats"]));
                string timeFrom = Convert.ToString(a.Rows[0]["runTime"]);
                string fromAmpm = timeFrom.Substring(timeFrom.Length - 2);
                timeFrom = timeFrom.Remove(timeFrom.Length - 2).Trim();
                //txtEmail.Value  = Convert.ToString(a.Rows[0]["EmailId"]);
                ddlFrom.SelectedValue = returnIdOnValueTme(timeFrom);
                ddlFromampm.SelectedValue = returnIdOnValueAmPm(fromAmpm);

                btnSave.Text = "Update";
                btnCancel.Visible = true;
            }

        }

        protected void grdSchedule_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void grdSchedule_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            clearField();
            btnSave.Text = "Save";
            btnCancel.Visible = false;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(this.hdnId.Value);
            var a = SpGetData.GetDataFromSP("DeleteSchedule", new object[,] { { "id" }, { id } });
            BindGridSchedule();

        }
        protected void btnDelSelected_Click(object sender, EventArgs e)
        {
            grdSchedule.Columns[0].Visible = true;
            foreach (GridViewRow grow in grdSchedule.Rows)
            {
                
                CheckBox chkdel = (CheckBox)grow.FindControl("chkDel");
                if (chkdel.Checked)
                {
                    int id = Convert.ToInt32(grow.Cells[0].Text);
                    //int id = Convert.ToInt32(this.hdnId.Value);
                    var a = SpGetData.GetDataFromSP("DeleteSchedule", new object[,] { { "id" }, { id } });
                }
            }

            BindGridSchedule();
        }
        public string returnIdOnValueTme(string txtValue)
        {
            string returnString = "";

            switch (txtValue)
            {
                case "01:00":
                    return "1";
                    break;
                case "02:00":
                    return "2";
                    break;
                case "03:00":
                    return "3";
                    break;
                case "04:00":
                    return "4";
                    break;
                case "05:00":
                    return "5";
                    break;
                case "06:00":
                    return "6";
                    break;
                case "07:00":
                    return "7";
                    break;
                case "08:00":
                    return "8";
                    break;
                case "09:00":
                    return "9";
                    break;
                case "10:00":
                    return "10";
                    break;
                case "11:00":
                    return "11";
                    break;
                case "12:00":
                    return "12";
                    break;
                case "11:59":
                    return "12";
                    break;
                case "12:01":
                    return "12";
                    break;
            }

            return returnString;
        }

        public string returnIdOnValueAmPm(string txtValue)
        {
            string returnString = "";

            switch (txtValue)
            {
                case "AM":
                    return "1";
                    break;
                case "PM":
                    return "2";
                    break;

            }

            return returnString;
        }

        public string returnIdOnValueFrequency(string txtValue)
        {
            string returnString = "";

            switch (txtValue)
            {
                case "Daily":
                    return "1";
                    break;
                case "Weekly":
                    return "2";
                    break;
                case "Monthly":
                    return "3";
                    break;

            }

            return returnString;
        }
        public string returnIdOnValueRepeat(string txtValue)
        {
            string returnString = "";

            switch (txtValue)
            {
                case "Day":
                    return "1";
                    break;
                case "Week":
                    return "2";
                    break;
                case "Month":
                    return "3";
                    break;

            }

            return returnString;
        }


    }
}