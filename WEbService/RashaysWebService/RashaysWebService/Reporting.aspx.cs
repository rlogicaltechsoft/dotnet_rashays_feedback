﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RashaysWebService.Model;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;

namespace RashaysWebService
{
    public partial class Reporting : System.Web.UI.Page
    {
        DataTable a = new DataTable();
        DataTable fake = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                fillStore();
            }
        }
        public void fillStore()
        {
            var a = SpGetData.GetDataFromSP("GetStoreListing", new object[,] { { }, { } });

            ddlStore1.DataSource = a;
            ddlStore1.DataTextField = "title";
            ddlStore1.DataValueField = "id";
            ddlStore1.DataBind();
            ddlStore1.Items.Insert(0, new ListItem("--Select Store--", "0"));
            ddlStore1.Items.Insert(1, new ListItem("--All Store--", "-1"));
        }
        public void fillLocation(int storeId)
        {
            var a = SpGetData.GetDataFromSP("GetlocationListing", new object[,] { { "storeId" }, { storeId } });

            ddlLocation1.DataSource = a;
            ddlLocation1.DataTextField = "location";
            ddlLocation1.DataValueField = "id";
            ddlLocation1.DataBind();
            ddlLocation1.Items.Insert(0, new ListItem("--Select Location--", "0"));
            ddlLocation1.Items.Insert(1, new ListItem("--All Location--", "-1"));
        }

        private static List<Reportingcls> LoadData(int storeId, int locationId, int PageNumber, int flg)
        {

            List<Reportingcls> objlist = new List<Reportingcls>();
            string date = "", date1 = "";
            if (HttpContext.Current.Session["date"] != null)
            {
                date = Convert.ToString(HttpContext.Current.Session["date"]);
                date1 = Convert.ToString(HttpContext.Current.Session["date1"]);
            }
            DataTable objDt = null;
            if (flg == 0)
            {
                if (date == "" && date1 == "")
                {
                    objDt = SpGetData.GetDataFromSP("BindGridFeedbackReporting", new object[,] { { "storeId", "locationId", "date" }, { storeId, locationId, "" } });
                }
                else
                {
                    objDt = SpGetData.GetDataFromSP("BindGridFeedbackReporting", new object[,] { { "storeId", "locationId", "date", "date1" }, { storeId, locationId, date, date1 } });
                    HttpContext.Current.Session["date"] = null;
                    HttpContext.Current.Session["date1"] = null;
                }

            }
            else
            {
                PageNumber = (PageNumber - 1);
                if (date == "" && date1 == "")
                {
                    objDt = SpGetData.GetDataFromSP("BindGridFeedbackReportingFilter", new object[,] { { "storeId", "locationId", "date", "PageNumber", "RowspPage" }, { storeId, locationId, "", PageNumber, 10 } });
                }
                else
                {
                    objDt = SpGetData.GetDataFromSP("BindGridFeedbackReportingFilter", new object[,] { { "storeId", "locationId", "date", "date1", "PageNumber", "RowspPage" }, { storeId, locationId, date, date1, PageNumber, 10 } });
                    HttpContext.Current.Session["date"] = null;
                    HttpContext.Current.Session["date1"] = null;
                }
            }
            //DataTable objDt = SpGetData.GetDataFromSP("BindGridFeedbackReporting", new object[,] { { "storeId", "locationId", "date" }, { storeId, locationId, "" } });
            if (objDt.Rows.Count > 0)
            {
                //HttpContext.Current.Session["feedbackcount1"] = null;
                //HttpContext.Current.Session["feedbackcount2"] = null;
                //HttpContext.Current.Session["feedbackcount3"] = null;
                int numberOfRecords = objDt.Select("feedbackcount=1").Count<DataRow>();
                //HttpContext.Current.Session["feedbackcount1"] = Convert.ToString(numberOfRecords);

                int numberOfRecords1 = objDt.Select("feedbackcount=2").Count<DataRow>();
                //HttpContext.Current.Session["feedbackcount2"] = Convert.ToString(numberOfRecords1);

                int numberOfRecords2 = objDt.Select("feedbackcount=3").Count<DataRow>();
                //HttpContext.Current.Session["feedbackcount3"] = Convert.ToString(numberOfRecords2);
                foreach (DataRow dr1 in objDt.Rows)
                {
                    var student = new Reportingcls
                    {
                        id = Convert.ToInt32(dr1["id"]),
                        question = Convert.ToString(dr1["question"]),
                        feedBack = Convert.ToString(dr1["feedBack"]),
                        happy = Convert.ToString(dr1["happy"]),
                        neutral = Convert.ToString(dr1["neutral"]),
                        sad = Convert.ToString(dr1["sad"]),
                        title = Convert.ToString(dr1["title"]),
                        location = Convert.ToString(dr1["location"]),
                        timeSlotName = Convert.ToString(dr1["timeSlotName"]),
                        datecreated = Convert.ToDateTime(dr1["datecreated"].ToString() == "" ? "01-01-1900" : dr1["datecreated"].ToString()),
                        countOfFack = Convert.ToInt32(dr1["countOfFack"]),
                        ratio = numberOfRecords + " : " + numberOfRecords1 + " : " + numberOfRecords2
                    };
                    objlist.Add(student);
                }

            }
            //else
            //{
            //    HttpContext.Current.Session["feedbackcount1"] = "0";
            //    HttpContext.Current.Session["feedbackcount2"] = "0";
            //    HttpContext.Current.Session["feedbackcount3"] = "0";
            //}
            return objlist;
        }


        protected void ddlStore1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int storeId = Convert.ToInt32(ddlStore1.SelectedValue);
            fillLocation(storeId);
        }

        protected void ddlLocation1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int storeId = Convert.ToInt32(ddlStore1.SelectedValue);
            int locationId = Convert.ToInt32(ddlLocation1.SelectedValue);
            string date = Convert.ToString(Session["date"]);
            string date1 = Convert.ToString(Session["date1"]);
            //bindGridview(storeId, locationId, date, date1);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "Loading", "javascript:Reload();", true);
        }

        protected void grdFeedBack_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //grdFeedBack.PageIndex = e.NewPageIndex;
            //grdFeedBack.Page.DataBind();
            int storeId = Convert.ToInt32(ddlStore1.SelectedValue);
            int locationId = Convert.ToInt32(ddlLocation1.SelectedValue);
            string date = Convert.ToString(Session["date"]);
            string date1 = Convert.ToString(Session["date1"]);
            // bindGridview(storeId, locationId, date, date1);
        }
        protected void grdTable_PreRender(object sender, EventArgs e)
        {

        }

        protected void grdFeedBack_PreRender(object sender, EventArgs e)
        {
            //if (grdFeedBack.Rows.Count > 0)
            //{
            //    grdFeedBack.UseAccessibleHeader = true;
            //    grdFeedBack.HeaderRow.TableSection = TableRowSection.TableHeader;

            //}

        }

        protected void single_cal4_TextChanged(object sender, EventArgs e)
        {
            string date = single_cal4.Text;
            Session["date"] = date;
        }
        protected void single_cal5_TextChanged(object sender, EventArgs e)
        {
            string date1 = single_cal5.Text;
            Session["date1"] = date1;
        }

        protected void grdFeedBack_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string QId = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "id"));
                    DateTime date = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "datecreated"));
                    DataTable dtfake = fake;
                    //int sec = date.Second;
                    //sec = 60 - sec;

                    //date.AddSeconds(sec);
                    // DataRow[] dr = dtfake.Select("row > 4");
                    // DataTable tt = dtfake.Select("row > 4");

                    date = date.AddMinutes(1);
                    date = date.AddSeconds(-date.Second);
                    DataView view = new DataView(dtfake);
                    view.RowFilter = "rows > 4";
                    DataTable filteredData = view.ToTable();
                    DataRow d = filteredData.Select("questionId='" + QId + "' and datecreated = '" + date + "'").FirstOrDefault();
                    if (d != null)
                    {
                        e.Row.Cells[1].ForeColor = System.Drawing.Color.Red;
                        e.Row.Cells[2].ForeColor = System.Drawing.Color.Red;
                        e.Row.Cells[3].ForeColor = System.Drawing.Color.Red;
                        e.Row.Cells[4].ForeColor = System.Drawing.Color.Red;
                        e.Row.Cells[5].ForeColor = System.Drawing.Color.Red;
                        e.Row.Cells[6].ForeColor = System.Drawing.Color.Red;
                        e.Row.Cells[7].ForeColor = System.Drawing.Color.Red;
                        e.Row.Cells[8].ForeColor = System.Drawing.Color.Red;
                        e.Row.Cells[9].ForeColor = System.Drawing.Color.Red;
                    }
                }
            }
            catch (Exception ex)
            { }
        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetData(int storeId, int locationId, int length, int flg)
        {
            // Initialization.    
            DataTables result = new DataTables();
            try
            {


                // Initialization.    
                string search = HttpContext.Current.Request.Params["search[value]"];
                string draw = HttpContext.Current.Request.Params["draw"];
                string order = HttpContext.Current.Request.Params["order[0][column]"];
                //string orderDir = HttpContext.Current.Request.Params["order[0][dir]"];
                string orderDir = "desc";
                int startRec = Convert.ToInt32(HttpContext.Current.Request.Params["start"]);
                if (flg == 0)
                {
                    HttpContext.Current.Session["draw"] = draw;
                }
                int pageNumber = 0;
                //if (length == 10)
                //{
                pageNumber = Convert.ToInt32(HttpContext.Current.Request.Params["length"]);
                //}
                //else
                //{
                //    pageNumber = length;
                //}
                // Loading.    
                //DataTable objDt = SpGetData.GetDataFromSP("BindGridFeedbackReporting", new object[,] { { "storeId", "locationId", "date" }, { storeId, locationId, "" } });

                List<Reportingcls> data = LoadData(storeId, locationId, length, flg);
                // Total record count.    
                int totalRecords = data.Count;
                // Verification.    
                if (!string.IsNullOrEmpty(search) &&
                  !string.IsNullOrWhiteSpace(search))
                {
                    // Apply search    
                    data = data.Where(p => p.id.ToString().ToLower().Contains(search.ToLower()) ||
                                p.question.ToLower().Contains(search.ToLower()) ||
                                p.feedBack.ToString().ToLower().Contains(search.ToLower()) ||
                                p.happy.ToLower().Contains(search.ToLower()) ||
                                p.neutral.ToLower().Contains(search.ToLower()) ||
                                p.sad.ToString().ToLower().Contains(search.ToLower()) ||
                                p.title.ToString().ToLower().Contains(search.ToLower()) ||
                                 p.location.ToString().ToLower().Contains(search.ToLower()) ||
                                p.timeSlotName.ToString().ToLower().Contains(search.ToLower()) ||
                                p.datecreated.ToString().ToLower().Contains(search.ToLower()) ||
                                p.countOfFack.ToString().ToLower().Contains(search.ToLower()) ||
                                p.ratio.ToString().ToLower().Contains(search.ToLower())).ToList();
                }
                // Sorting.    
                if (order != "0")
                {
                    data = SortByColumnWithOrder(order, orderDir, data);
                }
                // Filter record count.    
                int recFilter = data.Count;
                // Apply pagination.    
                if (flg == 0)
                {
                    data = data.Skip(startRec).Take(pageNumber).ToList();
                }
                // Loading drop down lists.    
                result.draw = Convert.ToInt32(draw);
                result.recordsTotal = totalRecords;
                result.recordsFiltered = recFilter;
                result.data = data;
            }
            catch (Exception ex)
            {
                // Info    
                Console.Write(ex);
            }
            // Return info.    
            return result;
        }

        private static List<Reportingcls> SortByColumnWithOrder(string order, string orderDir, List<Reportingcls> data)
        {
            // Initialization.    
            List<Reportingcls> lst = new List<Reportingcls>();
            try
            {
                // Sorting    
                switch (order)
                {
                    case "0":
                        // Setting.    
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.id).ToList()
                                                             : data.OrderBy(p => p.id).ToList();
                        break;
                    case "1":
                        // Setting.    
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.question).ToList()
                                                             : data.OrderBy(p => p.question).ToList();
                        break;
                    case "2":
                        // Setting.    
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.feedBack).ToList()
                                                             : data.OrderBy(p => p.feedBack).ToList();
                        break;
                    case "3":
                        // Setting.    
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.happy).ToList()
                                                             : data.OrderBy(p => p.happy).ToList();
                        break;
                    case "4":
                        // Setting.    
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.neutral).ToList()
                                                              : data.OrderBy(p => p.neutral).ToList();
                        break;
                    case "5":
                        // Setting.    
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.sad).ToList()
                                                             : data.OrderBy(p => p.sad).ToList();
                        break;
                    case "6":
                        // Setting.    
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.title).ToList()
                                                             : data.OrderBy(p => p.title).ToList();
                        break;
                    case "7":
                        // Setting.    
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.location).ToList()
                                                             : data.OrderBy(p => p.location).ToList();
                        break;
                    case "8":
                        // Setting.    
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.timeSlotName).ToList()
                                                             : data.OrderBy(p => p.timeSlotName).ToList();
                        break;
                    case "9":
                        // Setting.    
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.datecreated).ToList()
                                                             : data.OrderBy(p => p.datecreated).ToList();
                        break;
                    default:
                        // Setting.    
                        lst = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? data.OrderByDescending(p => p.id).ToList()
                                                             : data.OrderBy(p => p.id).ToList();
                        break;
                }
            }
            catch (Exception ex)
            {
                // info.    
                Console.Write(ex);
            }
            // info.    
            return lst;
        }


    }
    public class Reportingcls
    {
        public int id { get; set; }
        public string question { get; set; }
        public string feedBack { get; set; }
        public string happy { get; set; }
        public string neutral { get; set; }
        public string sad { get; set; }
        public string title { get; set; }
        public string location { get; set; }
        public string timeSlotName { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public int countOfFack { get; set; }
        public string ratio { get; set; }

    }
}