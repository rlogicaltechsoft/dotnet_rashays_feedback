﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace RashaysWebService.Model
{
    public class Listing
    {
        public string title { get; set; }
        public string description { get; set; }
        public DateTime dateCreated { get; set; }
        public DateTime dateUpdated { get; set; }
        public Boolean isDeleted { get; set; }

       
    }
}