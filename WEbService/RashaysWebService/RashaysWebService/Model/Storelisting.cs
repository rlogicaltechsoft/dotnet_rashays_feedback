﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Newtonsoft.Json;

namespace RashaysWebService.Model
{
    public class Storelisting
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        //public DateTime dateCreated { get; set; }
        //public DateTime dateUpdated { get; set; }
        //public bool isDeleted { get; set; }
    }

    public class Feedback
    {
        public int id { get; set; }
        public string success { get; set; }
    }

    public class questions
    {
        public int storeId { get; set; }
        public int locationId { get; set; }
        public int questionId { get; set; }
        public string question { get; set; }
        public string timeSlotName { get; set; }
        public string happy { get; set; }
        public string neutral { get; set; }
        public string sad { get; set; }
        public string timeFrom { get; set; }
        public string timeTo { get; set; }


    }

    public class locations
    {
        public int storeId { get; set; }
        public int id { get; set; }
        public string location { get; set; }


    }

    //[DataContract]
    //public class jsonDataCollection
    //{
    //    [DataMember]
    //    public IEnumerable<jSonData> jsonData { get; set; }
    //}
    [DataContract]
    public class jSonData
    {
        [DataMember]
        public int storeId { get; set; }
        [DataMember]
        public int feedBack { get; set; }
        [DataMember]
        public int locationId { get; set; }
        [DataMember]
        public int questionId { get; set; }
        [DataMember]
        public string description { get; set; }


    }
    //[DataContract]
    //public class jSonStr
    //{
    //    [DataMember]
    //    public string jsonStr { get; set; }
    //}

    //[DataContract]
    //public class Message
    //{
    //    [DataMember]
    //    public string firstName { get; set; }
    //}

    public class memberData
    {
        public string success { get; set; }
    }
}
