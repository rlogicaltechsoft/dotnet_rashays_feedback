﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Rashays.Master" AutoEventWireup="true" CodeBehind="Schedule.aspx.cs" Inherits="RashaysWebService.Schedule" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #ContentPlaceHolder1_ddlFromampm {
            margin-left: 50px;
        }

        #ContentPlaceHolder1_ddlFrom {
            margin-left: -10px;
        }

        .btn-group > .btn:first-child {
            width: 438px !important;
        }

        .multiselect-selected-text {
            float: left;
            margin-left: 8px;
        }

        .caret {
            float: right;
            margin-top: 11px;
            margin-right: -8px;
        }

        .bootstrap-tagsinput {
            width: 438px;
        }
        #ContentPlaceHolder1_btndel{
            margin-left:15px;
        }

        .hiddenCol{
            display:none;
        }
    </style>

    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet" />
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="../build/css/custom.min.css" rel="stylesheet">

    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <link href="../src/bootstrap-tagsinput.css" rel="stylesheet" />
    <script src="../src/bootstrap-tagsinput.js"></script>

    <%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>--%>
    <%--    <link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css"
        rel="stylesheet" type="text/css" />--%>
    <%--<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>--%>
    <link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css"
        rel="stylesheet" type="text/css" />
    <%--    <script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"
        type="text/javascript"></script>--%>
    <script src="../src/multiselect.js"></script>
    <script type="text/javascript">   $(function () {
       $('[id*=ddlStore]').multiselect({
           includeSelectAllOption: true
       });
   });
    </script>

    <%--<script src="../src/jquery-1.12.4.js"></script>--%>
    <script type="text/javascript">
        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {

                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        inputList[i].checked = true;
                    }
                    else {
                        inputList[i].checked = false;
                    }
                }
            }
        }

        function Check_Click(objRef) {
            var row = objRef.parentNode.parentNode;
            var GridView = row.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                var headerCheckBox = inputList[0];
                var checked = true;
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                        break;
                    }
                }
            }
            headerCheckBox.checked = checked;

        }
    </script>
    <script type="text/javascript">

        function isvalidstore() {
            var result = document.getElementById('ContentPlaceHolder1_ddlStore').value;
            if (result == "0") {
                alert("Please Select Store");
            }
            else {
                //alert("Dropdownlist Selected Value is: " + result);
            }
        }
        function isvalidFrequency() {
            var result = document.getElementById('ContentPlaceHolder1_ddlFrequency').value;
            if (result == "0") {
                alert("Please Select Frequency");
            }
            else {
                //alert("Dropdownlist Selected Value is: " + result);
            }
        }
        function isvalidRepeats() {
            var result = document.getElementById('ContentPlaceHolder1_ddlRepeats').value;
            if (result == "0") {
                alert("Please Select Repeats");
            }
            else {
                //alert("Dropdownlist Selected Value is: " + result);
            }
        }

        function DeleteAlert(objid) {
            $('#ContentPlaceHolder1_hdnId').val(objid);
            $('#modalAlert').modal('toggle');
            $('#modalAlert').modal('show');
            return false;
        }

        function DeleteAllAlert() {
            $('#modalAlertAll').modal('toggle');
            $('#modalAlertAll').modal('show');
            return false;
        }

        function Getemail() {

            var emailid = $("#txtEmail").val();
            if (emailid != "") {
                $('#ContentPlaceHolder1_hdnEmailId').val(emailid);
            }
            else {
                return false;
            }
        }
        function displayEmailValue(emailid) {
            $("#txtEmail").val(emailid);
        }


    </script>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="col-md-12">
        <div class="x_panel">
            <div class="form-horizontal form-label-left">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Store
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <%--<asp:DropDownList ID="ddlStore" runat="server" CssClass="select2_single form-control"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic" ErrorMessage="Please select store" ForeColor="Red" ValidationGroup="validateschedule" ControlToValidate="ddlStore" EnableClientScript="false" InitialValue="0"></asp:RequiredFieldValidator>--%>
                        <asp:ListBox ID="ddlStore" runat="server" SelectionMode="Multiple" CssClass="select2_single form-control"></asp:ListBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic" ErrorMessage="Please select store" ForeColor="Red" ValidationGroup="validateschedule" ControlToValidate="ddlStore" InitialValue="0"></asp:RequiredFieldValidator>
                        <%--<asp:Button Text="Submit" runat="server"  />--%>
                    </div>

                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Frequency
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:DropDownList ID="ddlFrequency" runat="server" CssClass="select2_single form-control">
                            <asp:ListItem Text="--Select Frequency--" Value="0" />
                            <asp:ListItem Text="Daily" Value="1" />
                            <asp:ListItem Text="Weekly" Value="2" />
                            <asp:ListItem Text="Monthly" Value="3" />
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="Please select Frequency" ForeColor="Red" ValidationGroup="validateschedule" ControlToValidate="ddlFrequency" InitialValue="0"></asp:RequiredFieldValidator>
                    </div>

                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Repeats Every
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:DropDownList ID="ddlRepeats" runat="server" CssClass="select2_single form-control">
                            <asp:ListItem Text="--Select Repeats--" Value="0" />
                            <asp:ListItem Text="Day" Value="1" />
                            <asp:ListItem Text="Week" Value="2" />
                            <asp:ListItem Text="Month" Value="3" />
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic" ErrorMessage="Please select Repeats" ForeColor="Red" ValidationGroup="validateschedule" ControlToValidate="ddlRepeats" InitialValue="0"></asp:RequiredFieldValidator>
                    </div>

                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Email Send TO
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <%--<asp:TextBox ID="txtLocation" runat="server" class="form-control col-md-7 col-xs-12"></asp:TextBox>--%>
                        <input type="text" id="txtEmail" data-role="tagsinput" class="form-control col-md-7 col-xs-12" />
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic" ErrorMessage="Please enter Emailaddress" ForeColor="Red" ValidationGroup="validateschedule" ControlToValidate="txtEmail" EnableClientScript="false"></asp:RequiredFieldValidator>--%>
                    </div>

                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Time to Run
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <asp:DropDownList ID="ddlFrom" runat="server" Width="102px" CssClass="select2_single form-control">
                                <asp:ListItem Text="--Time--" Value="0" />
                                <asp:ListItem Text="01:00" Value="1" />
                                <asp:ListItem Text="02:00" Value="2" />
                                <asp:ListItem Text="03:00" Value="3" />
                                <asp:ListItem Text="04:00" Value="4" />
                                <asp:ListItem Text="05:00" Value="5" />
                                <asp:ListItem Text="06:00" Value="6" />
                                <asp:ListItem Text="07:00" Value="7" />
                                <asp:ListItem Text="08:00" Value="8" />
                                <asp:ListItem Text="09:00" Value="9" />
                                <asp:ListItem Text="10:00" Value="10" />
                                <asp:ListItem Text="11:00" Value="11" />
                                <asp:ListItem Text="12:00" Value="12" />
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic" ErrorMessage="Please select time" ForeColor="Red" ValidationGroup="validateschedule" ControlToValidate="ddlFrom" InitialValue="0"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <asp:DropDownList ID="ddlFromampm" runat="server" Width="102px" CssClass="select2_single form-control">
                                <asp:ListItem Text="--AMPM--" Value="0" />
                                <asp:ListItem Text="AM" Value="1" />
                                <asp:ListItem Text="PM" Value="2" />
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic" ErrorMessage="Please select am/pm" ForeColor="Red" ValidationGroup="validateschedule" ControlToValidate="ddlFromampm" InitialValue="0"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClientClick="Getemail();" OnClick="btnSave_Click" ValidationGroup="validateschedule" class="btn btn-primary" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" class="btn btn-primary" />
                    </div>
                </div>

            </div>
        </div>
    </div>
      <asp:Button ID="btndel" runat="server" Text="Delete All"  class="btn btn-primary"  data-toggle="modal" OnClientClick="return DeleteAllAlert();" />
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                <asp:GridView ID="grdSchedule" runat="server" AutoGenerateColumns="false" OnRowCommand="grdSchedule_RowCommand" OnRowDeleting="grdSchedule_RowDeleting" OnRowEditing="grdSchedule_RowEditing" GridLines="None" CssClass="table table-striped">
                    <Columns>
                         <asp:BoundField DataField="id" HeaderText="id" ItemStyle-CssClass="hiddenCol" HeaderStyle-CssClass="hiddenCol" />
                      <%--  <asp:TemplateField HeaderText="id">
                            <ItemTemplate>
                                <asp:Label ID="lblId" runat="server" Text='<%# Eval("id")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Roles">

                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" onclick="checkAll(this);" runat="server" />
                            </HeaderTemplate>
                              <ItemTemplate>
                                <asp:CheckBox ID="chkDel" runat="server" onclick="Check_Click(this)" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Store">
                            <ItemTemplate>
                                <asp:Label ID="lbltitle" runat="server" Text='<%# Eval("title")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Frequency">
                            <ItemTemplate>
                                <asp:Label ID="lblfrequency" runat="server" Text='<%# Eval("frequency")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Repeats">
                            <ItemTemplate>
                                <asp:Label ID="lblrepeats" runat="server" Text='<%# Eval("repeats")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Runtime">
                            <ItemTemplate>
                                <asp:Label ID="lblruntime" runat="server" Text='<%# Eval("runtime")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:Label ID="lblEmailid" runat="server" Text='<%# Eval("EmailId")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnedit" runat="server" CommandName="Edit" CommandArgument='<%# Eval("id") %>' CssClass="btn btn-primary">Edit</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-primary" data-toggle="modal" OnClientClick='<%# String.Format("javascript:return DeleteAlert(\"{0}\")", Eval("id").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>

    <div id="modalAlert" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Delete Confirmation</h4>
                </div>
                <div class="modal-body">
                    <h4>Are you sure want to Delete this Schedule?</h4>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnDelete" runat="server" Text="Yes" OnClick="btnDelete_Click" class="btn btn-primary" />
                    <asp:Button ID="btnCancelDelete" runat="server" Text="Cancel" class="btn btn-primary" />
                </div>

            </div>
        </div>
    </div>

        <div id="modalAlertAll" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabe1l">Delete Confirmation</h4>
                </div>
                <div class="modal-body">
                    <h4>Are you sure want to Delete selected Schedule?</h4>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnDelSelected" runat="server" Text="Yes" OnClick="btnDelSelected_Click" class="btn btn-primary" />
                    <asp:Button ID="btnCanceldelete1" runat="server" Text="Cancel" class="btn btn-primary" />
                </div>

            </div>
        </div>
    </div>

    <div>
        <asp:HiddenField ID="hdnId" runat="server" />
        <asp:HiddenField ID="hdnEmailId" runat="server" />
    </div>
</asp:Content>
