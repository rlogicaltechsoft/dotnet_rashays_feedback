﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RashaysWebService.Model;
namespace RashaysWebService
{
    public partial class Question : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //bindGridview();
                fillStore();
                fillStore1();
                //fillLocation();
                fillTimeSlot();
                btnCancel.Visible = false;

            }
        }

        public void bindGridview(int storeId, int locationId, int timeSlotId)
        {
            try
            {
                var a = SpGetData.GetDataFromSP("BindGridQuestion", new object[,] { { "storeId", "locationId", "timeSlotId" }, { storeId, locationId, timeSlotId } });
                if (a.Rows.Count > 0)
                {
                    grdQuestion.DataSource = a;
                    grdQuestion.DataBind();
                    lblfound.Visible = false;
                    grdQuestion.Visible = true;
                }
                else
                {
                    grdQuestion.Visible = false;
                    lblfound.Visible = true;
                    lblfound.InnerText = "No Records Found";
                }
            }
            catch (Exception ex)
            {

            }
            // SetSelectedRecord();
            // GetSelectedRecord();
        }

        public void bindGridviewAll(int storeId)
        {
            try
            {
                var a = SpGetData.GetDataFromSP("BindGridQuestionAll", new object[,] { { "storeId" }, { storeId } });
                if (a.Rows.Count > 0)
                {
                    grdQuestion.DataSource = a;
                    grdQuestion.DataBind();
                    lblfound.Visible = false;
                    grdQuestion.Visible = true;
                }
                else
                {
                    grdQuestion.Visible = false;
                    lblfound.Visible = true;
                    lblfound.InnerText = "No Records Found";
                }

            }
            catch (Exception ex)
            {

            }
            // SetSelectedRecord();
            // GetSelectedRecord();
        }
        public void fillStore()
        {
            var a = SpGetData.GetDataFromSP("GetStoreListing", new object[,] { { }, { } });
            ddlStore.DataSource = a;
            ddlStore.DataTextField = "title";
            ddlStore.DataValueField = "id";
            ddlStore.DataBind();
            ddlStore.Items.Insert(0, new ListItem("--Select Store--", "0"));


        }

        public void fillStore1()
        {
            var a = SpGetData.GetDataFromSP("GetStoreListing", new object[,] { { }, { } });
            ddlStore1.DataSource = a;
            ddlStore1.DataTextField = "title";
            ddlStore1.DataValueField = "id";
            ddlStore1.DataBind();
            ddlStore1.Items.Insert(0, new ListItem("--Select Store--", "0"));
            ddlStore1.Items.Insert(1, new ListItem("All", "-1"));
        }
        public void fillLocation(int storeId)
        {
            var a = SpGetData.GetDataFromSP("GetlocationListing", new object[,] { { "storeId" }, { storeId } });
            ddlLocation.DataSource = a;
            ddlLocation.DataTextField = "location";
            ddlLocation.DataValueField = "id";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("--Select Location--", "0"));
        }


        public void fillLocation1(int storeId)
        {
            var a = SpGetData.GetDataFromSP("GetlocationListing", new object[,] { { "storeId" }, { storeId } });
            ddlLocation1.DataSource = a;
            ddlLocation1.DataTextField = "location";
            ddlLocation1.DataValueField = "id";
            ddlLocation1.DataBind();
            //ddlLocation1.Items.Insert(0, new ListItem("--Select Location--", "0"));
            ddlLocation1.Items.Insert(0, new ListItem("All", "-1"));
        }
        public void fillTimeSlot()
        {
            var a = SpGetData.GetDataFromSP("BindGridTimeSlot", new object[,] { { }, { } });
            ddlTimeSlot.DataSource = a;
            ddlTimeSlot.DataTextField = "timeSlotName";
            ddlTimeSlot.DataValueField = "id";
            ddlTimeSlot.DataBind();
            ddlTimeSlot.Items.Insert(0, new ListItem("--Select TimeSlot--", "0"));

        }
        public void fillTimeSlot1()
        {
            var a = SpGetData.GetDataFromSP("BindGridTimeSlot", new object[,] { { }, { } });
            ddlTimeSlot1.DataSource = a;
            ddlTimeSlot1.DataTextField = "timeSlotName";
            ddlTimeSlot1.DataValueField = "id";
            ddlTimeSlot1.DataBind();
            //ddlTimeSlot1.Items.Insert(0, new ListItem("--Select TimeSlot--", "0"));
            ddlTimeSlot1.Items.Insert(0, new ListItem("All", "-1"));
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (Convert.ToString(ViewState["id"]) == "")
                {
                    try
                    {
                        int storeId = Convert.ToInt32(ddlStore.SelectedValue);
                        int locationId = Convert.ToInt32(ddlLocation.SelectedValue);
                        int timeSlotId = Convert.ToInt32(ddlTimeSlot.SelectedValue);
                        string question = txtQuestion.Text;
                        string happy = txtHappy.Text;
                        string neutral = txtNeutral.Text;
                        string sad = txtSad.Text;
                        var a = SpGetData.GetDataFromSP("insertquestion", new object[,] { { "question", "happy", "neutral", "sad", "storeId", "locationId", "timeSlotId" }, { question, happy, neutral, sad, storeId, locationId, timeSlotId } });
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "Loading", "javascript:saveMessage();", true);
                        //bindGridview(storeId,locationId);
                        clearField();
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    try
                    {
                        int id = Convert.ToInt32(ViewState["id"]);
                        int storeId = Convert.ToInt32(ddlStore.SelectedValue);
                        int locationId = Convert.ToInt32(ddlLocation.SelectedValue);
                        int timeSlotId = Convert.ToInt32(ddlTimeSlot.SelectedValue);
                        string question = txtQuestion.Text;
                        string happy = txtHappy.Text;
                        string neutral = txtNeutral.Text;
                        string sad = txtSad.Text;

                        var a = SpGetData.GetDataFromSP("updateQuestion", new object[,] { { "id", "question", "happy", "neutral", "sad", "storeId", "locationId", "timeSlotId" }, { id, question, happy, neutral, sad, storeId, locationId, timeSlotId } });
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "Loading", "javascript:updateMessage();", true);

                        clearField();
                        btnSave.Text = "Save";
                        ViewState["id"] = "";
                        bindGridview(storeId, locationId, timeSlotId);
                        btnCancel.Visible = false;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        public void clearField()
        {
            txtQuestion.Text = "";
            txtHappy.Text = "";
            txtNeutral.Text = "";
            txtSad.Text = "";
            ddlStore.SelectedIndex = 0;
            ddlLocation.SelectedIndex = 0;
            //ddlStore1.SelectedIndex = 0;
            //ddlLocation1.SelectedIndex = 0;
            //ddlTimeSlot1.SelectedIndex = 0;
            ddlTimeSlot.SelectedIndex = 0;
            lblFrom.Text = "";
            lblTo.Text = "";
        }

        protected void grdQuestion_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "Edit")
            {
                int id = Convert.ToInt32(e.CommandArgument.ToString());
                ViewState["id"] = id;
                var a = SpGetData.GetDataFromSP("GetIdQuestion", new object[,] { { "id" }, { id } });

                txtQuestion.Text = Convert.ToString(a.Rows[0]["question"]);
                txtHappy.Text = Convert.ToString(a.Rows[0]["happy"]);
                txtNeutral.Text = Convert.ToString(a.Rows[0]["neutral"]);
                txtSad.Text = Convert.ToString(a.Rows[0]["sad"]);
                ddlStore.SelectedValue = Convert.ToString(a.Rows[0]["storeid"]);
                fillLocation(Convert.ToInt32(ddlStore.SelectedValue));
                ddlLocation.SelectedValue = Convert.ToString(a.Rows[0]["locationid"]);
                ddlTimeSlot.SelectedValue = Convert.ToString(a.Rows[0]["timeSlotId"]);
                lblFrom.Visible = true;
                lblTo.Visible = true;
                lblFrom.Text = Convert.ToString(a.Rows[0]["timeFrom"]);
                lblTo.Text = Convert.ToString(a.Rows[0]["timeTo"]);
                btnSave.Text = "Update";
                btnCancel.Visible = true;
            }
            else
            {
                //string[] arg = new string[3];
                //arg = e.CommandArgument.ToString().Split(';');
                //int id = Convert.ToInt32(arg[0]);
                //int storeId = Convert.ToInt32(arg[1]);
                //int locationId = Convert.ToInt32(arg[2]);
                //int timeSlotId = Convert.ToInt32(arg[3]);
                ////  int id = Convert.ToInt32(e.CommandArgument.ToString());
                //var a = SpGetData.GetDataFromSP("DeleteQuestion", new object[,] { { "id" }, { id } });
                //bindGridview(storeId, locationId, timeSlotId);
            }
        }

        protected void grdQuestion_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void grdQuestion_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            //int i = 0;
            ////string sids = "";
            //int sId = 0;
            //int lId = 0;
            //int tId = 0;
            //foreach (GridViewRow row in grdQuestion.Rows)
            //{
            //    var dt = SpGetData.GetDataFromSP("BindGridQuestionCheckbox", new object[,] { { }, { } });
            //    int id = Convert.ToInt32(dt.Rows[i]["id"]);
            //    sId = Convert.ToInt32(dt.Rows[i]["sid"]);
            //    lId = Convert.ToInt32(dt.Rows[i]["lid"]);
            //    tId = Convert.ToInt32(dt.Rows[i]["tid"]);
            //    CheckBox chk = (row.Cells[i].FindControl("chkActive") as CheckBox);


            //    //  if (chk != null && chk.Checked)
            //    // {
            //    //if (!sids.Contains(Convert.ToString(sId)))
            //    //{
            //    var a = SpGetData.GetDataFromSP("updateActivequestion", new object[,] { { "id", "checked", "sid","lid","tid" }, { id, chk.Checked, sId,lId,tId } });
            //    //}
            //    // sids += sid + ",";
            //    //  }

            //    i++;
            //}
            //bindGridview(sId,lId,tId);

        }

        protected void ddlLocation2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int storeId = Convert.ToInt32(ddlStore1.SelectedValue);
            //int locatinId = Convert.ToInt32(ddlLocation1.SelectedValue);
            //bindGridview(storeId, locatinId);
            //clearField();
            int storeId = Convert.ToInt32(ddlStore1.SelectedValue);
            int locatinId = Convert.ToInt32(ddlLocation1.SelectedValue);
            int timeSlotId = Convert.ToInt32(ddlTimeSlot1.SelectedValue);
            bindGridview(storeId, locatinId, timeSlotId);
        }

        protected void ddlTimeSlot_SelectedIndexChanged(object sender, EventArgs e)
        {
            int tid = Convert.ToInt32(ddlTimeSlot.SelectedValue);
            var a = SpGetData.GetDataFromSP("GetIdTimeslot", new object[,] { { "id" }, { tid } });
            if (a.Rows.Count > 0)
            {
                lblFrom.Visible = true;
                lblTo.Visible = true;

                lblFrom.Text = Convert.ToString(a.Rows[0]["timeFrom"]);
                lblTo.Text = Convert.ToString(a.Rows[0]["timeTo"]);
            }
            else
            {
                lblFrom.Visible = false;
                lblTo.Visible = false;
            }
        }

        protected void ddlTimeSlot1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int storeId = Convert.ToInt32(ddlStore1.SelectedValue);
            int locatinId = Convert.ToInt32(ddlLocation1.SelectedValue);
            int timeSlotId = Convert.ToInt32(ddlTimeSlot1.SelectedValue);
            bindGridview(storeId, locatinId, timeSlotId);
            clearField();
            SetSelectedRecord();

        }

        //protected void rbActive_CheckedChanged(object sender, EventArgs e)
        //{
        //    int sId = 0;
        //    int lId = 0;
        //    int tId = 0;
        //    for (int i = 0; i < grdQuestion.Rows.Count; i++)
        //    {
        //        RadioButton rb = (RadioButton)grdQuestion.Rows[i].Cells[10].FindControl("rbActive");
        //        var dt = SpGetData.GetDataFromSP("BindGridQuestionCheckbox", new object[,] { { }, { } });
        //        int id = Convert.ToInt32(dt.Rows[i]["id"]);
        //        sId = Convert.ToInt32(dt.Rows[i]["sid"]);
        //        lId = Convert.ToInt32(dt.Rows[i]["lid"]);
        //        tId = Convert.ToInt32(dt.Rows[i]["tid"]);
        //        if (rb.Checked == true)
        //        {
        //            var a = SpGetData.GetDataFromSP("updateActivequestion", new object[,] { { "id", "checked", "sid", "lid", "tid" }, { id, rb.Checked, sId, lId, tId } });
        //        }
        //        else
        //        {
        //            var a = SpGetData.GetDataFromSP("updateActivequestion", new object[,] { { "id", "checked", "sid", "lid", "tid" }, { id, rb.Checked, sId, lId, tId } });
        //        }
        //    }
        //    bindGridview(sId, lId, tId);
        //}




        private void GetSelectedRecord()
        {
            for (int i = 0; i < grdQuestion.Rows.Count; i++)
            {
                RadioButton rb = (RadioButton)grdQuestion.Rows[i]
                                .Cells[10].FindControl("RadioButton1");
                if (rb != null)
                {
                    if (rb.Checked)
                    {
                        HiddenField hf = (HiddenField)grdQuestion.Rows[i]
                                        .Cells[10].FindControl("HiddenField1");
                        if (hf != null)
                        {
                            ViewState["SelectedContact"] = hf.Value;
                        }
                        break;
                    }
                }
            }
        }

        private void SetSelectedRecord()
        {
            for (int i = 0; i < grdQuestion.Rows.Count; i++)
            {
                RadioButton rb = (RadioButton)grdQuestion.Rows[i].Cells[10]
                                                .FindControl("RadioButton1");
                if (rb.Checked == true)
                {
                    HiddenField hf = (HiddenField)grdQuestion.Rows[i]
                                        .Cells[10].FindControl("HiddenField1");
                    if (hf != null && ViewState["SelectedContact"] != null)
                    {
                        if (hf.Value.Equals(ViewState["SelectedContact"].ToString()))
                        {
                            rb.Checked = true;
                            break;
                        }
                    }
                }
            }
        }

        protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {

            int sId = 0;
            int lId = 0;
            int tId = 0;
            for (int i = 0; i < grdQuestion.Rows.Count; i++)
            {
                RadioButton rb = (RadioButton)grdQuestion.Rows[i].Cells[10]
                                                .FindControl("RadioButton1");
                HiddenField hf = (HiddenField)grdQuestion.Rows[i]
                                        .Cells[10].FindControl("HiddenField1");
                int id = Convert.ToInt32(hf.Value);
                if (rb.Checked == true)
                {

                    // if (hf != null && ViewState["SelectedContact"] != null)
                    //{
                    // if (hf.Value.Equals(ViewState["SelectedContact"].ToString()))
                    //{
                    rb.Checked = true;
                    var dt = SpGetData.GetDataFromSP("BindGridQuestionCheckbox", new object[,] { { "id" }, { id } });
                    //int id = Convert.ToInt32(dt.Rows[i]["id"]);
                    if (dt.Rows.Count > 0)
                    {
                        sId = Convert.ToInt32(dt.Rows[0]["sid"]);
                        lId = Convert.ToInt32(dt.Rows[0]["lid"]);
                        tId = Convert.ToInt32(dt.Rows[0]["tid"]);
                    }
                    if (rb.Checked == true)
                    {
                        var a = SpGetData.GetDataFromSP("updateActivequestion", new object[,] { { "id", "checked", "sid", "lid", "tid" }, { id, rb.Checked, sId, lId, tId } });
                    }
                    //break;
                    //  }
                    // }
                    // for (int i = 0; i < grdQuestion.Rows.Count; i++)
                    //{
                    //RadioButton rb = (RadioButton)grdQuestion.Rows[i].Cells[10].FindControl("rbActive");


                    //}

                }
                else
                {
                    var dt = SpGetData.GetDataFromSP("BindGridQuestionCheckbox", new object[,] { { "id" }, { id } });
                    //id = Convert.ToInt32(dt.Rows[i]["id"]);
                    if (dt.Rows.Count > 0)
                    {
                        sId = Convert.ToInt32(dt.Rows[0]["sid"]);
                        lId = Convert.ToInt32(dt.Rows[0]["lid"]);
                        tId = Convert.ToInt32(dt.Rows[0]["tid"]);
                    }

                    // if (rb.Checked == true)
                    // {
                    var a = SpGetData.GetDataFromSP("updateActivequestion", new object[,] { { "id", "checked", "sid", "lid", "tid" }, { id, rb.Checked, sId, lId, tId } });
                    // }
                }
            }
            // bindGridview(sId, lId, tId);
            //SetSelectedRecord();
        }

        protected void ddlStore_SelectedIndexChanged(object sender, EventArgs e)
        {
            int storeId = Convert.ToInt32(ddlStore.SelectedValue);
            fillLocation(storeId);
        }

        protected void ddlStore1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int storeId = Convert.ToInt32(ddlStore1.SelectedValue);
            fillLocation1(storeId);
            fillTimeSlot1();
            bindGridviewAll(storeId);
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            clearField();
            btnSave.Text = "Save";
            btnCancel.Visible = false;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(this.hdnId.Value);
            int storeId = Convert.ToInt32(this.hdnsId.Value);
            int locationId = Convert.ToInt32(this.hdnlId.Value);
            int timeSlotId = Convert.ToInt32(this.hdntId.Value);
            //  int id = Convert.ToInt32(e.CommandArgument.ToString());
            var a = SpGetData.GetDataFromSP("DeleteQuestion", new object[,] { { "id" }, { id } });
            //bindGridview(Convert.ToInt32(ddlStore1.SelectedValue), Convert.ToInt32(ddlLocation1.SelectedValue), Convert.ToInt32(ddlTimeSlot1.SelectedValue));
            bindGridview(storeId, locationId, timeSlotId);
        }

        //protected void grdQuestion_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    grdQuestion.PageIndex = e.NewPageIndex;
        //    grdQuestion.Page.DataBind();
        //    bindGridview(Convert.ToInt32(ddlStore1.SelectedValue), Convert.ToInt32(ddlLocation1.SelectedValue), Convert.ToInt32(ddlTimeSlot1.SelectedValue));
        //}

       

    }
}
