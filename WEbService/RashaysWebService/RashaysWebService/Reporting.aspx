﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Rashays.Master" AutoEventWireup="true" CodeBehind="Reporting.aspx.cs" Inherits="RashaysWebService.Reporting" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/jquery-ui.css" rel="stylesheet" />

    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet" />
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet" />
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" />
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet" />
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
    <link href="../build/css/custom.min.css" rel="stylesheet" />
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet" />
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet" />
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet" />
    <script src="../src/jquery-1.12.4.js"></script>
    <script src="../css/jquery-ui.js"></script>
    <link href="../css/dataTables.responsive.css" rel="stylesheet" />
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="../vendors/date.format.js" type="text/javascript"></script>


    <script type="text/javascript">
        function GetData() {
            // alert(0);
            var date = document.getElementById('single_cal4').value;
            document.getElementById('ContentPlaceHolder1_hdnfiled').value = date;
        }
        function Reload() {
            $("#ReloadTable").click();
        }
        $(document).ready(function () {
            $("#ContentPlaceHolder1_ddlLocation1").change(function () {
                var storeId = $("#ContentPlaceHolder1_ddlStore1").val();
                var locId = $("#ContentPlaceHolder1_ddlLocation1").val();
                reloadDataTbl(storeId, locId);
                //setInterval(function () {
                //if (!oTable.data().any()) {
                //alert('<%= Session["feedbackcount1"] %>');
                //    $("#ContentPlaceHolder1_lblHappy").text('0');
                //    $("#ContentPlaceHolder1_lblNuetral").text('0');
                //    $("#ContentPlaceHolder1_lblSad").text('0');
                //} else {
                <%-- $("#ContentPlaceHolder1_lblHappy").text('<%= Session["feedbackcount1"] %>');
                        $("#ContentPlaceHolder1_lblNuetral").text('<%= Session["feedbackcount2"] %>');
                        $("#ContentPlaceHolder1_lblSad").text('<%= Session["feedbackcount3"] %>'); --%>
                //}
                //console.log('feedbackcount1: ' + '<%= Session["feedbackcount1"] %>');

                //}, 300);

            });

            function reloadDataTbl(storeId, locId) {
                //$.fn.dataTable.moment('DD-MMM-Y HH:mm:ss')
               

                // var kk = ('#grdFeedBack_paginate').val();
             
                var flg = 0;
                $('#showAfterDatatableLoad').css("display", "");
                var oTable = $('#grdFeedBack').DataTable(
                 {
                     destroy: true,
                     "columnDefs": [
                       {
                           "width": "5%", "targets": [0], "visible": false,
                           "searchable": true
                       },
                       { "className": "text-center custom-middle-align", "targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] }
                     ],
                     "language":
                       {
                           "processing": "<div class='overlay custom-loader-background'><i class='fa fa-cog fa-spin custom-loader-color'></i></div>"
                       },
                     "processing": true,
                     "serverSide": true,
                     "ajax":
                       {
                           "url": "Reporting.aspx/GetData",
                           "contentType": "application/json",
                           "type": "GET",
                           "dataType": "JSON",
                           "data": function (d) {
                               //console.log(d);
                               d.storeId = storeId, d.locationId = locId, d.flg = flg;
                               return d;
                           },
                           "dataSrc": function (json) {
                               //console.log(json);
                               json.draw = json.d.draw;
                               json.recordsTotal = json.d.recordsTotal;
                               json.recordsFiltered = json.d.recordsFiltered;
                               json.data = json.d.data;
                               var return_data = json;
                               //console.log(json.d.data)                               
                               return return_data.data;
                           }
                       },
                     dom: "Bfrtip",
                     buttons: [
                       {
                           extend: "copy",
                           className: "btn-sm"
                       },
                       {
                           extend: "csv",
                           className: "btn-sm",
                           title: "CsvFile"
                       },
                       {
                           extend: "excel",
                           className: "btn-sm"
                       },
                       {
                           extend: "pdfHtml5",
                           className: "btn-sm"
                       },
                       {
                           extend: "print",
                           className: "btn-sm"
                       },
                     ],
                     "initComplete": function( settings, json ) {
                         var val = $('.tdRationColumn:last').text();
                         if (val != "ratio") {

                             $("#ContentPlaceHolder1_lblHappy").text($('.tdRationColumn:last').text());
                         } else {
                             $("#ContentPlaceHolder1_lblHappy").text("0:0:0");
                         }
                     },
                     "rowCallback": function (row, data, index) {
                         if (data.countOfFack > 4) {
                             $("td", row).css("color", "red");
                         }

                     },
                     "columns": [
                            { "data": "id" },
                            { "data": "question" },
                            { "data": "feedBack" },
                            { "data": "happy" },
                            { "data": "neutral" },
                            { "data": "sad" },
                            { "data": "title" },
                            { "data": "location" },
                            { "data": "timeSlotName" },
                            {
                                "data": "datecreated", "Type": "date-uk"
                                , "render": function (value) {
                                    if (value === null) return "";

                                    var pattern = /Date\(([^)]+)\)/;
                                    var results = pattern.exec(value);
                                    var date = new Date(parseFloat(results[1]));
                                    return dateFormat(date, "dd-mm-yyyy hh:MM:ss TT");
                                }
                            },
                            { "data": "ratio", "className": "hidden tdRationColumn" }
                     ]
                 });


               // setTimeout(function () {
                    //var val = $('.tdRationColumn:last').text();
                    //if (val != "ratio") {

                    //    $("#ContentPlaceHolder1_lblHappy").text($('.tdRationColumn:last').text());
                    //} else {
                    //    $("#ContentPlaceHolder1_lblHappy").text("0:0:0");
                    //}

               // }, 1000);


                //$('ul.pagination li').live(function (e) {
                //    alert($(this).find("a").text());
                //});

            
                $("#ReloadTable").click(function () {
                    // Clear the current filter
                    //oTable = oTable;
                    //oTable.fnFilter('', 0);
                    //oTable.fnFilter('');

                    //// Reset all "visible" values in the first cell
                    //var table = oTable;
                    //table.api().rows().indexes().each(function (idx) {
                    //    var d = table.api().row(idx).data();
                    //    table.api().row(idx).data()[0] = 0;
                    //    d.counter++;
                    //    table.api().row(idx).data(d);
                    //});
                    var flg = 0;
                    $('#showAfterDatatableLoad').css("display", "");
                    var oTable = $('#grdFeedBack').DataTable(
                     {
                         destroy: true,
                         "columnDefs": [
                           {
                               "width": "5%", "targets": [0], "visible": false,
                               "searchable": true
                           },
                           { "className": "text-center custom-middle-align", "targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] }
                         ],
                         "language":
                           {
                               "processing": "<div class='overlay custom-loader-background'><i class='fa fa-cog fa-spin custom-loader-color'></i></div>"
                           },
                         "processing": true,
                         "serverSide": true,
                         "ajax":
                           {
                               "url": "Reporting.aspx/GetData",
                               "contentType": "application/json",
                               "type": "GET",
                               "dataType": "JSON",
                               "data": function (d) {
                                   //console.log(d);
                                   d.storeId = storeId, d.locationId = locId, d.flg = flg;
                                   return d;
                               },
                               "dataSrc": function (json) {
                                   //console.log(json);
                                   json.draw = json.d.draw;
                                   json.recordsTotal = json.d.recordsTotal;
                                   json.recordsFiltered = json.d.recordsFiltered;
                                   json.data = json.d.data;
                                   var return_data = json;
                                   //console.log(json.d.data)                               
                                   return return_data.data;
                               }
                           },
                         dom: "Bfrtip",
                         buttons: [
                           {
                               extend: "copy",
                               className: "btn-sm"
                           },
                           {
                               extend: "csv",
                               className: "btn-sm",
                               title: "CsvFile"
                           },
                           {
                               extend: "excel",
                               className: "btn-sm"
                           },
                           {
                               extend: "pdfHtml5",
                               className: "btn-sm"
                           },
                           {
                               extend: "print",
                               className: "btn-sm"
                           },
                         ],
                         "initComplete": function (settings, json) {
                             var val = $('.tdRationColumn:last').text();
                             if (val != "ratio") {

                                 $("#ContentPlaceHolder1_lblHappy").text($('.tdRationColumn:last').text());
                             } else {
                                 $("#ContentPlaceHolder1_lblHappy").text("0:0:0");
                             }
                         },
                         "rowCallback": function (row, data, index) {
                             if (data.countOfFack > 4) {
                                 $("td", row).css("color", "red");
                             }

                         },
                         "columns": [
                                { "data": "id" },
                                { "data": "question" },
                                { "data": "feedBack" },
                                { "data": "happy" },
                                { "data": "neutral" },
                                { "data": "sad" },
                                { "data": "title" },
                                { "data": "location" },
                                { "data": "timeSlotName" },
                                {
                                    "data": "datecreated", "Type": "date-uk"
                                    , "render": function (value) {
                                        if (value === null) return "";

                                        var pattern = /Date\(([^)]+)\)/;
                                        var results = pattern.exec(value);
                                        var date = new Date(parseFloat(results[1]));
                                        return dateFormat(date, "dd-mm-yyyy hh:MM:ss TT");
                                    }
                                },
                                { "data": "ratio", "className": "hidden tdRationColumn" }
                         ]
                     });
                });

                $("#FilterCurrentPage").click(function () {

                  //  debugger;
                   // var h = new Object();
                   // var x = $('.odd,.even').filter(function () {
                   //     var idx = $(this)[0]._DT_RowIndex;
                   //     h[idx] = idx;
                   //     return this.style.display == ''
                   // });

                   // // update the first value based on the currently visible rows
                   // var table = $('#grdFeedBack').dataTable();
                   //// var table = oTable;
                   // table.api().rows().indexes().each(function (idx) {
                   //     var d = table.api().row(idx).data();
                   //     if (h.hasOwnProperty(idx)) {
                   //         table.api().row(idx).data()[0] = 1;
                   //     }
                   //     d.counter++;
                   //     table.api().row(idx).data(d);
                   // });

                   // // filter rows with a value of 1 in the first cell
                   // //oTable = $('#ContentPlaceHolder1_grdFeedBack').dataTable();
                   // oTable = $('#grdFeedBack').dataTable();
                    // oTable.fnFilter(1, 0);
                    //debugger;
                    var length = 1;
                    var flg = 1;
                    $('#showAfterDatatableLoad').css("display", "");
                    debugger;
                    length = $(".paginate_button.active").find("a");
                    length = length[0]['innerHTML'];
                    //alert(length);

                    var oTable = $('#grdFeedBack').DataTable(
                     {
                         destroy: true,
                         "columnDefs": [
                           {
                               "width": "5%", "targets": [0], "visible": false,
                               "searchable": true
                           },
                           { "className": "text-center custom-middle-align", "targets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] }
                         ],
                         "language":
                           {
                               "processing": "<div class='overlay custom-loader-background'><i class='fa fa-cog fa-spin custom-loader-color'></i></div>"
                           },
                         "processing": true,
                         "serverSide": true,
                         "ajax":
                           {
                               "url": "Reporting.aspx/GetData",
                               "contentType": "application/json",
                               "type": "GET",
                               "dataType": "JSON",
                               "data": function (d) {
                                   //console.log(d);
                                   d.storeId = storeId, d.locationId = locId, d.length = length, d.flg = flg;
                                   return d;
                               },
                               "dataSrc": function (json) {
                                   console.log(json);
                                   json.draw = json.d.draw;
                                   json.recordsTotal = json.d.recordsTotal;
                                   json.recordsFiltered = json.d.recordsFiltered;
                                   json.data = json.d.data;
                                   var return_data = json;
                                   //console.log(json.d.data)                               
                                   return return_data.data;
                               }
                           },
                         dom: "Bfrtip",
                         buttons: [
                           {
                               extend: "copy",
                               className: "btn-sm"
                           },
                           {
                               extend: "csv",
                               className: "btn-sm",
                               title: "CsvFile"
                           },
                           {
                               extend: "excel",
                               className: "btn-sm"
                           },
                           {
                               extend: "pdfHtml5",
                               className: "btn-sm"
                           },
                           {
                               extend: "print",
                               className: "btn-sm"
                           },
                         ],
                         "rowCallback": function (row, data, index) {
                             if (data.countOfFack > 4) {
                                 $("td", row).css("color", "red");
                             }

                         },
                         "columns": [
                                { "data": "id" },
                                { "data": "question" },
                                { "data": "feedBack" },
                                { "data": "happy" },
                                { "data": "neutral" },
                                { "data": "sad" },
                                { "data": "title" },
                                { "data": "location" },
                                { "data": "timeSlotName" },
                                {
                                    "data": "datecreated", "Type": "date-uk"
                                    , "render": function (value) {
                                        if (value === null) return "";

                                        var pattern = /Date\(([^)]+)\)/;
                                        var results = pattern.exec(value);
                                        var date = new Date(parseFloat(results[1]));
                                        return dateFormat(date, "dd-mm-yyyy hh:MM:ss TT");
                                    }
                                },
                                { "data": "ratio", "className": "hidden tdRationColumn" }
                         ]
                     });

                });

            }


        });
    </script>

    <style>
        .dataTables_filter {
            width: auto !important;
        }

        .dataTables_length {
            width: 16% !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="form-horizontal form-label-left">
                    <fieldset>
                        <asp:UpdatePanel runat="server" ID="sad">
                            <ContentTemplate>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                        Select Date
                                    </label>
                                    <div class="xdisplay_inputx form-group has-feedback col-md-6 col-sm-6 col-xs-12">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <asp:TextBox runat="server" type="date" data-date-inline-picker="true" ID="single_cal4" OnTextChanged="single_cal4_TextChanged" AutoPostBack="true" CssClass="form-control col-md-7 col-xs-12">
                                            </asp:TextBox>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <asp:TextBox runat="server" type="date" data-date-inline-picker="true" ID="single_cal5" OnTextChanged="single_cal5_TextChanged" AutoPostBack="true" CssClass="form-control col-md-7 col-xs-12">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </fieldset>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                            Select Store
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <asp:DropDownList ID="ddlStore1" runat="server" CssClass="select2_single form-control" OnSelectedIndexChanged="ddlStore1_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                            Select Location
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <asp:DropDownList ID="ddlLocation1" runat="server" CssClass="select2_single form-control"></asp:DropDownList>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>
    <div id="showAfterDatatableLoad" style="display: none;">
        <div class="row">
            <div class="form-group">
                <div class="col-md-3">
                    <button id="FilterCurrentPage" name="button" type="button" onclick="javascript:void(0);">Filter Current Page</button>
                    <button id="ReloadTable" name="button" type="button" onclick="javascript:void(0);">Reload Table</button>
                </div>
                <div class="col-md-9">
                    <label style="float: left">
                        Ratio :
                    </label>
                    <div style="float: left; margin-left: 15px">
                        <asp:Label ID="lblHappy" runat="server" EnableViewState="false">
                        </asp:Label>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_content ">
                        <table id="grdFeedBack" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Question</th>
                                    <th>Feedback</th>
                                    <th>happy</th>
                                    <th>neutral</th>
                                    <th>sad</th>
                                    <th>title</th>
                                    <th>location</th>
                                    <th>timeSlotName</th>
                                    <th>datecreated</th>
                                    <th class="hidden">ratio</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <%--</div>--%>
                    </div>
                    <asp:HiddenField ID="hdnfiled" runat="server" />
                </div>
            </div>
        </div>

    </div>
</asp:Content>
