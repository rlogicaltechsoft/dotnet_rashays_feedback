﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Rashays.Master" AutoEventWireup="true" CodeBehind="Question.aspx.cs" Inherits="RashaysWebService.Question" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .pagination {
            display: inline-block;
        }

            .pagination a {
                color: black;
                float: left;
                padding: 8px 16px;
                text-decoration: none;
            }

                .pagination a.active {
                    background-color: #4CAF50;
                    color: white;
                }
    </style>
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript">
        function saveMessage() {
            //alert("Question save sucessfully...");
        }
        function updateMessage() {
            //alert("Question update sucessfully...");
        }

        function RadioCheck(rb) {
            var gv = document.getElementById("<%=grdQuestion.ClientID%>");
            var rbs = gv.getElementsByTagName("input");
            var row = rb.parentNode.parentNode;
            for (var i = 0; i < rbs.length; i++) {
                if (rbs[i].type == "radio") {
                    if (rbs[i].checked && rbs[i] != rb) {
                        rbs[i].checked = false;
                        break;
                    }
                }
            }
        }
        function DeleteAlert(objid, objsid, objlid, objtid) {
            //alert(objtid);
            $('#ContentPlaceHolder1_hdnId').val(objid);
            $('#ContentPlaceHolder1_hdnsId').val(objsid);
            $('#ContentPlaceHolder1_hdnlId').val(objlid);
            $('#ContentPlaceHolder1_hdntId').val(objtid);
            $('#modalAlert').modal('toggle');
            $('#modalAlert').modal('show');
            return false;
        }

        function NoDelete() {
            $('#NoDelete').modal('toggle');
            $('#NoDelete').modal('show');
        }
    </script>
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet" />
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="../build/css/custom.min.css" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="col-md-12">
        <div class="x_panel">
            <div class="form-horizontal form-label-left">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Select Store
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:DropDownList ID="ddlStore" runat="server" DataTextField="id" DataValueField="title" CssClass="select2_single form-control" OnSelectedIndexChanged="ddlStore_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="Please select store" ForeColor="Red" ValidationGroup="abc" ControlToValidate="ddlStore" EnableClientScript="false" InitialValue="0"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Select Location
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:DropDownList ID="ddlLocation" runat="server" DataTextField="id" DataValueField="location" CssClass="select2_single form-control"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic" ErrorMessage="Please select location" ForeColor="Red" ValidationGroup="abc" ControlToValidate="ddlLocation" EnableClientScript="false" InitialValue="0"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Select TimeSlot
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:DropDownList ID="ddlTimeSlot" runat="server" DataTextField="id" DataValueField="timeSlotName" CssClass="select2_single form-control" OnSelectedIndexChanged="ddlTimeSlot_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic" ErrorMessage="Please select timeslot" ForeColor="Red" ValidationGroup="abc" ControlToValidate="ddlTimeSlot" EnableClientScript="false" InitialValue="0"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        TimeSlot Duration
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:Label ID="lblFrom" runat="server" Text='<%# Eval("timeFrom") %>' Visible="false" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>

                        <asp:Label ID="lblTo" runat="server" Text='<%# Eval("timeTo") %>' Visible="false" class="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Question
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:TextBox ID="txtQuestion" runat="server" class="form-control col-md-7 col-xs-12"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic" ErrorMessage="Please enter question" ForeColor="Red" ValidationGroup="abc" ControlToValidate="txtQuestion" EnableClientScript="false"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Happy thank you message
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:TextBox ID="txtHappy" runat="server" class="form-control col-md-7 col-xs-12"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic" ErrorMessage="Please enter message" ForeColor="Red" ValidationGroup="abc" ControlToValidate="txtHappy" EnableClientScript="false"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Neutral thank you message
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:TextBox ID="txtNeutral" runat="server" class="form-control col-md-7 col-xs-12"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic" ErrorMessage="Please enter message" ForeColor="Red" ValidationGroup="abc" ControlToValidate="txtNeutral" EnableClientScript="false"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Sad thank you message
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:TextBox ID="txtSad" runat="server" class="form-control col-md-7 col-xs-12"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic" ErrorMessage="Please enter message" ForeColor="Red" ValidationGroup="abc" ControlToValidate="txtSad" EnableClientScript="false"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" ValidationGroup="abc" class="btn btn-primary" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" class="btn btn-primary" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="x_panel">
            <div class="form-horizontal form-label-left">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Select Store
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:DropDownList ID="ddlStore1" runat="server" DataTextField="id" DataValueField="title" CssClass="select2_single form-control" OnSelectedIndexChanged="ddlStore1_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Select Location
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:DropDownList ID="ddlLocation1" runat="server" DataTextField="id" DataValueField="location" CssClass="select2_single form-control" OnSelectedIndexChanged="ddlLocation2_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Select TimeSlot
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:DropDownList ID="ddlTimeSlot1" runat="server" DataTextField="id" DataValueField="timeSlotName" CssClass="select2_single form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlTimeSlot1_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="x_content">
                <%--       <div class="pagination" style="text-align: left !important;">--%>
                <asp:GridView ID="grdQuestion" runat="server" AutoGenerateColumns="false" OnRowCommand="grdQuestion_RowCommand" OnRowDeleting="grdQuestion_RowDeleting" OnRowEditing="grdQuestion_RowEditing" GridLines="None" CssClass="table table-striped">
                    <Columns>
                        <asp:TemplateField HeaderText="id" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblId" runat="server" Text='<%# Eval("id")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Question">
                            <ItemTemplate>
                                <asp:Label ID="lblQuestion" runat="server" Text='<%# Eval("question")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Happy message">
                            <ItemTemplate>
                                <asp:Label ID="lblHappy" runat="server" Text='<%# Eval("happy")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Neutral Message">
                            <ItemTemplate>
                                <asp:Label ID="lblNeutral" runat="server" Text='<%# Eval("neutral")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sad Message">
                            <ItemTemplate>
                                <asp:Label ID="lblSad" runat="server" Text='<%# Eval("sad")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Store">
                            <ItemTemplate>
                                <asp:Label ID="lblTitle" runat="server" Text='<%# Eval("title")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location">
                            <ItemTemplate>
                                <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("location")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TimeSlot">
                            <ItemTemplate>
                                <asp:Label ID="lblTimeSlot" runat="server" Text='<%# Eval("timeSlotName")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnedit" runat="server" CommandName="Edit" CommandArgument='<%# Eval("id") %>' CssClass="btn btn-primary !important" >Edit</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <%--<asp:Button ID="btndelete" runat="server" Text="Delete" CommandName="Delete" CommandArgument='<%# Eval("id") + ";" +Eval("sid")  + ";" +Eval("lid")  + ";" +Eval("tid")  %>' />--%>
                                <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-primary" data-toggle="modal" OnClientClick='<%# String.Format("javascript:return DeleteAlert(\"{0}\",\"{1}\",\"{2}\",\"{3}\")", Eval("id").ToString(), Eval("sid").ToString(), Eval("lid").ToString(), Eval("tid").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Active">
                            <ItemTemplate>
                                <asp:RadioButton ID="RadioButton1" runat="server" GroupName="rbgrp" onclick="RadioCheck(this);" Checked='<%# Eval("checked") %>' OnCheckedChanged="RadioButton1_CheckedChanged" AutoPostBack="true" />
                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#Eval("id")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>

                </asp:GridView>
                <%--</div>--%>
            </div>
        </div>
    </div>

    <label id="lblfound" runat="server" visible="false" style="color: red">
    </label>
    <div id="modalAlert" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Delete Confirmation</h4>
                </div>
                <div class="modal-body">
                    <h4>Are you sure want to Delete this location?</h4>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnDelete" runat="server" Text="Yes" OnClick="btnDelete_Click" class="btn btn-primary" />
                    <asp:Button ID="btnCancelDelete" runat="server" Text="Cancel" class="btn btn-primary" />
                </div>

            </div>
        </div>
    </div>
    <div id="NoDelete" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel1">Delete</h4>
                </div>
                <div class="modal-body">
                    <h4>this Location have already question, please delete question first, thanks.</h4>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="Button2" runat="server" Text="Ok" class="btn btn-primary" />
                </div>

            </div>
        </div>
    </div>
    <div>
        <asp:HiddenField ID="hdnId" runat="server" />
        <asp:HiddenField ID="hdnsId" runat="server" />
        <asp:HiddenField ID="hdnlId" runat="server" />
        <asp:HiddenField ID="hdntId" runat="server" />
    </div>
</asp:Content>
