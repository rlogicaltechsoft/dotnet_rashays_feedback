﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static RashaysWebService.Reporting;

namespace RashaysWebService
{
    public class DataTables
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<Reportingcls> data { get; set; }
    }
}