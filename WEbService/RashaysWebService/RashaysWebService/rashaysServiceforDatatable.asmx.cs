﻿using RashaysWebService.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using static RashaysWebService.Reporting;

namespace RashaysWebService
{
    /// <summary>
    /// Summary description for rashaysServiceforDatatable
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class rashaysServiceforDatatable : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        //public List<Reportingcls> bindGridview(int storeId, int locationId, string date, string date1)
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public void SetCustomerInformation(int storeId, int locationId)
        {
            //public List<Reportingcls> bindGridview()
            //{
            //  int storeId = 0;
            // int locationId = 0;
            string date = "";
            string date1 = "";
            List<Reportingcls> objlist = new List<Reportingcls>();
            DataTable a = new DataTable();
            DataTable fake = new DataTable();
            try
            {
                Reportingcls obj = new Reportingcls();
                if (date == "")
                {
                    fake = SpGetData.GetDataFromSP("Bindfakeresult", new object[,] { { }, { } });
                    a = SpGetData.GetDataFromSP("BindGridFeedbackReporting", new object[,] { { "storeId", "locationId", "date" }, { storeId, locationId, "" } });
                    //grdFeedBack.DataSource = a;
                    // grdFeedBack.DataBind();

                    foreach (DataRow dr1 in a.Rows)
                    {
                        var student = new Reportingcls
                        {
                            id = Convert.ToInt32(dr1["id"]),
                            question = Convert.ToString(dr1["question"]),
                            feedBack = Convert.ToString(dr1["feedBack"]),
                            happy = Convert.ToString(dr1["happy"]),
                            neutral = Convert.ToString(dr1["neutral"]),
                            sad = Convert.ToString(dr1["sad"]),
                            title = Convert.ToString(dr1["title"]),
                            location = Convert.ToString(dr1["location"]),
                            timeSlotName = Convert.ToString(dr1["timeSlotName"]),
                            datecreated = Convert.ToDateTime(dr1["datecreated"])
                        };
                    objlist.Add(student);
                    }

                    DataRow[] dr;
                    dr = a.Select("feedbackcount=1");
                    //lblHappy.Text = Convert.ToString(dr.Length);
                    dr = null;

                    dr = a.Select("feedbackcount=2");
                    //lblNuetral.Text = Convert.ToString(dr.Length);
                    dr = null;

                    dr = a.Select("feedbackcount=3");
                    //lblSad.Text = Convert.ToString(dr.Length);
                    dr = null;

                }
                else
                {
                    fake = SpGetData.GetDataFromSP("Bindfakeresult", new object[,] { { }, { } });
                    DateTime dt = Convert.ToDateTime(date);
                    date = dt.ToString("yyyy-MM-dd");
                    var b = SpGetData.GetDataFromSP("BindGridFeedbackReporting", new object[,] { { "storeId", "locationId", "date", "date1" }, { storeId, locationId, date, date1 } });
                    //grdFeedBack.DataSource = b;
                    //grdFeedBack.DataBind();

                    

                    foreach (DataRow dr1 in a.Rows)
                    {
                        obj.id = Convert.ToInt32(dr1["id"]);
                        obj.question = Convert.ToString(dr1["question"]);
                        obj.feedBack = Convert.ToString(dr1["feedBack"]);
                        obj.happy = Convert.ToString(dr1["happy"]);
                        obj.neutral = Convert.ToString(dr1["neutral"]);
                        obj.sad = Convert.ToString(dr1["sad"]);
                        obj.title = Convert.ToString(dr1["title"]);
                        obj.location = Convert.ToString(dr1["location"]);
                        obj.timeSlotName = Convert.ToString(dr1["timeSlotName"]);
                        obj.datecreated = Convert.ToDateTime(dr1["datecreated"]);
                        objlist.Add(obj);
                    }

                    DataRow[] dr;
                    dr = b.Select("feedbackcount=1");
                    //lblHappy.Text = Convert.ToString(dr.Length);
                    dr = null;

                    dr = b.Select("feedbackcount=2");
                    //lblNuetral.Text = Convert.ToString(dr.Length);
                    dr = null;

                    dr = b.Select("feedbackcount=3");
                    //lblSad.Text = Convert.ToString(dr.Length);
                    dr = null;
                }
                // Session["date"] = "";

            }
            catch (Exception ex)
            {

            }
            var js = new JavaScriptSerializer();
            HttpContext.Current.Response.Write(js.Serialize(objlist));
            //return objlist;
        }

    }
}
