﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Rashays.Master" AutoEventWireup="true" CodeBehind="Notification.aspx.cs" Inherits="RashaysWebService.Notification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet" />
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- Chart.js -->
        <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
        <!-- gauge.js -->
        <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
        <!-- bootstrap-progressbar -->

    <script type="text/javascript">
        function DeleteAlert(objid) {
            $('#ContentPlaceHolder1_hdnId').val(objid);
            $('#modalAlert').modal('toggle');
            $('#modalAlert').modal('show');
            return false;
        }

        function NoDelete() {
            $('#NoDelete').modal('toggle');
            $('#NoDelete').modal('show');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="col-md-6">
        <div class="x_panel">
            <div class="form-horizontal form-label-left">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Select Store
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:DropDownList ID="ddlStore1" runat="server" DataTextField="id" DataValueField="title" CssClass="select2_single form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlStore1_SelectedIndexChanged"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="Please select store" ForeColor="Red" ValidationGroup="abc" ControlToValidate="ddlStore1" EnableClientScript="false" InitialValue="0"></asp:RequiredFieldValidator>
                    </div>

                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Select Location
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:DropDownList ID="ddlLocation1" runat="server" DataTextField="id" DataValueField="location" CssClass="select2_single form-control"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic" ErrorMessage="Please select location" ForeColor="Red" ValidationGroup="abc" ControlToValidate="ddlLocation1" EnableClientScript="false" InitialValue="0"></asp:RequiredFieldValidator>
                    </div>

                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        <img src="Images/sad.png" alt="" />
                        <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:TextBox ID="txtMinutes" runat="server" placeholder="Minutes" class="form-control col-md-7 col-xs-12"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic" ErrorMessage="Please enter minute" ForeColor="Red" ValidationGroup="abc" ControlToValidate="txtMinutes" EnableClientScript="false"></asp:RequiredFieldValidator>
                        <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" ForeColor="Red" ControlToValidate="txtMinutes"  ValidationGroup="abc" ErrorMessage="Value must be a whole number" />
                    </div>

                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Count
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:TextBox ID="txtCount" runat="server" class="form-control col-md-7 col-xs-12"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic" ErrorMessage="Please enter count" ForeColor="Red" ValidationGroup="abc" ControlToValidate="txtCount" EnableClientScript="false"></asp:RequiredFieldValidator>
                        <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" ForeColor="Red" ControlToValidate="txtCount"  ValidationGroup="abc" ErrorMessage="Value must be a whole number" />
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" ValidationGroup="abc" class="btn btn-primary" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" class="btn btn-primary" />
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="x_panel">
            <div class="x_content">
                <asp:GridView ID="grdNotification" runat="server" AutoGenerateColumns="false" OnRowCommand="grdNotification_RowCommand" OnRowDeleting="grdNotification_RowDeleting" OnRowEditing="grdNotification_RowEditing" GridLines="None" CssClass="table table-striped">
                    <Columns>
                        <asp:TemplateField HeaderText="Store">
                            <ItemTemplate>
                                <asp:Label ID="lbltitle" runat="server" Text='<%# Eval("title")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location">
                            <ItemTemplate>
                                <asp:Label ID="lbllocation" runat="server" Text='<%# Eval("location")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Minutes">
                            <ItemTemplate>
                                <asp:Label ID="lblquestion" runat="server" Text='<%# Eval("minutes")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Count">
                            <ItemTemplate>
                                <asp:Label ID="lblfeedback" runat="server" Text='<%# Eval("count")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnedit" runat="server" CommandName="Edit" CommandArgument='<%# Eval("id") %>' CssClass="btn btn-primary">Edit</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-primary" data-toggle="modal" OnClientClick='<%# String.Format("javascript:return DeleteAlert(\"{0}\")", Eval("id").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <div id="modalAlert" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Delete Confirmation</h4>
                </div>
                <div class="modal-body">
                    <h4>Are you sure want to Delete this location?</h4>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnDelete" runat="server" Text="Yes" OnClick="btnDelete_Click" class="btn btn-primary" />
                    <asp:Button ID="btnCancelDelete" runat="server" Text="Cancel" class="btn btn-primary" />
                </div>

            </div>
        </div>
    </div>
    <div id="NoDelete" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel1">Delete</h4>
                </div>
                <div class="modal-body">
                    <h4>this Location have already question, please delete question first, thanks.</h4>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="Button2" runat="server" Text="Ok" class="btn btn-primary" />
                </div>

            </div>
        </div>
    </div>
    <div>
        <asp:HiddenField ID="hdnId" runat="server" />
    </div>
</asp:Content>
