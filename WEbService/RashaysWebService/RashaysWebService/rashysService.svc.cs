﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using RashaysWebService.Model;
using System.Data;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Data.SqlClient;

namespace RashaysWebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "rashysService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select rashysService.svc or rashysService.svc.cs at the Solution Explorer and start debugging.
    public class rashysService : IrashysService
    {
        //public string test()
        //{
        //    return "hello .. .. .. !! !! !!";
        //}
        public List<Storelisting> GetStore()
        {
            List<Storelisting> objlist = new List<Storelisting>();
            try
            {
                // Storelisting objSet = new Storelisting();
                Storelisting objSet;
                var a = SpGetData.GetDataFromSP("GetStoreListing", new object[,] { { }, { } });
                foreach (DataRow dr in a.Rows)
                {
                    objSet = new Storelisting();
                    objSet.id = Convert.ToInt32(dr["id"]);
                    objSet.title = Convert.ToString(dr["title"]);
                    objSet.description = Convert.ToString(dr["description"]);
                    objlist.Add(objSet);
                }
            }
            catch (Exception ex)
            {

            }
            return objlist;
        }

        public Feedback InsertfeedBack(int storeId, int feedBack, int locationId, int questionId, string description)
        {

            Feedback Objfeedback = new Feedback();
            try
            {
                var a = SpGetData.GetDataFromSP("InsertFeedback", new object[,] { { "storeId", "feedBack", "locationId", "questionId", "description" }, { storeId, feedBack, locationId, questionId, description } });
                if (a.Rows.Count > 0)
                {
                    Objfeedback.id = Convert.ToInt32(a.Rows[0]["id"]);
                    Objfeedback.success = "true";
                }
                else
                {
                    Objfeedback.success = "false";
                }
            }
            catch (Exception ex)
            {
                Objfeedback.success = "false : " + ex.Message.ToString();
            }
            return Objfeedback;
        }

        public List<questions> GetQuestion(int storeId, int locationId)
        {
            List<questions> objlist = new List<questions>();
            try
            {
                // Storelisting objSet = new Storelisting();
                questions objSet;
                var a = SpGetData.GetDataFromSP("GetQuestions", new object[,] { { "storeId", "locationId" }, { storeId, locationId } });
                foreach (DataRow dr in a.Rows)
                {
                    objSet = new questions();
                    objSet.storeId = Convert.ToInt32(dr["storeId"]);
                    objSet.locationId = Convert.ToInt32(dr["locationId"]);
                    objSet.timeSlotName = Convert.ToString(dr["timeSlotName"]);
                    objSet.question = Convert.ToString(dr["question"]);
                    objSet.questionId = Convert.ToInt32(dr["qId"]);
                    objSet.happy = Convert.ToString(dr["happy"]);
                    objSet.neutral = Convert.ToString(dr["neutral"]);
                    objSet.sad = Convert.ToString(dr["sad"]);
                    objSet.timeFrom = Convert.ToString(dr["timeFrom"]);
                    objSet.timeTo = Convert.ToString(dr["timeTo"]);
                    objlist.Add(objSet);
                }
            }
            catch (Exception ex)
            {

            }
            return objlist;
        }

        public List<locations> GetLocation(int storeId)
        {
            List<locations> objlist = new List<locations>();
            try
            {
                // Storelisting objSet = new Storelisting();
                locations objSet;
                var a = SpGetData.GetDataFromSP("GetLocation", new object[,] { { "storeId" }, { storeId } });
                foreach (DataRow dr in a.Rows)
                {
                    objSet = new locations();
                    objSet.id = Convert.ToInt32(dr["id"]);
                    objSet.storeId = Convert.ToInt32(dr["storeId"]);
                    objSet.location = Convert.ToString(dr["location"]);
                    objlist.Add(objSet);
                }
            }
            catch (Exception ex)
            {

            }
            return objlist;
        }

        //public memberData InsertfeedBackOff(jSonData[] jsonData)
        //{
        //    var res = JsonConvert.SerializeObject(jsonData);
        //   // return SaveJsontoDB(res);
        //    memberData objMemberData = new memberData();
        //    string firstName = "";

        //    return objMemberData;
            
        //}

        //private string SaveJsontoDB(string oJson)
        //{
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("{\"jsonResult\":[{");


        //    var jSonData = serializer.Deserialize<jsonDataCollection>(oJson);
        //    foreach (var data in jSonData.jsonData)
        //    {
        //        Feedback Objfeedback = new Feedback();
        //        try
        //        {
        //            var a = SpGetData.GetDataFromSP("InsertFeedback", new object[,] { { "storeId", "feedBack", "locationId", "questionId", "description" }, { data.storeId, data.feedBack, data.locationId, data.questionId, data.description } });
        //            if (a.Rows.Count > 0)
        //            {
        //                Objfeedback.id = Convert.ToInt32(a.Rows[0]["id"]);
        //                Objfeedback.success = "true";
        //            }
        //            else
        //            {
        //                Objfeedback.success = "false";
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Objfeedback.success = "false : " + ex.Message.ToString();
        //        }
        //    }

        //    //sb.ToString().TrimEnd(',');
        //    sb.Remove(sb.Length - 1, 1);
        //    sb.Append("}]}");
        //    return JsonConvert.SerializeObject(sb.ToString());
        //}
        public memberData InsertfeedBackOff(jSonData[] message)
        {
            memberData Objfeedback = new memberData();
            int storeId = 0;
            int feedBack = 0;
            int locationId = 0;
            int questionId = 0;
            string description = "";
            foreach (jSonData data in message)
            {
                storeId = data.storeId;
                feedBack = data.feedBack;
                locationId = data.locationId;
                questionId = data.questionId;
                description = data.description;

             
                 try
                 {
                     var a = SpGetData.GetDataFromSP("InsertFeedback", new object[,] { { "storeId", "feedBack", "locationId", "questionId", "description" }, { storeId, feedBack, locationId, questionId, description } });
                     if (a.Rows.Count > 0)
                     {
                         Objfeedback.success = "true";
                     }
                     else
                     {
                         Objfeedback.success = "false";
                     }
                 }
                 catch (Exception ex)
                 {
                     Objfeedback.success = "false : " + ex.Message.ToString();
                 }
            }

            return Objfeedback;
        }

    }
}


