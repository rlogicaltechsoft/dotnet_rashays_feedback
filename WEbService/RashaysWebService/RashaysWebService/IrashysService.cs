﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using RashaysWebService.Model;

namespace RashaysWebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IrashysService" in both code and config file together.
    [ServiceContract(Namespace = "", Name = "rashysService")]
    public interface IrashysService
    {

        //[OperationContract]
        //[WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json, UriTemplate = "test")]
        //string test();

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetStore")]
        List<Storelisting> GetStore();

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "InsertfeedBack?storeId={storeId}&feedBack={feedBack}&locationId={locationId}&questionId={questionId}&description={description}")]
        Feedback InsertfeedBack(int storeId, int feedBack, int locationId,int questionId ,string description);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetQuestion?storeId={storeId}&locationId={locationId}")]
        List<questions> GetQuestion(int storeId, int locationId);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetLocation?storeId={storeId}")]
        List<locations> GetLocation(int storeId);

        //[OperationContract]
        //[WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "InsertfeedBackOff")]
        //memberData InsertfeedBackOff(jSonData[] jsonData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, UriTemplate = "InsertfeedBackOff")]
        memberData InsertfeedBackOff(jSonData[] message);
    }
}
