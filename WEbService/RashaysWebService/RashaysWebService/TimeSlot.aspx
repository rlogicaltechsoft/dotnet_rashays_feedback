﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Rashays.Master" AutoEventWireup="true" CodeBehind="TimeSlot.aspx.cs" Inherits="RashaysWebService.TimeSlot" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet" />
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- Chart.js -->
        <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
        <!-- gauge.js -->
        <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
        <!-- bootstrap-progressbar -->

    <script type="text/javascript">
        function DeleteAlert(objid) {
            $('#ContentPlaceHolder1_hdnId').val(objid);
            $('#modalAlert').modal('toggle');
            $('#modalAlert').modal('show');
            return false;
        }

        function NoDelete() {
            $('#NoDelete').modal('toggle');
            $('#NoDelete').modal('show');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <div class="col-md-12">
        <div class="x_panel">
            <div class="form-horizontal form-label-left">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        TimeSlot Name 
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:TextBox ID="txtName" runat="server" class="form-control col-md-7 col-xs-12"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="Please enter timeslotname" ForeColor="Red" ValidationGroup="abc" ControlToValidate="txtName" EnableClientScript="false"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Select TimeSlot
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <asp:DropDownList ID="ddlFrom" runat="server" Width="145px" CssClass="select2_single form-control">
                                <asp:ListItem Text="--Select time--" Value="0" />
                                <asp:ListItem Text="01:00" Value="1" />
                                <asp:ListItem Text="02:00" Value="2" />
                                <asp:ListItem Text="03:00" Value="3" />
                                <asp:ListItem Text="04:00" Value="4" />
                                <asp:ListItem Text="05:00" Value="5" />
                                <asp:ListItem Text="06:00" Value="6" />
                                <asp:ListItem Text="07:00" Value="7" />
                                <asp:ListItem Text="08:00" Value="8" />
                                <asp:ListItem Text="09:00" Value="9" />
                                <asp:ListItem Text="10:00" Value="10" />
                                <asp:ListItem Text="11:00" Value="11" />
                                <asp:ListItem Text="12:00" Value="12" />
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic" ErrorMessage="Please select time" ForeColor="Red" ValidationGroup="abc" ControlToValidate="ddlFrom" EnableClientScript="false" InitialValue="0"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <asp:DropDownList ID="ddlFromampm" runat="server" Width="110px" CssClass="select2_single form-control" OnSelectedIndexChanged="ddlFromampm_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Text="--AMPM--" Value="0" />
                                <asp:ListItem Text="AM" Value="1" />
                                <asp:ListItem Text="PM" Value="2" />
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic" ErrorMessage="Please select am/pm" ForeColor="Red" ValidationGroup="abc" ControlToValidate="ddlFromampm" EnableClientScript="false" InitialValue="0"></asp:RequiredFieldValidator>
                        </div>


                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <asp:DropDownList ID="ddlTo" runat="server" Width="145px" CssClass="select2_single form-control" OnSelectedIndexChanged="ddlTo_SelectedIndexChanged" AutoPostBack="true" Enabled="false">
                                <asp:ListItem Text="--Select time--" Value="0" />
                                <asp:ListItem Text="01:00" Value="1" />
                                <asp:ListItem Text="02:00" Value="2" />
                                <asp:ListItem Text="03:00" Value="3" />
                                <asp:ListItem Text="04:00" Value="4" />
                                <asp:ListItem Text="05:00" Value="5" />
                                <asp:ListItem Text="06:00" Value="6" />
                                <asp:ListItem Text="07:00" Value="7" />
                                <asp:ListItem Text="08:00" Value="8" />
                                <asp:ListItem Text="09:00" Value="9" />
                                <asp:ListItem Text="10:00" Value="10" />
                                <asp:ListItem Text="11:00" Value="11" />
                                <asp:ListItem Text="12:00" Value="12" />
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic" ErrorMessage="Please select time" ForeColor="Red" ValidationGroup="abc" ControlToValidate="ddlTo" EnableClientScript="false" InitialValue="0"></asp:RequiredFieldValidator>
                        </div>


                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <asp:DropDownList ID="ddlToamapm" runat="server" Width="110px" Enabled="false" CssClass="select2_single form-control">
                                <asp:ListItem Text="--AMPM--" Value="0" />
                                <asp:ListItem Text="AM" Value="1" />
                                <asp:ListItem Text="PM" Value="2" />
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic" ErrorMessage="Please select am/pm" ForeColor="Red" ValidationGroup="abc" ControlToValidate="ddlToamapm" EnableClientScript="false" InitialValue="0"></asp:RequiredFieldValidator>
                        </div>

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" ValidationGroup="abc" class="btn btn-primary" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" class="btn btn-primary" />
                    </div>
                </div>

            </div>

        </div>
    </div>
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                <asp:GridView ID="grdTimeSlot" runat="server" AutoGenerateColumns="false" OnRowCommand="grdTimeSlot_RowCommand" OnRowDeleting="grdTimeSlot_RowDeleting" OnRowEditing="grdTimeSlot_RowEditing" GridLines="None" CssClass="table table-striped">
                    <Columns>
                        <asp:TemplateField HeaderText="id" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblId" runat="server" Text='<%# Eval("id")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TimeSlotName">
                            <ItemTemplate>
                                <asp:Label ID="lblTimeName" runat="server" Text='<%# Eval("timeSlotName")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TimeFrom">
                            <ItemTemplate>
                                <asp:Label ID="lblFromTime" runat="server" Text='<%# Eval("timeFrom")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TimeTo">
                            <ItemTemplate>
                                <asp:Label ID="lblToTome" runat="server" Text='<%# Eval("timeTo")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnedit" runat="server" CommandName="Edit" CommandArgument='<%# Eval("id") %>' CssClass="btn btn-primary">Edit</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-primary" data-toggle="modal" OnClientClick='<%# String.Format("javascript:return DeleteAlert(\"{0}\")", Eval("id").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
     <div id="modalAlert" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Delete Confirmation</h4>
                </div>
                <div class="modal-body">
                    <h4>Are you sure want to Delete this location?</h4>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnDelete" runat="server" Text="Yes" OnClick="btnDelete_Click" class="btn btn-primary" />
                    <asp:Button ID="btnCancelDelete" runat="server" Text="Cancel" class="btn btn-primary" />
                </div>

            </div>
        </div>
    </div>
    <div id="NoDelete" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel1">Delete</h4>
                </div>
                <div class="modal-body">
                    <h4>this Location have already question, please delete question first, thanks.</h4>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="Button2" runat="server" Text="Ok" class="btn btn-primary" />
                </div>

            </div>
        </div>
    </div>
    <div>
        <asp:HiddenField ID="hdnId" runat="server" />
    </div>
  
</asp:Content>
