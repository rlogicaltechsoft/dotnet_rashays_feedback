﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RashaysWebService.Model;
namespace RashaysWebService
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string userName = txtUsername.Text;
            string password = txtPassword.Text;
            var a = SpGetData.GetDataFromSP("Logindetails", new object[,] { { "userName", "password" }, { userName, password } });
            if (a.Rows.Count > 0)
            {
                if (userName == Convert.ToString(a.Rows[0]["userName"]) && password == Convert.ToString(a.Rows[0]["password"]))
                {
                    Session["Role"] = Convert.ToString(a.Rows[0]["Role"]);
                    if (Convert.ToString(Session["Role"]) == "Admin")
                    {
                        Response.Redirect("~/Store.aspx");
                    }
                    else
                    {
                        Response.Redirect("~/Reporting.aspx");
                    }
                    //Response.Redirect("http://localhost:28961/Store.aspx");
                    //  Response.Redirect("http://feedback.rlogical.com/Store.aspx");
                    // Response.Redirect("http://rashays.rlogical.com/Store.aspx");
                    //   Response.Redirect("http://feedbackrashays.rashays.com/Store.aspx");

                }
            }
        }

        protected void btnLogin_Click1(object sender, EventArgs e)
        {

        }
    }
}