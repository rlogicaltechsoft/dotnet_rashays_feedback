﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Rashays.Master" AutoEventWireup="true" CodeBehind="ChartByHours.aspx.cs" Inherits="RashaysWebService.ChartByHours" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%-- <script src="https://code.highcharts.com/highcharts.js"></script>--%>
    <script src="../css/highchart.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>



    <script type="text/javascript">

        $(document).ready(function () {
            //var k = 11;
            //for (i = 0; i < 12; i++) {
            //    var x = new Date();
            //    var myVariable = new Date();
            //    var makeDate = new Date(myVariable);
            //    makeDate = new Date(makeDate.setMonth(makeDate.getMonth() - k));
            //    var n = makeDate.getMonth();
            //    alert(n);
            //    k--;
            //}
            //Getdata();
            GetdataStore();
            FillTime();
            // GetdataComparision();
            FillQuestionsComparison();

        });

        function FillQuestionsComparison() {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ChartByHours.aspx/FillQuestionsComparison",
                data: "",
                dataType: "json",
                success: function (data) {
                    //alert('jrd : ' + data.d);
                    var appenddata1 = "";
                    document.getElementById("ContentPlaceHolder1_ddlQuestionCmp").innerHTML = "";
                    $("#ContentPlaceHolder1_ddlQuestionCmp").append(data.d);

                }
            });
        }
        function GetComparisonData(Question) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ChartByHours.aspx/GetComparisonData",
                data: "{'Question': '" + Question + "'}",
                dataType: "json",
                success: function (data) {
                    //alert('jrd : ' + data.d);
                    var displayCnt = new Array();

                    var Series1Data = new Array();
                    var Series2Data = new Array();
                    var Series3Data = new Array();
                    for (i = 0; i < data.d.length; ++i) {
                        var displaycount = data.d[i].displayCnt;
                        var displaydays = data.d[i].displayday;
                        var displayfeedback = data.d[i].feedback;
                        //alert('count-' + displaycount + 'displaydays-' + displaydays + 'displayfeedback-' + displayfeedback);
                        Series1Data.push([displaycount, displaydays, displayfeedback]);

                    }
                    GetdataComparision(Series1Data);
                }
            });
        }
        function GetdataComparision(Series1Data) {
            //alert(Series1Data);
            var a = [];
            var count = 0;
            for (var i = 0; i < Series1Data.length; i++) {
                var n = Series1Data[i];
                n = n[1];
                //alert('a-'+ a);
                //alert('n-'+ n);
                if (a.includes(n)) {
                    a += n;
                }
                else {
                    a += n;
                    count++;
                }
            }

            var Series0Data = new Array();
            var Series2Data = new Array();
            var Series3Data = new Array();

            var arraynew = new Array();
            var af1 = "";
            var h = 0;

            var current = null;
            var cnt = 0;
            // alert(Series1Data);
            for (i = 0; i < count; i++) {
                //  alert(i);
                var tomorrow;
                af1 = Series1Data[h];
                arraynew.push(af1[1]);
                tomorrow = af1[1];
                // alert('arraynew -' + arraynew);
                var cnt = 0;
                if (af1 != null) {

                    // if (tomorrow == af1[1]) {
                    for (p = 0; p < 3; p++) {
                        af1 = Series1Data[h];
                        //alert('tomorrow- ' + tomorrow);
                        //alert('af1- ' + af1[1]);
                        if (af1 != null) {
                            // if (tomorrow == af1[1]) {
                            //alert('af1- ' + af1);
                            if (af1[2] == "1") {
                                //  alert('1- ' + af1[1]);
                                Series0Data.push(af1[0]);
                                h++;
                            }
                            else if (af1[2] == "2") {
                                //  alert('2- ' + af1[1]);
                                Series2Data.push(af1[0]);
                                h++;
                            }

                            else if (af1[2] == "3") {
                                //  alert('3- ' + af1[1]);
                                Series3Data.push(af1[0]);
                                h++;
                            }

                            // }
                        }

                    }

                    //}
                    //else {
                    //    series0data.push('');
                    //    series2data.push('');
                    //    series3data.push('');
                    //}
                }
                else {
                    Series0Data.push('');
                    Series2Data.push('');
                    Series3Data.push('');
                }
            }
            Highcharts.chart('comparision', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'By Comparison'
                },
                xAxis: {
                    categories: arraynew
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Responses'
                    }
                },
                legend: {
                    reversed: true
                },
                plotOptions: {
                    series: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    }
                },
                series: [{
                    name: 'Happy',
                    data: Series0Data
                }, {
                    name: 'Nuetral',
                    data: Series2Data
                }, {
                    name: 'Sad',
                    data: Series3Data
                }]
            });
        }


        function Getdata() {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "ChartByHours.aspx/GetcountData",
                data: "",
                dataType: "json",
                success: function (data) {
                    var displayCnt = new Array();
                    var Series1Data = new Array();
                    var Series2Data = new Array();
                    var Series3Data = new Array();

                    for (i = 0; i < data.d.length; ++i) {
                        var displaycount = data.d[i].displayCnt;
                        var displaydays = data.d[i].displayday;
                        var displayfeedback = data.d[i].feedback;
                        Series1Data.push([displaycount, displaydays, displayfeedback]);

                    }

                }
            });
        }
        function GetdataStore() {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ChartByHours.aspx/fillStoreStr",
                data: "",
                dataType: "json",
                success: function (data) {
                    var appenddata1 = "";
                    document.getElementById("ContentPlaceHolder1_ddlStore").innerHTML = "";
                    $("#ContentPlaceHolder1_ddlStore").append(data.d);

                }
            });
        }
        function FillTime() {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ChartByHours.aspx/FillTime",
                data: "",
                dataType: "json",
                success: function (data) {
                    var appenddata1 = "";
                    document.getElementById("ContentPlaceHolder1_ddlTime").innerHTML = "";
                    $("#ContentPlaceHolder1_ddlTime").append(data.d);

                }
            });
        }
        function FillQuestions(storeid, flag) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ChartByHours.aspx/FillQuestions",
                data: "{'storeid': " + storeid + ",'flag': '" + flag + "'}",
                dataType: "json",
                success: function (data) {
                    //alert('jrd : ' + data.d);
                    var appenddata1 = "";
                    document.getElementById("ContentPlaceHolder1_ddlQuestion").innerHTML = "";
                    $("#ContentPlaceHolder1_ddlQuestion").append(data.d);

                }
            });
        }


        function GetdataStorewise(storeid, flag, timeId, questionId) {
            if (questionId == "") {
                questionId = -1;
            }
            if (timeId == "") {
                timeId = -1;
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ChartByHours.aspx/GetcountDatastorewise",
                data: "{'storeid': " + storeid + " ,'flag': '" + flag + "','timeId': '" + timeId + "','questionId': '" + questionId + "'}",
                dataType: "json",
                success: function (data) {
                    var displayCnt = new Array();

                    var Series1Data = new Array();
                    var Series2Data = new Array();
                    var Series3Data = new Array();
                    for (i = 0; i < data.d.length; ++i) {
                        var displaycount = data.d[i].displayCnt;
                        var displaydays = data.d[i].displayday;
                        var displayfeedback = data.d[i].feedback;
                        // alert(displaycount);
                        Series1Data.push([displaycount, displaydays, displayfeedback]);

                    }
                    if (timeId == 1) {
                        Filldatabyhour(Series1Data);

                    }
                    else if (timeId == 2) {
                        Filldatabyweek(Series1Data);
                    }
                    else if (timeId == 3) {

                        Filldatabyweekcustom(Series1Data, timeId);
                    }
                    else if (timeId == 4) {
                        Filldatabyweekcustom(Series1Data, timeId);
                    }
                    else if (timeId == -1) {
                        Filldatabyweek(Series1Data, Series2Data, Series3Data);
                    }

                }
            });
        }
        function GetdataQuestionwise(questionId, timeId, storeId, To, From) {
           // console.log('questionId: ' + questionId + ' , timeId: ' + timeId + ', storeId: ' + storeId + ', To: ' + To + ', From: ' + From);

            if (timeId == "") {
                timeId = -1;
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ChartByHours.aspx/GetcountDataquestionwise",
                data: "{'questionId': " + questionId + ",'timeId': '" + timeId + "','storeId': '" + storeId + "','To': '" + To + "','From': '" + From + "'}",
                dataType: "json",
                success: function (data) {

                    var displayCnt = new Array();

                    var Series1Data = new Array();
                    var Series2Data = new Array();
                    var Series3Data = new Array();

                    for (i = 0; i < data.d.length; ++i) {
                        var displaycount = data.d[i].displayCnt;
                        var displaydays = data.d[i].displayday;
                        var displayfeedback = data.d[i].feedback;
                        // alert(displaycount);
                        Series1Data.push([displaycount, displaydays, displayfeedback]);

                    }
                    if (timeId == 0) {
                        Filldatabyweek(Series1Data);
                    }
                    else if (timeId == 1) {
                        Filldatabyhour(Series1Data);

                    }
                    else if (timeId == 2) {

                        Filldatabyweek(Series1Data);
                    }
                    else if (timeId == 3) {
                        //alert(Series1Data);
                        Filldatabyweekcustom(Series1Data, timeId);
                    }
                    else if (timeId == 4) {
                        Filldatabyweekcustom(Series1Data, timeId);
                    }
                    else if (timeId == -1) {
                        Filldatabyweek(Series1Data, Series2Data, Series3Data);
                    }
                    else if (timeId == 5) {
                        // document.getElementById('OpenPopup').style.display = "Block";
                        // Filldatabyweekcustom(Series1Data, timeId);

                        GetdataTimewiseChooseDate(timeId, storeId, questionId, To, From);
                    }
                }
            });
        }
        function GetdataTimewise(timeId, storeId, questionId) {
            if (questionId == "") {
                questionId = -1;
            }
            // alert('timeId ' + timeId + 'storeId ' + storeId + 'questionId ' +questionId);
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ChartByHours.aspx/GetcountDatatimewise",
                data: "{'timeId': '" + timeId + "','storeId': '" + storeId + "','questionId': '" + questionId + "'}",
                dataType: "json",
                success: function (data) {
                    var displayCnt = new Array();

                    var Series1Data = new Array();
                    var Series2Data = new Array();
                    var Series3Data = new Array();

                    for (i = 0; i < data.d.length; ++i) {

                        var displaycount = data.d[i].displayCnt;
                        var displaydays = data.d[i].displayday;
                        var displayfeedback = data.d[i].feedback;

                        Series1Data.push([displaycount, displaydays, displayfeedback]);

                    }


                    if (timeId == 1) {
                        Filldatabyhour(Series1Data);

                    }
                    else if (timeId == 2) {
                        Filldatabyweek(Series1Data);
                    }
                    else if (timeId == 3) {

                        Filldatabyweekcustom(Series1Data, timeId);
                    }
                    else if (timeId == 4) {
                        Filldatabyweekcustom(Series1Data, timeId);
                    }
                    else if (timeId == -1) {
                        Filldatabyweek(Series1Data, Series2Data, Series3Data);
                    }
                    else if (timeId == 5) {

                        //  document.getElementById('OpenPopup').style.display = "Block";

                        $('#OpenPopup').modal('toggle');
                        $('#OpenPopup').modal('show');
                    }


                }
            });
        }
        function GetdataTimewiseChooseDate(timeId, storeId, questionId, To, From) {

            if (questionId == "") {
                questionId = -1;
            }

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ChartByHours.aspx/GetcountDatatimewiseChooseDate",
                data: "{'timeId': " + timeId + ",'storeId': " + storeId + ",'questionId': " + questionId + ",'To': '" + To + "','From': '" + From + "'}",
                dataType: "json",
                success: function (data) {
                    var displayCnt = new Array();

                    var Series1Data = new Array();
                    var Series2Data = new Array();
                    var Series3Data = new Array();

                    for (i = 0; i < data.d.length; ++i) {

                        var displaycount = data.d[i].displayCnt;
                        var displaydays = data.d[i].displayday;
                        var displayfeedback = data.d[i].feedback;

                        Series1Data.push([displaycount, displaydays, displayfeedback]);

                    }

                    if (timeId == 5) {
                        Filldatabyweekcustom(Series1Data, timeId);

                    }



                }
            });
        }
        function Filldatabyweekcustom(Series1Data, timeId) {

          //  console.log('Series1Data: ' + Series1Data + ', timeId:' + timeId);

            //alert(Series1Data);
            var Series0Data = new Array();
            var Series2Data = new Array();
            var Series3Data = new Array();


            if (timeId == 3) {
                //alert('af1 -' + Series1Data);
                var k = 30;
                var arraynew = new Array();
                var af1 = "";
                var h = 0;

                var current = null;
                var cnt = 0;

                for (i = 0; i < 30; i++) {

                    var tomorrow = new Date();
                    tomorrow.setDate(tomorrow.getDate() - k);
                    var tomorrow1 = tomorrow.format("MM/dd/yyyy");
                    arraynew.push(tomorrow1);
                    k--;
                    // alert('tomorrow -' + tomorrow1);

                    af1 = Series1Data[h];
                    // alert('af1 -' + af1[1]);
                    var cnt = 0;
                    if (af1 != null) {
                        // alert('tomorrow -' + tomorrow1);
                        // alert('af1 -' + af1[1]);
                        if (tomorrow1 == af1[1]) {

                            for (var j = 0; j < Series1Data.length; j++) {


                                if (tomorrow1 == Series1Data[j][1]) {

                                    if (Series1Data[j][1] != current) {
                                        if (cnt > 0) {
                                        }
                                        current = Series1Data[j];
                                        current = current[1]
                                        cnt = 1;
                                    } else {

                                        cnt++;
                                    }
                                }
                            }
                            for (p = 0; p < 3; p++) {
                                af1 = Series1Data[h];
                                if (af1 != null) {
                                    if (tomorrow1 == af1[1]) {

                                        if (af1[2] == "1") {
                                            //  alert('1 -' + af1[1]);
                                            Series0Data.push(af1[0]);
                                            h++;
                                        }
                                        else if (af1[2] == "2") {
                                            // alert('2 -' + af1[1]);
                                            Series2Data.push(af1[0]);
                                            h++;
                                        }

                                        else if (af1[2] == "3") {
                                            //  alert('3 -' + af1[1]);
                                            Series3Data.push(af1[0]);
                                            h++;
                                        }

                                    }
                                }

                            }

                        }
                        else {
                            Series0Data.push('');
                            Series2Data.push('');
                            Series3Data.push('');
                        }
                    }
                    else {
                        Series0Data.push('');
                        Series2Data.push('');
                        Series3Data.push('');
                    }

                }
                //alert('Series0Data -' + Series0Data);
                //alert('Series2Data -' + Series2Data);
                //alert('Series3Data -' + Series3Data);
            }
            else if (timeId == 4) {
                // alert(Series1Data);
                var k = 11;
                var arraynew = new Array();
                var af1 = "";
                var h = 0;

                var current = null;
                var cnt = 0;
                var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
                ];
                for (i = 0; i < 12; i++) {
                    var myVariable = new Date();
                    var tomorrow = new Date(myVariable);

                    // alert(k);
                    tomorrow = new Date(tomorrow.setMonth(tomorrow.getMonth() - k));
                    //var  substring = "Mar 01";
                    //if (tomorrow.indexOf(substring) !== -1)
                    //{
                    //    tomorrow = tomorrow.replace("Mar 01", "Feb 28");
                    //}
                    //var n = tomorrow.includes("Mar 01");
                    //if (n == true)
                    //{
                    //    tomorrow = tomorrow.replace("Mar 01", "Feb 28");
                    //}

                    tomorrow1 = monthNames[tomorrow.getMonth()]
                    // alert(tomorrow1);
                    arraynew.push(tomorrow1);
                    //if (arraynew == "January,March")
                    //{
                    //    arraynew = "January,February";
                    //}
                    //alert(arraynew);
                    k--;
                    af1 = Series1Data[h];

                    // alert(Series1Data);
                    var cnt = 0;
                    if (af1 != null) {
                        if (tomorrow1 == af1[1]) {
                            //for (var j = 0; j < Series1Data.length; j++) {

                            //    alert(Series1Data[j][1]);
                            //    if (tomorrow1 == Series1Data[j][1]) {

                            //        if (Series1Data[j][1] != current) {
                            //            if (cnt > 0) {
                            //            }
                            //            current = Series1Data[j];
                            //            current = current[1]
                            //            cnt = 1;
                            //        } else {

                            //            cnt++;
                            //        }
                            //    }
                            //}
                            for (p = 0; p < 3; p++) {
                                af1 = Series1Data[h];
                                //  alert('main-' + af1[1]);
                                // alert('main1-' + tomorrow1);
                                if (af1 != null) {
                                    if (tomorrow1 == af1[1]) {

                                        if (af1[2] == "1") {
                                            Series0Data.push(af1[0]);
                                            h++;
                                        }
                                        else if (af1[2] == "2") {
                                            Series2Data.push(af1[0]);
                                            h++;
                                        }

                                        else if (af1[2] == "3") {
                                            Series3Data.push(af1[0]);
                                            h++;
                                        }

                                    }
                                }

                            }
                        }
                        else {
                            Series0Data.push('');
                            Series2Data.push('');
                            Series3Data.push('');
                        }
                    }
                    else {
                        Series0Data.push('');
                        Series2Data.push('');
                        Series3Data.push('');
                    }
                }

            }
            else if (timeId == 5) {
                var To = document.getElementById('txtTo').value;
                var From = document.getElementById('txtFrom').value;
                //alert('to-' + To + ' from - ' + From);
                var date1 = new Date(To);
                var date2 = new Date(From);
                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                diffDays = diffDays + 1;
                // alert(diffDays);
                var k = diffDays;
                var arraynew = new Array();
                var af1 = "";
                var h = 0;
                //alert(Series1Data);
                var current = null;
                var cnt = 0;
                for (i = 0; i < diffDays; i++) {

                    var tomorrow = new Date(To);
                    tomorrow.setDate(tomorrow.getDate() - (k - 1));
                    var tomorrow1 = tomorrow.format("dd/MM/yyyy");
                    arraynew.push(tomorrow1);
                    k--;
                    af1 = Series1Data[h];

                    var cnt = 0;
                    if (af1 != null) {
                        //  alert('tom -' +tomorrow1);
                        //alert('af -' +af1[1]);
                        if (tomorrow1 == af1[1]) {
                            for (var j = 0; j < Series1Data.length; j++) {

                                if (tomorrow1 == Series1Data[j][1]) {

                                    if (Series1Data[j][1] != current) {
                                        if (cnt > 0) {
                                        }
                                        current = Series1Data[j];
                                        current = current[1]
                                        cnt = 1;
                                    } else {

                                        cnt++;
                                    }
                                }
                            }
                            for (p = 0; p < 3; p++) {
                                af1 = Series1Data[h];
                                if (af1 != null) {
                                    if (tomorrow1 == af1[1]) {

                                        if (af1[2] == "1") {
                                            //alert(af1[1]);
                                            Series0Data.push(af1[0]);
                                            h++;
                                        }
                                        else if (af1[2] == "2") {
                                            // alert(af1[1]);
                                            Series2Data.push(af1[0]);
                                            h++;
                                        }

                                        else if (af1[2] == "3") {
                                            // alert(af1[1]);
                                            Series3Data.push(af1[0]);
                                            h++;
                                        }

                                    }
                                }

                            }

                        }
                        else {
                            Series0Data.push('');
                            Series2Data.push('');
                            Series3Data.push('');
                        }
                    }
                    else {
                        Series0Data.push('');
                        Series2Data.push('');
                        Series3Data.push('');
                    }
                }
            }
            var max = max;
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                    //<a href="http://java2s.com"> java2s.com</a>'
                },
                title: {
                    text: 'By weekday'
                },
                xAxis: {
                    categories: arraynew
                },
                yAxis: {
                    min: 0,
                    max: max,
                    title: {
                        text: 'Responses'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'top',
                    y: 25,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    }
                },
                series: [{
                    name: 'Happy',
                    data: Series0Data
                },
                {
                    name: 'Nuetral',
                    data: Series2Data
                }, {
                    name: 'Sad',
                    data: Series3Data
                }]

            });
        }
        function Filldatabyhour(Series1Data) {
            var zero = 0, one = 0, two = 0, three = 0, four = 0, five = 0, six = 0, seven = 0, eight = 0, nine = 0, ten = 0, eleven = 0, twelve = 0, thirteen = 0, fourteen, fifteen, sixteen, seventeen, eighteen, nineteen, twenty, twentone, twentytwo, twentythree;
            //var max = 50;
            var res1 = "", res2 = "", res3 = "", res4 = "", res5 = "", res6 = "", res7 = "", res8 = "", res9 = "", res10 = "", res11 = "", res12 = "", res13 = "", res14 = "", res15 = "", res16 = "", res17 = "", res18 = "", res19 = "", res20 = "", res21 = "", res22 = "", res23 = "", res24 = "", res25 = "", res26 = "", res27 = "", res28 = "", res29 = "", res30 = "", res31 = "", res32 = "", res33 = "", res34 = "", res35 = "", res36 = "", res37 = "", res38 = "", res39 = "", res40 = "", res41 = "", res42 = "", res43 = "", res44 = "", res45 = "", res46 = "", res47 = "", res48 = "", res49 = "", res50 = "", res51 = "", res52 = "", res53 = "", res54 = "", res55 = "", res56 = "", res57 = "", res58 = "", res59 = "", res60 = "", res61 = "", res62 = "", res63 = "", res64 = "", res65 = "", res66 = "", res67 = "", res68 = "", res69 = "", res70 = "", res71 = "", res72 = "";
            //alert(Series1Data);
            for (i = 0; i < 48; ++i) {
                var af1 = "";
                af1 = Series1Data[i];
                if (af1 != null) {
                    if (af1[2] == 1) {
                        //alert("first");
                        if (af1[1] == "0") {
                            res1 = af1[0];
                            zero += res1;
                        }
                        if (af1[1] == "1") {
                            res2 = af1[0];
                            one += res2;
                        }
                        if (af1[1] == "2") {
                            res3 = af1[0];
                            two += res3;
                        }
                        if (af1[1] == "3") {
                            res4 = af1[0];
                            three += res4;
                        }
                        if (af1[1] == "4") {
                            res5 = af1[0];
                            four += res5;
                        }
                        if (af1[1] == "5") {
                            res6 = af1[0];
                            five += res6;
                        }
                        if (af1[1] == "6") {
                            res7 = af1[0];
                            six += res7;
                        }
                        if (af1[1] == "7") {
                            res8 = af1[0];
                            seven += res8;
                        }
                        if (af1[1] == "8") {
                            res9 = af1[0];
                            eight += res9;
                        }
                        if (af1[1] == "9") {
                            res10 = af1[0];
                            nine += res10;
                        }
                        if (af1[1] == "10") {
                            res11 = af1[0];
                            ten += res11;
                        }
                        if (af1[1] == "11") {
                            res12 = af1[0];
                            eleven += res12;
                        }
                        if (af1[1] == "12") {
                            res13 = af1[0];
                            twelve += res13;
                        }
                        if (af1[1] == "13") {
                            res14 = af1[0];
                            eleven += res14;
                        }
                        if (af1[1] == "14") {
                            res15 = af1[0];
                            twelve += res15;
                        }
                        if (af1[1] == "15") {
                            res16 = af1[0];
                            eleven += res16;
                        }
                        if (af1[1] == "16") {
                            res17 = af1[0];
                            twelve += res17;
                        }
                        if (af1[1] == "17") {
                            res18 = af1[0];
                            eleven += res18;
                        }
                        if (af1[1] == "18") {
                            res19 = af1[0];
                            twelve += res19;
                        }
                        if (af1[1] == "19") {
                            res20 = af1[0];
                            eleven += res20;
                        }
                        if (af1[1] == "20") {
                            res21 = af1[0];
                            twelve += res21;
                        }
                        if (af1[1] == "21") {
                            res22 = af1[0];
                            eleven += res22;
                        }
                        if (af1[1] == "22") {
                            res23 = af1[0];
                            twelve += res23;
                        }
                        if (af1[1] == "23") {
                            res24 = af1[0];
                            twelve += res24;
                        }



                    }
                    if (af1[2] == 2) {
                        // alert("second")
                        if (af1[1] == "0") {
                            res25 = af1[0];
                            zero += res25;
                        }
                        if (af1[1] == "1") {
                            res26 = af1[0];
                            one += res26;
                        }
                        if (af1[1] == "2") {
                            res27 = af1[0];
                            two += res27;
                        }
                        if (af1[1] == "3") {
                            res28 = af1[0];
                            three += res28;
                        }
                        if (af1[1] == "4") {
                            res29 = af1[0];
                            four += res29;
                        }
                        if (af1[1] == "5") {
                            res30 = af1[0];
                            five += res30;
                        }
                        if (af1[1] == "6") {
                            res31 = af1[0];
                            six += res31;
                        }
                        if (af1[1] == "7") {
                            res32 = af1[0];
                            seven += res32;
                        }
                        if (af1[1] == "8") {
                            res33 = af1[0];
                            eight += res33;
                        }
                        if (af1[1] == "9") {
                            res34 = af1[0];
                            nine += res34;
                        }
                        if (af1[1] == "10") {
                            res35 = af1[0];
                            ten += res35;
                        }
                        if (af1[1] == "11") {
                            res36 = af1[0];
                            eleven += res36;
                        }
                        if (af1[1] == "12") {
                            res37 = af1[0];
                            twelve += res37;
                        }
                        if (af1[1] == "13") {
                            res38 = af1[0];
                            eleven += res38;
                        }
                        if (af1[1] == "14") {
                            res39 = af1[0];
                            twelve += res39;
                        }
                        if (af1[1] == "15") {
                            res40 = af1[0];
                            eleven += res40;
                        }
                        if (af1[1] == "16") {
                            res41 = af1[0];
                            twelve += res41;
                        }
                        if (af1[1] == "17") {
                            res42 = af1[0];
                            eleven += res42;
                        }
                        if (af1[1] == "18") {
                            res43 = af1[0];
                            twelve += res43;
                        }
                        if (af1[1] == "19") {
                            res44 = af1[0];
                            eleven += res44;
                        }
                        if (af1[1] == "20") {
                            res45 = af1[0];
                            twelve += res45;
                        }
                        if (af1[1] == "21") {
                            res46 = af1[0];
                            eleven += res46;
                        }
                        if (af1[1] == "22") {
                            res47 = af1[0];
                            twelve += res47;
                        }
                        if (af1[1] == "23") {
                            res48 = af1[0];
                            twelve += res48;
                        }
                    }

                    if (af1[2] == 3) {
                        // alert("third");
                        if (af1[1] == "0") {
                            res49 = af1[0];
                            zero += res49;
                        }
                        if (af1[1] == "1") {
                            res50 = af1[0];
                            one += res50;
                        }
                        if (af1[1] == "2") {
                            res51 = af1[0];
                            two += res51;
                        }
                        if (af1[1] == "3") {
                            res52 = af1[0];
                            three += res52;
                        }
                        if (af1[1] == "4") {
                            res53 = af1[0];
                            four += res53;
                        }
                        if (af1[1] == "5") {
                            res54 = af1[0];
                            five += res54;
                        }
                        if (af1[1] == "6") {
                            res55 = af1[0];
                            six += res55;
                        }
                        if (af1[1] == "7") {
                            res56 = af1[0];
                            seven += res56;
                        }
                        if (af1[1] == "8") {
                            res57 = af1[0];
                            eight += res57;
                        }
                        if (af1[1] == "9") {
                            res58 = af1[0];
                            nine += res58;
                        }
                        if (af1[1] == "10") {
                            res59 = af1[0];
                            ten += res59;
                        }
                        if (af1[1] == "11") {
                            res60 = af1[0];
                            eleven += res60;
                        }
                        if (af1[1] == "12") {
                            res61 = af1[0];
                            twelve += res61;
                        }
                        if (af1[1] == "13") {
                            res62 = af1[0];
                            eleven += res62;
                        }
                        if (af1[1] == "14") {
                            res63 = af1[0];
                            twelve += res63;
                        }
                        if (af1[1] == "15") {
                            res64 = af1[0];
                            eleven += res64;
                        }
                        if (af1[1] == "16") {
                            res65 = af1[0];
                            twelve += res65;
                        }
                        if (af1[1] == "17") {
                            res66 = af1[0];
                            eleven += res66;
                        }
                        if (af1[1] == "18") {
                            res67 = af1[0];
                            twelve += res67;
                        }
                        if (af1[1] == "19") {
                            res68 = af1[0];
                            eleven += res68;
                        }
                        if (af1[1] == "20") {
                            res69 = af1[0];
                            twelve += res69;
                        }
                        if (af1[1] == "21") {
                            res70 = af1[0];
                            eleven += res70;
                        }
                        if (af1[1] == "22") {
                            res71 = af1[0];
                            twelve += res71;
                        }
                        if (af1[1] == "23") {
                            res72 = af1[0];
                            twelve += res72;
                        }
                    }
                }
                else {
                    break;
                }

            }

            //if (zero < 50 && one < 50 && two < 50 && three < 50 && four < 50 && five < 50 && six < 50 && seven < 50 && eight < 50 && nine < 50 && ten < 50 && eleven < 50 && twelve < 50 && thirteen < 50 && fourteen < 50 && fifteen < 50 && sixteen < 50 && seventeen < 50 && eighteen < 50 && nineteen < 50 && twenty < 50 && twentone < 50 && twentytwo < 50 && twentythree < 50) {
            //    max = 50;
            //}
            //else if ((zero > 50 && zero < 100) || (one > 50 && one < 100) || (two > 50 && two < 100) || (three > 50 && three < 100) || (four > 50 && four < 100) || (five > 50 && five < 100) || (six > 50 && six < 100) || (seven > 50 && seven < 100) || (eight > 50 && eight < 100) || (nine > 50 && nine < 100) || (ten > 50 && ten < 100) || (eleven > 50 && eleven < 100) || (twelve > 50 && twelve < 100) || (thirteen > 50 && thirteen < 100) || (fourteen > 50 && fourteen < 100) || (fifteen > 50 && fifteen < 100) || (sixteen > 50 && sixteen < 100) || (seventeen > 50 && seventeen < 100) || (eighteen > 50 && eighteen < 100) || (nineteen > 50 && nineteen < 100) || (twenty > 50 && twenty < 100) || (twentone > 50 && twentone < 100) || (twentytwo > 50 && twentytwo < 100) || (twentythree > 50 && twentythree < 100)) {
            //    max = 100;
            //}
            //else if ((zero > 100 && zero < 150) || (one > 100 && one < 150) || (two > 100 && two < 150) || (three > 100 && three < 150) || (four > 100 && four < 150) || (five > 100 && five < 150) || (six > 100 && six < 150) || (seven > 100 && seven < 150) || (eight > 100 && eight < 150) || (nine > 100 && nine < 150) || (ten > 100 && ten < 150) || (eleven > 100 && eleven < 150) || (twelve > 100 && twelve < 150) || (thirteen > 100 && thirteen < 150) || (fourteen > 100 && fourteen < 150) || (fifteen > 100 && fifteen < 150) || (sixteen > 100 && sixteen < 150) || (seventeen > 100 && seventeen < 150) || (eighteen > 100 && eighteen < 150) || (nineteen > 100 && nineteen < 150) || (twenty > 100 && twenty < 150) || (twentone > 100 && twentone < 150) || (twentytwo > 100 && twentytwo < 150) || (twentythree > 100 && twentythree < 150)) {
            //    max = 150;
            //}
            //else if ((zero > 150 && zero < 200) || (one > 150 && one < 200) || (two > 150 && two < 200) || (three > 150 && three < 200) || (four > 150 && four < 200) || (five > 150 && five < 200) || (six > 150 && six < 200) || (seven > 150 && seven < 200) || (eight > 150 && eight < 200) || (nine > 150 && nine < 200) || (ten > 150 && ten < 200) || (eleven > 150 && eleven < 200) || (twelve > 150 && twelve < 200) || (thirteen > 150 && thirteen < 200) || (fourteen > 150 && fourteen < 200) || (fifteen > 150 && fifteen < 200) || (sixteen > 150 && sixteen < 200) || (seventeen > 150 && seventeen < 200) || (eighteen > 150 && eighteen < 200) || (nineteen > 150 && nineteen < 200) || (twenty > 150 && twenty < 200) || (twentone > 150 && twentone < 200) || (twentytwo > 150 && twentytwo < 200) || (twentythree > 150 && twentythree < 200)) {
            //    max = 200;
            //}
            //else if ((zero > 200 && zero < 250) || (one > 200 && one < 250) || (two > 200 && two < 250) || (three > 200 && three < 250) || (four > 200 && four < 250) || (five > 200 && five < 250) || (six > 200 && six < 250) || (seven > 200 && seven < 250) || (eight > 200 && eight < 250) || (nine > 200 && nine < 250) || (ten > 200 && ten < 250) || (eleven > 200 && eleven < 250) || (twelve > 200 && twelve < 250) || (thirteen > 200 && thirteen < thirteen) || (fourteen > 200 && fourteen < 250) || (fifteen > 200 && fifteen < 250) || (sixteen > 200 && sixteen < 250) || (seventeen > 200 && seventeen < 250) || (eighteen > 200 && eighteen < 250) || (nineteen > 200 && nineteen < 250) || (twenty > 200 && twenty < 250) || (twentone > 200 && twentone < 250) || (twentytwo > 200 && twentytwo < 250) || (twentythree > 200 && twentythree < 250)) {
            //    max = 250;
            //}
            var max = max;
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                    //<a href="http://java2s.com"> java2s.com</a>'
                },
                title: {
                    text: 'By weekday'
                },
                xAxis: {
                    categories: ['12AM', '1AM', '2AM', '3AM', '4AM', '5AM', '6AM', '7AM', '8AM', '9AM', '10AM', '11AM', '12PM', '1PM', '2PM', '3PM', '4PM', '5PM', '6PM', '7PM', '8PM', '9PM', '10PM', '11PM', '12PM']
                },
                yAxis: {
                    min: 0,
                    max: max,
                    title: {
                        text: 'Responses'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'top',
                    y: 25,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    }
                },
                series: [{
                    name: 'Happy',
                    data: [res1, res2, res3, res4, res5, res6, res7, res8, res9, res10, res11, res12, res13, res14, res15, res16, res17, res18, res19, res20, res21, res22, res23, res24]
                },
                {
                    name: 'Nuetral',
                    data: [res25, res26, res27, res28, res29, res30, res31, res32, res33, res34, res35, res36, res37, res38, res39, res40, res41, res42, res43, res44, res45, res46, res47, res48]
                }, {
                    name: 'Sad',
                    data: [res49, res50, res51, res52, res53, res54, res55, res56, res57, res58, res59, res60, res61, res62, res63, res64, res65, res66, res67, res68, res69, res70, res71, res72]
                }]

            });
        }

        function Filldatabyweek(Series1Data) {

            console.log('Series1Data: ' + Series1Data);
            // alert(Series1Data);
            var Series0Data = new Array();
            var Series2Data = new Array();
            var Series3Data = new Array();

            var k = 6;
            var arraynew = new Array();
            var af1 = "";
            var h = 0;

            var current = null;
            var cnt = 0;
            // alert(Series1Data);
            for (i = 0; i < 7; i++) {

                var tomorrow = new Date();
                tomorrow.setDate(tomorrow.getDate() - k);
                var tomorrow1 = tomorrow.format("dd/MM/yyyy");
                arraynew.push(tomorrow1);
                k--;

                af1 = Series1Data[h];

                var cnt = 0;
                if (af1 != null) {
                    if (tomorrow1 == af1[1]) {
                        for (var j = 0; j < Series1Data.length; j++) {


                            if (tomorrow1 == Series1Data[j][1]) {

                                if (Series1Data[j][1] != current) {
                                    if (cnt > 0) {
                                    }
                                    current = Series1Data[j];
                                    current = current[1]
                                    cnt = 1;
                                } else {

                                    cnt++;
                                }
                            }
                        }
                        for (p = 0; p < 3; p++) {
                            af1 = Series1Data[h];
                            if (af1 != null) {
                                if (tomorrow1 == af1[1]) {

                                    if (af1[2] == "1") {
                                        //alert(af1[1]);
                                        Series0Data.push(af1[0]);
                                        h++;
                                    }
                                    else if (af1[2] == "2") {
                                        // alert(af1[1]);
                                        Series2Data.push(af1[0]);
                                        h++;
                                    }

                                    else if (af1[2] == "3") {
                                        // alert(af1[1]);
                                        Series3Data.push(af1[0]);
                                        h++;
                                    }

                                }
                            }

                        }

                    }
                    else {
                        Series0Data.push('');
                        Series2Data.push('');
                        Series3Data.push('');
                    }
                }
                else {
                    Series0Data.push('');
                    Series2Data.push('');
                    Series3Data.push('');
                }
            }

            var max = max;
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                    //<a href="http://java2s.com"> java2s.com</a>'
                },
                title: {
                    text: 'By weekday'
                },
                xAxis: {
                    categories: arraynew
                },
                yAxis: {
                    min: 0,
                    max: max,
                    title: {
                        text: 'Responses'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'top',
                    y: 25,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    }
                },
                series: [{
                    name: 'Happy',
                    data: Series0Data
                },
                {
                    name: 'Nuetral',
                    data: Series2Data
                }, {
                    name: 'Sad',
                    data: Series3Data
                }]

            });
        }

        function GetStoreId(ddlStore) {
            var timeId = document.getElementById("hdnTime").value
            var questionId = document.getElementById("hdnQuestion").value;
            var selected = $(ddlStore).find('option:selected');
            var extra = selected.data('flg');
            //alert('1. ' + extra);
            var selectedText = ddlStore.options[ddlStore.selectedIndex].innerHTML;
            var selectedValue = ddlStore.value;
           // alert(timeId);
            document.getElementById("hdnStore").value = selectedValue;
            GetdataStorewise(selectedValue, extra, timeId, questionId);
            FillQuestions(selectedValue, extra)
        }
        function GetQuestionId(ddlQuestion) {
            var selectedText = ddlQuestion.options[ddlQuestion.selectedIndex].innerHTML;
            var selectedValue = ddlQuestion.value;
            document.getElementById("hdnQuestion").value = selectedValue;
            //alert("Selected Text: " + selectedText + " Value: " + selectedValue);
            // alert(selectedValue);
            var storeId = document.getElementById("hdnStore").value;
            var timeId = document.getElementById("hdnTime").value
            var To = document.getElementById("hdnTo").value;
            var From = document.getElementById("hdnFrom").value
            //alert(timeId);
            GetdataQuestionwise(selectedValue, timeId, storeId, To, From);
        }
        function GetTimeId(ddlTime) {
            var storeId = document.getElementById("hdnStore").value;
            var questionId = document.getElementById("hdnQuestion").value;
            var selectedText = ddlTime.options[ddlTime.selectedIndex].innerHTML;
            var selectedValue = ddlTime.value;
            document.getElementById("hdnTime").value = selectedValue;
            // alert("Selected Text: " + selectedText + " Value: " + selectedValue);
            if (selectedValue == 5) {

                // document.getElementById('OpenPopup').style.display = "Block";
                $('#OpenPopup').modal('toggle');
                $('#OpenPopup').modal('show');
                // alert(3);
                return false;
            }
            GetdataTimewise(selectedValue, storeId, questionId);
        }

        function GetQuestionIdAll(ddlQuestionCmp) {
            var selectedText = ddlQuestionCmp.options[ddlQuestionCmp.selectedIndex].innerHTML;
            // var selectedValue = ddlQuestionCmp.value;
            // alert(selectedText);
            GetComparisonData(selectedText);
        }
        function btnclick() {
            var To = document.getElementById('txtTo').value;
            var From = document.getElementById('txtFrom').value;
            document.getElementById("hdnTo").value = To;
            document.getElementById("hdnFrom").value = From;
            var storeId = document.getElementById("hdnStore").value;
            var questionId = document.getElementById("hdnQuestion").value;
            var timeId = document.getElementById("hdnTime").value
            //alert("Selected Text: " + To + " value: " + From);
           // document.getElementById('OpenPopup').style.display = "None";
            $('#OpenPopup').modal('toggle');
            GetdataTimewiseChooseDate(timeId, storeId, questionId, To, From);
        }
        function FilldataOnLoad(Series1Data) {
            var mon = 0, tue = 0, wed = 0, thu = 0, fri = 0, sat = 0;
            var max = 100;
            var res1 = "", res2 = "", res3 = "", res4 = "", res5 = "", res6 = "", res7 = "", res8 = "", res9 = "", res10 = "", res11 = "", res12 = "", res13 = "", res14 = "", res15 = "", res16 = "", res17 = "", res18 = "";
            //alert(Series1Data);
            for (i = 0; i < 18; ++i) {
                var af1 = "";
                af1 = Series1Data[i];
                if (af1 != null) {
                    if (af1[2] == 1) {
                        //alert("first");
                        if (af1[1] == "Monday") {
                            res1 = af1[0];
                            mon += res1;
                        }
                        if (af1[1] == "Tuesday") {
                            res2 = af1[0];
                            tue += res2;
                        }
                        if (af1[1] == "Wednesday") {
                            res3 = af1[0];
                            wed += res3;
                        }
                        if (af1[1] == "Thursday") {
                            res4 = af1[0];
                            thu += res4;
                        }
                        if (af1[1] == "Friday") {
                            res5 = af1[0];
                            fri += res5;
                        }
                        if (af1[1] == "Saturday") {
                            res6 = af1[0];
                            sat += res6;
                        }
                    }
                    if (af1[2] == 2) {
                        // alert("second")
                        if (af1[1] == "Monday") {
                            res7 = af1[0];
                            mon += res7;
                        }
                        if (af1[1] == "Tuesday") {
                            res8 = af1[0];
                            tue += res8;
                        }
                        if (af1[1] == "Wednesday") {
                            res9 = af1[0];
                            wed += res9;
                        }
                        if (af1[1] == "Thursday") {
                            res10 = af1[0];
                            thu += res10;
                        }
                        if (af1[1] == "Friday") {
                            res11 = af1[0];
                            fri += res11;
                        }
                        if (af1[1] == "Saturday") {
                            res12 = af1[0];
                            sat += res12;
                        }
                    }

                    if (af1[2] == 3) {
                        // alert("third");
                        if (af1[1] == "Monday") {
                            res13 = af1[0];
                            mon += res13;
                        }
                        if (af1[1] == "Tuesday") {
                            res14 = af1[0];
                            tue += res14;
                        }
                        if (af1[1] == "Wednesday") {
                            res15 = af1[0];
                            wed += res15;
                        }
                        if (af1[1] == "Thursday") {
                            res16 = af1[0];
                            thu += res16;
                        }
                        if (af1[1] == "Friday") {
                            res17 = af1[0];
                            fri += res17;
                        }
                        if (af1[1] == "Saturday") {
                            res18 = af1[0];
                            sat += res18;
                        }
                    }
                }
                else {
                    break;
                }

            }

            //if (mon < 50 && tue < 50 && wed < 50 && thu < 50 && fri < 50 && sat < 50) {
            //    max = 50;
            //}
            //else if ((mon > 50 && mon < 100) || (tue > 50 && tue < 100) || (wed > 50 && wed < 100) || (thu > 50 && thu < 100) || (fri > 50 && fri < 100) || (sat > 50 && sat < 100)) {
            //    max = 100;
            //}
            //else if ((mon > 100 && mon < 150) || (tue > 100 && tue < 150) || (wed > 100 && wed < 150) || (thu > 100 && thu < 150) || (fri > 100 && fri < 150) || (sat > 100 && sat < 150)) {
            //    max = 150;
            //}
            //else if ((mon > 150 && mon < 200) || (tue > 150 && tue < 200) || (wed > 150 && wed < 200) || (thu > 150 && thu < 200) || (fri > 150 && fri < 200) || (sat > 150 && sat < 200)) {
            //    max = 200;
            //}
            //else if ((mon > 200 && mon < 250) || (tue > 200 && tue < 250) || (wed > 200 && wed < 250) || (thu > 200 && thu < 250) || (fri > 200 && fri < 250) || (sat > 200 && sat < 250)) {
            //    max = 250;
            //}
            var max = max;
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                    //<a href="http://java2s.com"> java2s.com</a>'
                },
                title: {
                    text: 'By weekday'
                },
                xAxis: {
                    categories: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
                },
                yAxis: {
                    min: 0,
                    max: max,
                    title: {
                        text: 'Responses'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'top',
                    y: 25,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    }
                },
                series: [{
                    name: 'Happy',
                    data: [res1, res2, res3, res4, res5, res6]
                },
                {
                    name: 'Nuetral',
                    data: [res7, res8, res9, res10, res11, res12]
                }, {
                    name: 'Sad',
                    data: [res13, res14, res15, res16, res17, res18]
                }]

            });
        }


    </script>

    <script type="text/javascript">

        //$(document).ready(function () {
        //    //Create groups for dropdown list
        //    $("select#ddlStore option[classification='LessThanFifty']").wrapAll("<optgroup label='Less than fifty'>");
        //    $("select#ddlStore option[classification='GreaterThanFifty']").wrapAll("<optgroup label='Greater than fifty'>");
        //});

    </script>
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet" />
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- Chart.js -->
        <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
        <!-- gauge.js -->
        <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
        <!-- bootstrap-progressbar -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="form-horizontal form-label-left">

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Select Location
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:DropDownList ID="ddlStore" runat="server" CssClass="select2_single form-control" onchange="GetStoreId(this)">
                            <asp:ListItem Text="--Select Store--" Value="0" />
                        </asp:DropDownList>
                    </div>

                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Select Question
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:DropDownList ID="ddlQuestion" runat="server" CssClass="select2_single form-control" onchange="GetQuestionId(this)">
                            <asp:ListItem Text="--Select Question--" Value="0" />
                        </asp:DropDownList>
                    </div>

                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Select Time
                       <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:DropDownList ID="ddlTime" runat="server" CssClass="select2_single form-control" onchange="GetTimeId(this)">
                            <asp:ListItem Text="--Select Time--" Value="0" />
                        </asp:DropDownList>
                    </div>

                </div>
                <input type="hidden" id="hdnStore" />
                <input type="hidden" id="hdnQuestion" />
                <input type="hidden" id="hdnTime" />
                <input type="hidden" id="hdnTo" />
                <input type="hidden" id="hdnFrom" />

                <div>
                </div>
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                <div style="margin-left: 50px">
                    <h1>Comparison</h1>
                </div>
                <div style="margin-left: 50px">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                            Select Question
                       <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <asp:DropDownList ID="ddlQuestionCmp" runat="server" CssClass="select2_single form-control" onchange="GetQuestionIdAll(this)">
                                <asp:ListItem Text="--Select Question--" Value="0" />
                            </asp:DropDownList>
                        </div>

                    </div>


                </div>
                <div id="comparision" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </div>
        </div>
    </div>
    <%--  <div id="OpenPopup1" style="display: none">
        <div id="SubDivDetails">
            <table>
                <tr>
                    <td>From
                    </td>
                    <td>
                        <%-- <input type="date" data-date-inline-picker="true" id="txtFrom" />
                    </td>
                </tr>
                &nbsp;
                <tr>
                    <td>To
                    </td>
                    <td>
                        <input type="date" data-date-inline-picker="true" id="txtTo" />
                    </td>
                </tr>

                &nbsp;
                <tr>
                    <td>
                        <input type="button" value="Ok" onclick="btnclick();" />
                    </td>
                </tr>
            </table>
        </div>
    </div>--%>
    <div id="OpenPopup" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" >
        <div class="modal-dialog modal-lg" style="width:350px !important;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Select Date</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="form-horizontal form-label-left">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                        From
                            <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="date" data-date-inline-picker="true" id="txtFrom" />
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                        To
                            <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="date" data-date-inline-picker="true" id="txtTo" />
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <input type="button" value="Ok" onclick="btnclick();" class="btn btn-primary" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>

            </div>
        </div>
    </div>
  

</asp:Content>


