﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Rashays.Master" AutoEventWireup="true" CodeBehind="Store.aspx.cs" Inherits="RashaysWebService.Store" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .pagination {
            display: inline-block;
        }

            .pagination a {
                color: black;
                float: left;
                padding: 8px 16px;
                text-decoration: none;
            }

                .pagination a.active {
                    background-color: #4CAF50;
                    color: white;
                }
    </style>
    <script type="text/javascript">
        function validate() {
            var summary = "";
            summary += isvalidstore();
            if (summary != "") {
                alert(summary);
                return false;
            }
            else {
                return true;
            }
        }
        function isvalidstore() {
            var uid;
            var temp = document.getElementById("<%=txtStore.ClientID %>");
            uid = temp.value;
            if (uid == "") {
                return ("Please Enter Store Name" + "\n");
            }
            else {
                return "";
            }
        }

        function DeleteAlert(objid) {
            $('#ContentPlaceHolder1_hdnId').val(objid);
            $('#modalAlert').modal('toggle');
            $('#modalAlert').modal('show');
            return false;
        }

        function NoDelete() {
            $('#NoDelete').modal('toggle');
            $('#NoDelete').modal('show');
        }
        //function checkVal() {
        //    var store = $("#txtStore").val();
        //    alert(store);
        //    if (store == "") {
        //        alert('hi');
        //        $("divStore").show();
        //        return false;
        //    }
        //}

    </script>
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet" />
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- Chart.js -->
        <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
        <!-- gauge.js -->
        <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
        <!-- bootstrap-progressbar -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="form-horizontal form-label-left">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Store Name 
                            <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:TextBox ID="txtStore" runat="server" class="form-control col-md-7 col-xs-12" ValidationGroup="abc"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="Please enter store" ForeColor="Red" ValidationGroup="abc" ControlToValidate="txtStore" EnableClientScript="false"></asp:RequiredFieldValidator>

                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Test Mail
                            <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:TextBox ID="txttestmail" runat="server" class="form-control col-md-7 col-xs-12"></asp:TextBox>

                    </div>

                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <asp:Button ID="btnMail" runat="server" Text="Test Mail" OnClick="btnMail_Click" class="btn btn-primary" />
                        <asp:Label ID="lblid" runat="server" ForeColor="Green"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Email1 
                            <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:TextBox ID="txtEmail1" runat="server" class="form-control col-md-7 col-xs-12"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic" ErrorMessage="Please enter email" ForeColor="Red" ValidationGroup="abc" ControlToValidate="txtEmail1" EnableClientScript="false"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail1" ValidationGroup="abc" ErrorMessage="Invalid Email Format" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Email2 
                      
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:TextBox ID="txtEmail2" runat="server" class="form-control col-md-7 col-xs-12"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail2" ErrorMessage="Invalid Email Format" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Email3 
                      
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:TextBox ID="txtEmail3" runat="server" class="form-control col-md-7 col-xs-12"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail3" ErrorMessage="Invalid Email Format" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" ValidationGroup="abc" class="btn btn-primary" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" class="btn btn-primary" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                <%--<div class="pagination" style="text-align: left !important;">--%>
                <asp:GridView ID="grdStore" runat="server" AutoGenerateColumns="false" OnRowCommand="grdStore_RowCommand" GridLines="None" CssClass="table table-striped" OnRowDeleting="grdStore_RowDeleting" OnRowEditing="grdStore_RowEditing">
                    <Columns>
                        <asp:TemplateField HeaderText="id" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblId" runat="server" Text='<%# Eval("id")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Store">
                            <ItemTemplate>
                                <asp:Label ID="lblstore" runat="server" Text='<%# Eval("title")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email1">
                            <ItemTemplate>
                                <asp:Label ID="lblEamil1" runat="server" Text='<%# Eval("email1")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email2">
                            <ItemTemplate>
                                <asp:Label ID="lblEmail2" runat="server" Text='<%# Eval("email2")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email3">
                            <ItemTemplate>
                                <asp:Label ID="lblEmail3" runat="server" Text='<%# Eval("email3")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnedit" runat="server" CommandName="Edit" CommandArgument='<%# Eval("id") %>' CssClass="btn btn-primary">Edit</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-primary" data-toggle="modal" OnClientClick='<%# String.Format("javascript:return DeleteAlert(\"{0}\")", Eval("id").ToString()) %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <%--</div>--%>
            </div>
        </div>
    </div>

    <div id="modalAlert" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Delete Confirmation</h4>
                </div>
                <div class="modal-body">
                    <h4>Are you sure want to Delete this store?</h4>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnDelete" runat="server" Text="Yes" OnClick="btnDelete_Click" class="btn btn-primary" />
                    <asp:Button ID="btnCancelDelete" runat="server" Text="Cancel" class="btn btn-primary" />
                </div>

            </div>
        </div>
    </div>
    <div id="NoDelete" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel1">Delete</h4>
                </div>
                <div class="modal-body">
                    <h4>this Store have already question, please delete question first, thanks.</h4>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="Button2" runat="server" Text="Ok" class="btn btn-primary" />
                </div>

            </div>
        </div>
    </div>
    <div>
        <asp:HiddenField ID="hdnId" runat="server" />
    </div>
</asp:Content>
