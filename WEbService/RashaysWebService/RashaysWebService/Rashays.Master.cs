﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RashaysWebService.Model;

namespace RashaysWebService
{
    public partial class Rashays : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string currentPage = System.IO.Path.GetFileName(Request.Url.AbsolutePath);
            currentPage = currentPage.Substring(0, currentPage.IndexOf("."));
            lblformName.Text = currentPage;
           
            if (Convert.ToString(Session["Role"]) == "Marketing")
            {
                Store.Visible = false;
                Location.Visible = false;
                Question.Visible = false;
                TimeSlot.Visible = false;
                FeedBack.Visible = false;
                Notification.Visible = false;
                Charts.Visible = false;
                AdminRole.Visible = false;
            }
        }
    }
}