﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RashaysWebService.Model;
namespace RashaysWebService
{
    public partial class Notification : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                fillStore();
                Bindnotification();
            }

        }
        public void fillStore()
        {
            var a = SpGetData.GetDataFromSP("GetStoreListing", new object[,] { { }, { } });

            ddlStore1.DataSource = a;
            ddlStore1.DataTextField = "title";
            ddlStore1.DataValueField = "id";
            ddlStore1.DataBind();
            ddlStore1.Items.Insert(0, new ListItem("--Select Store--", "0"));
        }
        public void fillLocation(int storeId)
        {
            var a = SpGetData.GetDataFromSP("GetlocationListing", new object[,] { { "storeId" }, { storeId } });

            ddlLocation1.DataSource = a;
            ddlLocation1.DataTextField = "location";
            ddlLocation1.DataValueField = "id";
            ddlLocation1.DataBind();
            ddlLocation1.Items.Insert(0, new ListItem("--Select Location--", "0"));
        }
        public void Bindnotification()
        {
            var a = SpGetData.GetDataFromSP("GetNOtification", new object[,] { { }, { } });
            grdNotification.DataSource = a;
            grdNotification.DataBind();
        }
        public void Bindnotificationminutewise(int minutes)
        {
            var a = SpGetData.GetDataFromSP("GetMinutewiseFeedback", new object[,] { { "minutes" }, { minutes } });
            grdNotification.DataSource = a;
            grdNotification.DataBind();
        }
        protected void ddlStore1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int storeId = Convert.ToInt32(ddlStore1.SelectedValue);
            fillLocation(storeId);
        }

        public void clearField()
        {
            ddlStore1.SelectedIndex = 0;
            ddlLocation1.SelectedIndex = 0;
            txtMinutes.Text = "";
            txtCount.Text = "";
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (Convert.ToString(ViewState["id"]) == "")
                {
                    try
                    {
                        int storeId = Convert.ToInt32(ddlStore1.SelectedValue);
                        int locationId = Convert.ToInt32(ddlLocation1.SelectedValue);
                        int minutes = Convert.ToInt32(txtMinutes.Text);
                        int count = Convert.ToInt32(txtCount.Text);
                        var a = SpGetData.GetDataFromSP("insertNotification", new object[,] { { "storeId", "locationId", "minutes", "count" }, { storeId, locationId, minutes, count } });
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "Loading", "javascript:saveMessage();", true);
                        Bindnotification();
                        clearField();
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    try
                    {
                        int id = Convert.ToInt32(ViewState["id"]);
                        int storeId = Convert.ToInt32(ddlStore1.SelectedValue);
                        int locationId = Convert.ToInt32(ddlLocation1.SelectedValue);
                        int minutes = Convert.ToInt32(txtMinutes.Text);
                        int count = Convert.ToInt32(txtCount.Text);
                        var a = SpGetData.GetDataFromSP("updateNotification", new object[,] { { "id","storeId", "locationId", "minutes", "count" }, { id,storeId, locationId, minutes, count } });
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "Loading", "javascript:saveMessage();", true);
                        Bindnotification();
                        clearField();
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            
        }

        protected void grdNotification_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int id = Convert.ToInt32(e.CommandArgument.ToString());
                ViewState["id"] = id;
                var a = SpGetData.GetDataFromSP("GetIdNotification", new object[,] { { "id" }, { id } });
                ddlStore1.SelectedValue = Convert.ToString(a.Rows[0]["storeId"]);
                fillLocation(Convert.ToInt32(ddlStore1.SelectedValue));
                ddlLocation1.SelectedValue = Convert.ToString(a.Rows[0]["locationId"]);
                txtMinutes.Text = Convert.ToString(a.Rows[0]["minutes"]);
                txtCount.Text = Convert.ToString(a.Rows[0]["count"]);
                btnSave.Text = "Update";
                btnCancel.Visible = true;
            }
            else
            {
                //int id = Convert.ToInt32(e.CommandArgument.ToString());
                //var a = SpGetData.GetDataFromSP("DeleteNotification", new object[,] { { "id" }, { id } });
                //Bindnotification();
            }
        }

        protected void grdNotification_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void grdNotification_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            clearField();
            btnSave.Text = "Save";
            btnCancel.Visible = false;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(this.hdnId.Value);
            var a = SpGetData.GetDataFromSP("DeleteNotification", new object[,] { { "id" }, { id } });
            Bindnotification();
        }
    }
}