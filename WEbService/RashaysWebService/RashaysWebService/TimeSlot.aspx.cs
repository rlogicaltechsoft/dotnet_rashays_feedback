﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RashaysWebService.Model;

namespace RashaysWebService
{
    public partial class TimeSlot : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //ddlFrom.Items.Insert(0, new ListItem("--Select time--", "0"));
                BindTimeSlot();
                //BindAMPM();
                btnCancel.Visible = false;

            }
        }

        public void BindAMPM()
        {
            ddlFromampm.DataSource = Enum.GetNames(typeof(EnumAmPm));
            ddlFromampm.DataBind();
            ddlToamapm.DataSource = Enum.GetNames(typeof(EnumAmPm));
            ddlToamapm.DataBind();
        }
        //public void Bindtime()
        //{
        //    //ddlFrom.DataSource = Enum.GetNames(typeof(EnumAmPm));
        //    //ddlFrom.DataBind();
        //    //ddlTo.DataSource = Enum.GetNames(typeof(EnumAmPm));
        //    //ddlTo.DataBind();
        //}
        public void BindTimeSlot()
        {
            try
            {
                var a = SpGetData.GetDataFromSP("BindGridTimeSlot", new object[,] { { }, { } });
                grdTimeSlot.DataSource = a;
                grdTimeSlot.DataBind();
            }
            catch (Exception ex)
            {

            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (Convert.ToString(ViewState["id"]) == "")
            {
                if (txtName.Text != "" && ddlFrom.SelectedValue != "0" && ddlFromampm.SelectedValue != "0" && ddlTo.SelectedValue != "0" && ddlToamapm.SelectedValue != "0")
                {
                    try
                    {
                        string timeSlotName = txtName.Text;
                        string timeFrom = ddlFrom.SelectedItem.Text;

                        string fromampm = ddlFromampm.SelectedItem.Text;
                        timeFrom = timeFrom + " " + fromampm;
                        if (timeFrom == "12:00 PM")
                        {
                            timeFrom = "11:59 PM";
                        }
                        if (timeFrom == "12:00 AM")
                        {
                            timeFrom = "12:01 AM";
                        }
                        string timeTo = ddlTo.SelectedItem.Text;
                        string toampm = ddlToamapm.SelectedItem.Text;
                        timeTo = timeTo + " " + toampm;
                        if (timeTo == "12:00 PM")
                        {
                            timeTo = "11:59 PM";
                        }
                        if (timeTo == "12:00 AM")
                        {
                            timeTo = "12:01 AM";
                        }
                        var a = SpGetData.GetDataFromSP("insertTimeSlot", new object[,] { { "timeSlotName", "timeFrom", "timeTo" }, { timeSlotName, timeFrom, timeTo } });
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "Loading", "javascript:saveMessage();", true);
                        BindTimeSlot();

                        // BindAMPM();
                        clearField();
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            else
            {
                if (Convert.ToString(ViewState["id"]) != "")
                {
                    if (ddlFrom.SelectedValue != "0")
                    {
                        if (ddlFromampm.SelectedValue != "0")
                        {
                            if (ddlTo.SelectedValue != "0")
                            {
                                if (ddlToamapm.SelectedValue != "0")
                                {
                                    //if (txtName.Text != "" || ddlFrom.SelectedValue != "0" || ddlFromampm.SelectedValue != "0" || ddlTo.SelectedValue != "0" || ddlToamapm.SelectedValue != "0")
                                    if (txtName.Text != "")
                                    {
                                        try
                                        {
                                            int id = Convert.ToInt32(ViewState["id"]);
                                            string timeSlotName = txtName.Text;
                                            string timeFrom = ddlFrom.SelectedItem.Text;
                                            string fromampm = ddlFromampm.SelectedItem.Text;
                                            timeFrom = timeFrom + " " + fromampm;
                                            //if (timeFrom == "12:00 PM")
                                            //{
                                            //    timeFrom = "11:59 PM";
                                            //}
                                            if (timeFrom == "12:00 AM")
                                            {
                                                timeFrom = "11:59 AM";
                                            }
                                            string timeTo = ddlTo.SelectedItem.Text;
                                            string toampm = ddlToamapm.SelectedItem.Text;
                                            timeTo = timeTo + " " + toampm;
                                            if (timeTo == "12:00 PM")
                                            {
                                                timeTo = "11:59 PM";
                                            }
                                            if (timeTo == "12:00 AM")
                                            {
                                                timeTo = "12:01 AM";
                                            }
                                            var a = SpGetData.GetDataFromSP("updateTimeSlot", new object[,] { { "id", "timeSlotName", "timeFrom", "timeTo" }, { id, timeSlotName, timeFrom, timeTo } });
                                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "Loading", "javascript:updateMessage();", true);

                                            btnSave.Text = "Save";
                                            ViewState["id"] = "";
                                            //RequiredFieldValidator1.Visible = false;
                                            //RequiredFieldValidator2.Visible = false;
                                            //RequiredFieldValidator3.Visible = false;
                                            //RequiredFieldValidator4.Visible = false;
                                            btnCancel.Visible = false;
                                            BindTimeSlot();
                                            //ddlFrom.Items.Insert(0, new ListItem("--SelectTime--", "0"));
                                            //ddlTo.Items.Insert(0, new ListItem("--SelectTime--", "0"));
                                            clearField();
                                            // BindAMPM();

                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        public void clearField()
        {
            txtName.Text = "";
            //ddlFrom.SelectedValue = -1;
            ddlFrom.SelectedValue = Convert.ToString(0);
            // ddlFromampm.SelectedValue = Convert.ToString(ddlFromampm.SelectedValue);
            ddlTo.SelectedValue = Convert.ToString(0);
            //ddlToamapm.SelectedValue = Convert.ToString((EnumAmPm)Enum.Parse(typeof(EnumAmPm), ddlToamapm.SelectedValue));
            ddlFromampm.SelectedValue = Convert.ToString(0);
            ddlToamapm.SelectedValue = Convert.ToString(0);
        }

        protected void grdTimeSlot_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int id = Convert.ToInt32(e.CommandArgument.ToString());
                ViewState["id"] = id;
                var a = SpGetData.GetDataFromSP("GetIdTimeslot", new object[,] { { "id" }, { id } });
                txtName.Text = Convert.ToString(a.Rows[0]["timeSlotName"]);
                string timeFrom = Convert.ToString(a.Rows[0]["timeFrom"]);
                string fromAmpm = timeFrom.Substring(timeFrom.Length - 2);
                timeFrom = timeFrom.Remove(timeFrom.Length - 2).Trim();
                string timeTo = Convert.ToString(a.Rows[0]["timeTo"]);
                string toAmpm = timeTo.Substring(timeTo.Length - 2);
                timeTo = timeTo.Remove(timeTo.Length - 2).Trim();
                //returnIdOnValue(timeFrom);
                //ddlFrom.SelectedItem.Text = timeFrom;
                ddlFrom.SelectedValue = returnIdOnValueTme(timeFrom);
                ddlFromampm.SelectedValue = returnIdOnValueAmPm(fromAmpm);
                ddlTo.SelectedValue = returnIdOnValueTme(timeTo);
                ddlToamapm.SelectedValue = returnIdOnValueAmPm(toAmpm);
                btnSave.Text = "Update";
                btnCancel.Visible = true;
                ddlTo.Enabled = true;
                ddlToamapm.Enabled = true;
            }
            else
            {
                //int id = Convert.ToInt32(e.CommandArgument.ToString());
                //var a = SpGetData.GetDataFromSP("DeleteTimeSlot", new object[,] { { "id" }, { id } });
                //BindTimeSlot();
                //clearField();
            }
        }

        protected void grdTimeSlot_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void grdTimeSlot_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            clearField();
            btnSave.Text = "Save";
            btnCancel.Visible = false;
            ddlToamapm.Items.Insert(1, new ListItem("AM", ""));
            ViewState["id"] = "";
        }

        protected void ddlTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if(Convert.ToInt32(ddlFrom.SelectedValue) >3)
            //{
            if (Convert.ToInt32(ddlFromampm.SelectedValue) == 2)
            {
                //if (Convert.ToInt32(ddlTo.SelectedValue) > 3 && Convert.ToInt32(ddlTo.SelectedValue) < 12)
                //{
                //ddlToamapm.Items.Remove(ddlToamapm.Items.FindByValue("AM"));
                if (Convert.ToInt32(ddlTo.SelectedValue) != 12)
                {
                    if (ddlToamapm.Items.Count != 2)
                    {
                        ddlToamapm.Items.Remove(ddlToamapm.Items[1]);
                    }
                    else
                    {
                        //if (ddlToamapm.Items.Count == 2)
                        //{
                            ddlToamapm.Items.Insert(2, new ListItem("PM", ""));
                       // }
                    }
                }
                //else
                //{
                //    if (ddlToamapm.Items.Count != 2)
                //    {
                //        ddlToamapm.Items.Remove(ddlToamapm.Items[2]);
                //    }
                //}
                if (Convert.ToInt32(ddlTo.SelectedValue) == 12)
                {
                    if (ddlToamapm.Items.Count == 2)
                    {
                        ddlToamapm.Items.Insert(1, new ListItem("AM", ""));

                    }
                    if (ddlToamapm.Items.Count != 2)
                    {
                        ddlToamapm.Items.Remove(ddlToamapm.Items[2]);
                    }
                }
            }
            //}


        }

        protected void ddlFromampm_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlFromampm.SelectedValue) > 3 && Convert.ToInt32(ddlFromampm.SelectedValue) == 2)
            {
                //ddlTo.Items.Remove(ddlTo.Items[0]);
                ddlTo.Items.Remove(ddlTo.Items[1]);
                ddlTo.Items.Remove(ddlTo.Items[1]);
                ddlTo.Items.Remove(ddlTo.Items[1]);
            }
            if (ddlTo.Items.Count == 10 && Convert.ToInt32(ddlFromampm.SelectedValue) == 1)
            {
                ddlTo.Items.Insert(1, new ListItem("01:00", ""));
                ddlTo.Items.Insert(2, new ListItem("02:00", ""));
                ddlTo.Items.Insert(3, new ListItem("03:00", ""));
            }

            if (Convert.ToInt32(ddlFrom.SelectedValue) != 0 && Convert.ToInt32(ddlFromampm.SelectedValue) != 0)
            {
                ddlTo.Enabled = true;
                ddlToamapm.Enabled = true;
            }
            if (Convert.ToInt32(ddlFromampm.SelectedValue) == 1 && ddlToamapm.Items.Count == 2)
            {
                ddlToamapm.Items.Insert(1, new ListItem("AM", ""));
            }
        }

        public enum EnumAmPm
        {
            SelectAmPm = 0,
            AM,
            PM
        }

        public string returnIdOnValueTme(string txtValue)
        {
            string returnString = "";

            switch (txtValue)
            {
                case "01:00":
                    return "1";
                    break;
                case "02:00":
                    return "2";
                    break;
                case "03:00":
                    return "3";
                    break;
                case "04:00":
                    return "4";
                    break;
                case "05:00":
                    return "5";
                    break;
                case "06:00":
                    return "6";
                    break;
                case "07:00":
                    return "7";
                    break;
                case "08:00":
                    return "8";
                    break;
                case "09:00":
                    return "9";
                    break;
                case "10:00":
                    return "10";
                    break;
                case "11:00":
                    return "11";
                    break;
                case "12:00":
                    return "12";
                    break;
                case "11:59":
                    return "12";
                    break;
                case "12:01":
                    return "12";
                    break;
            }

            return returnString;
        }

        public string returnIdOnValueAmPm(string txtValue)
        {
            string returnString = "";

            switch (txtValue)
            {
                case "AM":
                    return "1";
                    break;
                case "PM":
                    return "2";
                    break;

            }

            return returnString;
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            //HiddenField1.Value
            int id = Convert.ToInt32(hdnId.Value);
            var a = SpGetData.GetDataFromSP("DeleteTimeSlot", new object[,] { { "id" }, { id } });
            BindTimeSlot();
            clearField();
        }

    }
}

