﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RashaysWebService.Model;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Net.Mail;

namespace RashaysWebService
{
    public partial class Store : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindStore();
                btnCancel.Visible = false;
            }
        }

        public void BindStore()
        {
            try
            {
                var a = SpGetData.GetDataFromSP("BindGridStore", new object[,] { { }, { } });
                grdStore.DataSource = a;
                grdStore.DataBind();
            }
            catch (Exception ex)
            {

            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (Convert.ToString(ViewState["id"]) == "")
                {
                    try
                    {
                        string store = txtStore.Text;
                        string email1 = txtEmail1.Text;
                        string email2 = txtEmail2.Text;
                        string email3 = txtEmail3.Text;
                        var a = SpGetData.GetDataFromSP("insertStore", new object[,] { { "title", "email1", "email2", "email3" }, { store, email1, email2, email3 } });
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "Loading", "javascript:saveMessage();", true);
                        BindStore();
                        clearField();
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    try
                    {
                        int id = Convert.ToInt32(ViewState["id"]);
                        string store = txtStore.Text;
                        string email1 = txtEmail1.Text;
                        string email2 = txtEmail2.Text;
                        string email3 = txtEmail3.Text;
                        var a = SpGetData.GetDataFromSP("updateStore", new object[,] { { "id", "title", "email1", "email2", "email3" }, { id, store, email1, email2, email3 } });
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "Loading", "javascript:updateMessage();", true);
                        BindStore();
                        clearField();
                        btnSave.Text = "Save";
                        ViewState["id"] = "";
                        btnCancel.Visible = false;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }
        public void clearField()
        {
            txtStore.Text = "";
            txtEmail1.Text = "";
            txtEmail2.Text = "";
            txtEmail3.Text = "";
        }

        protected void grdStore_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                int id = Convert.ToInt32(e.CommandArgument.ToString());
                ViewState["id"] = id;
                var a = SpGetData.GetDataFromSP("GetIdstore", new object[,] { { "id" }, { id } });

                txtStore.Text = Convert.ToString(a.Rows[0]["title"]);
                txtEmail1.Text = Convert.ToString(a.Rows[0]["Email1"]);
                txtEmail2.Text = Convert.ToString(a.Rows[0]["Email2"]);
                txtEmail3.Text = Convert.ToString(a.Rows[0]["Email3"]);
                btnSave.Text = "Update";
                btnCancel.Visible = true;
            }
            //else
            //{
            //    int id = Convert.ToInt32(e.CommandArgument.ToString());
            //    var a = SpGetData.GetDataFromSP("Deletestore", new object[,] { { "id" }, { id } });
            //    BindStore();
            //}
        }

        protected void grdStore_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void grdStore_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            clearField();
            btnSave.Text = "Save";
            btnCancel.Visible = false;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(this.hdnId.Value);
            var a = SpGetData.GetDataFromSP("Deletestore", new object[,] { { "id" }, { id } });
            if (a.Rows.Count > 0)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "NoDelete()", true);
            }
            BindStore();
        }

        //protected void grdStore_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    grdStore.PageIndex = e.NewPageIndex;
        //    grdStore.Page.DataBind();
        //    BindStore();
        //}
        protected void btnMail_Click(object sender, EventArgs e)
        {
            try
            {
                ProcessLog("btnMail_Click", "btnMail_Click", "Store");
                string Email = txttestmail.Text;
                sendmail("Mail Adsress", "Mail Adsress is " + Email + "");
            }
            catch (Exception ex)
            {
                ErrorLog("btnMail_Click", ex.ToString(), "Store");
            }

        }

        public void sendmail(string subject, string str1)
        {
            try
            {
                //string server = System.Configuration.ConfigurationSettings.AppSettings["hostName"];
                //string port = System.Configuration.ConfigurationSettings.AppSettings["port"];
                //string username = System.Configuration.ConfigurationSettings.AppSettings["acctname"];
                //string pwd = System.Configuration.ConfigurationSettings.AppSettings["password"];
                ////System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage("rlogicaltestmail@gmail.com", txttestmail.Text);
                //System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage("noreply@ruomky.com", txttestmail.Text);

                //Email.Subject = subject;
                //Email.Body = str1;
                //System.Net.Mail.SmtpClient mailClient = new System.Net.Mail.SmtpClient();
                //mailClient.EnableSsl = true;
                //mailClient.Host = server;
                //mailClient.Port = Convert.ToInt32(port);
                //Email.IsBodyHtml = true;
                //mailClient.UseDefaultCredentials = false;
                //System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential(username, pwd);
                //mailClient.Credentials = basicAuthenticationInfo;
                //try
                //{
                //    mailClient.Send(Email);
                //    Email = null;
                //    lblid.Text = "Mail Sent Sucessfully";
                //    txttestmail.Text = "";
                //}
                //catch (Exception ex1)
                //{
                //    ErrorLog("sendmail", ex1.ToString(), "Store");
                //}
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient();
                mail.To.Add(txttestmail.Text);
                mail.From = new MailAddress(ConfigurationManager.AppSettings["acctname"]);
                string msgSubject = subject;
                mail.Subject = msgSubject;
                mail.IsBodyHtml = true;
                string msgBody = str1;
                mail.Body = msgBody;
                SmtpServer.Host = ConfigurationManager.AppSettings["hostName"].ToString();
                SmtpServer.Port = Convert.ToInt16(25);
                SmtpServer.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                SmtpServer.EnableSsl = true;
                SmtpServer.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["acctname"].ToString(), ConfigurationManager.AppSettings["password"].ToString());
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                ErrorLog("sendmail", ex.ToString(), "Store");
            }
        }

        public static int ErrorLog(String MethodName, String ErrorMessage, String PageName)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["RashyasConnectionstring"].ConnectionString);
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    con.Open();
                    cmd.CommandText = "ErrorLogSp";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@MethodName", SqlDbType.VarChar, 100).Value = MethodName;
                    cmd.Parameters.Add("@pageName", SqlDbType.VarChar, 2000).Value = PageName;
                    cmd.Parameters.Add("@errorMessage", SqlDbType.VarChar, 2000).Value = ErrorMessage;
                    // cmd.Parameters.Add("@CodeSourceName", SqlDbType.VarChar, 50).Value = _sourceName;
                    // cmd.Parameters.Add("@opt", SqlDbType.Int, 1).Value = 1;
                    int _result = cmd.ExecuteNonQuery();
                    con.Close();
                    return _result;
                }
            }

        }
        public static int ProcessLog(String MethodName, String ProcessName, String PageName)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["RashyasConnectionstring"].ConnectionString);
            //SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["RashyasConnectionstring"]);
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    con.Open();
                    cmd.CommandText = "processLogSp";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@MethodName", SqlDbType.VarChar, 100).Value = MethodName;
                    cmd.Parameters.Add("@ProcessName", SqlDbType.VarChar, 2000).Value = ProcessName;
                    cmd.Parameters.Add("@PageName", SqlDbType.VarChar, 2000).Value = PageName;
                    // cmd.Parameters.Add("@CodeSourceName", SqlDbType.VarChar, 50).Value = _sourceName;
                    // cmd.Parameters.Add("@opt", SqlDbType.Int, 1).Value = 1;
                    int _result = cmd.ExecuteNonQuery();
                    con.Close();
                    return _result;
                }
            }

        }

    }
}