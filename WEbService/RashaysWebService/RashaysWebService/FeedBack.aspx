﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Rashays.Master" AutoEventWireup="true" CodeBehind="FeedBack.aspx.cs" Inherits="RashaysWebService.FeedBack" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet" />
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" />
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../vendors/nprogress/nprogress.js"></script>
        <!-- Chart.js -->
        <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
        <!-- gauge.js -->
        <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
        <!-- bootstrap-progressbar -->

    <style>
        .pagination {
            display: inline-block;
        }

            .pagination a {
                color: black;
                float: left;
                padding: 8px 16px;
                text-decoration: none;
            }

                .pagination a.active {
                    background-color: #4CAF50;
                    color: white;
                }
    </style>
    <script type="text/javascript">
       

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="col-md-12">
        <div class="x_panel">
            <div class="form-horizontal form-label-left">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Select Store
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:DropDownList ID="ddlStore1" runat="server" CssClass="select2_single form-control" OnSelectedIndexChanged="ddlStore1_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>

                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        Select Location
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <asp:DropDownList ID="ddlLocation1" runat="server" CssClass="select2_single form-control" OnSelectedIndexChanged="ddlLocation1_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content ">
                <%--<div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">--%>
                <div id="datatable_paginate" class="dataTables_paginate paging_simple_numbers" style="text-align: left !important;">
                    <asp:GridView ID="grdFeedBack" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="table table-striped" AllowPaging="true" PageSize="10" OnPageIndexChanging="grdFeedBack_PageIndexChanging" >
                        <Columns>
                            <asp:TemplateField HeaderText="id" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblId" runat="server" Text='<%# Eval("id")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Question">
                                <ItemTemplate>
                                    <asp:Label ID="lblQuestion" runat="server" Text='<%# Eval("question")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Feedback">
                                <ItemTemplate>
                                    <asp:Label ID="lblFeedBack" runat="server" Text='<%# Eval("feedBack")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Happy message">
                                <ItemTemplate>
                                    <asp:Label ID="lblHappy" runat="server" Text='<%# Eval("happy")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Neutral Message">
                                <ItemTemplate>
                                    <asp:Label ID="lblNeutral" runat="server" Text='<%# Eval("neutral")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sad Message">
                                <ItemTemplate>
                                    <asp:Label ID="lblSad" runat="server" Text='<%# Eval("sad")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Store">
                                <ItemTemplate>
                                    <asp:Label ID="lblTitle" runat="server" Text='<%# Eval("title")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Location">
                                <ItemTemplate>
                                    <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("location")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TimeSlot">
                                <ItemTemplate>
                                    <asp:Label ID="lblTimeSlot" runat="server" Text='<%# Eval("timeSlotName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date Created">
                                <ItemTemplate>
                                    <asp:Label ID="lbldate" runat="server" Text='<%# Eval("datecreated")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%-- <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:LinkButton ID="btnedit" runat="server" CommandName="Edit" CommandArgument='<%# Eval("id") %>'>Edit</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete">
                    <ItemTemplate>
                        <asp:Button ID="btndelete" runat="server" Text="Delete" CommandName="Delete" CommandArgument='<%# Eval("id") + ";" +Eval("sid")  + ";" +Eval("lid")  + ";" +Eval("tid")  %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Active">
                    <ItemTemplate>
                        <asp:RadioButton ID="RadioButton1" runat="server" GroupName="rbgrp" onclick="RadioCheck(this);" Checked='<%# Eval("checked") %>' OnCheckedChanged="RadioButton1_CheckedChanged" AutoPostBack="true" />
                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#Eval("id")%>' />
                    </ItemTemplate>
                </asp:TemplateField>--%>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <%--</div>--%>
        </div>
    </div>

</asp:Content>
