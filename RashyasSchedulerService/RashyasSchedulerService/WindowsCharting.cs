﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;
namespace RashyasSchedulerService
{
    sealed class WindowsCharting
    {


        //method generates the chart
#pragma warning disable 0628
        protected internal Chart GenerateChart(DataTable dtChartDataSource, int width, int height, string bgColor, int intType, string Mode, string emailId, string storename)
        {

            string[] range = new string[dtChartDataSource.Rows.Count];
            for (int i = 0; i < dtChartDataSource.Rows.Count; i++)
            {
                range[i] = dtChartDataSource.Rows[i]["displaymonth"].ToString();
                //dtChartDataSource.Rows.Add();
            }

            Chart chart = new Chart()
            {
                Width = width,
                Height = height
            };
            chart.Legends.Add(new Legend() { Name = "Legend" });
            chart.Legends[0].Docking = Docking.Bottom;
            //chart.Legends[0].Position = new ElementPosition(0, 0, 100, 20);
            ChartArea chartArea = new ChartArea() { Name = "ChartArea" };
            //Remove X-axis grid lines
            chartArea.AxisX.MajorGrid.LineWidth = 0;

            //chartArea.AxisY.TextOrientation = TextOrientation.Rotated270;

            //chartArea.AxisX.TextOrientation = TextOrientation.Rotated270;

            //Remove Y-axis grid lines
            chartArea.AxisY.MajorGrid.LineWidth = 1;
            chartArea.AxisX.Interval = 1;
            //Chart Area Back Color
            chartArea.BackColor = Color.FromName(bgColor);
            chart.ChartAreas.Add(chartArea);
            chart.Palette = ChartColorPalette.BrightPastel;
            string series = string.Empty;

            //create series and add data points to the series
            if (dtChartDataSource != null)
            {
                foreach (DataColumn dc in dtChartDataSource.Columns)
                {
                    if (dc.ColumnName != "displaymonth")
                    {
                        //a series to the chart
                        if (chart.Series.FindByName(dc.ColumnName) == null)
                        {
                            //if (dc.ColumnName != "displaymonth")
                            //{
                            series = dc.ColumnName;
                            chart.Series.Add(series);
                            chart.Series[series].ChartType = (SeriesChartType)intType;
                            //}
                        }
                        //Add data points to the series
                        int counter = 0;
                        foreach (DataRow dr in dtChartDataSource.Rows)
                        {
                            double dataPoint = 0;
                            double.TryParse(dr[dc.ColumnName].ToString(), out dataPoint);
                            string ArrrayVal = range[counter];
                            DataPoint objDataPoint = new DataPoint() { AxisLabel = ArrrayVal, YValues = new double[] { dataPoint } };
                            chart.Series[series].Points.Add(objDataPoint);
                            counter++;
                        }
                    }
                }
            }

            //MemoryStream ms = new MemoryStream();
            //chart.SaveImage(ms, ChartImageFormat.Png);
            //Byte[] bytesArray = ms.GetBuffer();
            //string base64String = Convert.ToBase64String(bytesArray, 0, bytesArray.Length);
            //string streamImage = "data:image/png;base64," + base64String;
            string path = @DateTime.Now.ToShortDateString().Replace("/", "_") + ".jpg";
            chart.SaveImage(path, ChartImageFormat.Jpeg);


            try
            {
                if (Mode == "TIME_DATA")
                {
                    sendmailtest("Feedback Results - " + storename + "", path, emailId, storename);
                }
                if (Mode == "WEEKLY_DATA")
                {
                    sendmailtest("Feedback Results - " + storename + "", path, emailId, storename);
                }
                if (Mode == "MONTH_DATA")
                {
                    sendmailtest("Feedback Results - " + storename + "", path, emailId, storename);
                }


            }
            catch (Exception)
            {

                throw;
            }

            if (Mode == "MONTH_DATA")
            {
                chart.ChartAreas[0].AxisX.LabelStyle.Angle = -90;
            }
            return chart;
        }

        public static string FixBase64ForImage(string Image)
        {
            System.Text.StringBuilder sbText = new System.Text.StringBuilder(Image, Image.Length);
            sbText.Replace("\r\n", string.Empty); sbText.Replace(" ", string.Empty);
            return sbText.ToString();
        }

        public static List<Count> GetcountDatatimewise(int timeId, int storeId, int questionId)
        {

            List<Count> list = new List<Count>();
            var a = SpGetData.GetDataFromSP("ChartGetCountTimewise", new object[,] { { "timeId", "storeId", "questionId" }, { timeId, storeId, questionId } });
            //DataTable dt = a;
            if (a.Rows.Count > 0)
            {
                List<Count> objcount = new List<Count>();
                string[] columnNames = a.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToArray();
                if (columnNames[1] == "displayhour")
                {
                    foreach (DataRow dr in a.Rows)
                    {
                        Count objCount1 = new Count();
                        objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                        objCount1.displayday = Convert.ToString(dr[1].ToString());
                        objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                        list.Add(objCount1);
                    }
                }
                else if (columnNames[1] == "displayyear")
                {
                    foreach (DataRow dr in a.Rows)
                    {
                        Count objCount1 = new Count();
                        objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                        objCount1.displayday = Convert.ToString(dr[1].ToString());
                        objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                        list.Add(objCount1);

                        DataRow[] drmain = a.Select("displayyear='" + objCount1.displayday + "'");
                        objcount = list.Where(x => x.displayday == objCount1.displayday).ToList();
                        objcount.Count();
                        int listcount = objcount.Count();
                        DataRow[] dr1 = a.Select("displayyear='" + objCount1.displayday + "' and feedback = '1'");
                        if (dr1.Length == 0)
                        {
                            object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                            if (obj != null)
                            {
                                // continue;
                                if (drmain.Length == listcount)
                                {
                                    Count objCount0 = new Count();
                                    objCount0.displayCnt = 0;
                                    objCount0.displayday = Convert.ToString(dr[1].ToString());
                                    objCount0.feedback = 1;
                                    list.Add(objCount0);
                                }
                            }

                        }
                        DataRow[] dr2 = a.Select("displayyear='" + objCount1.displayday + "' and feedback = '2'");
                        if (dr2.Length == 0)
                        {
                            object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                            if (obj != null)
                            {
                                //continue;
                                if (drmain.Length == listcount)
                                {
                                    Count objCount2 = new Count();
                                    objCount2.displayCnt = 0;
                                    objCount2.displayday = Convert.ToString(dr[1].ToString());
                                    objCount2.feedback = 2;
                                    list.Add(objCount2);
                                }
                            }

                        }
                        DataRow[] dr3 = a.Select("displayyear='" + objCount1.displayday + "' and feedback = '3'");
                        if (dr3.Length == 0)
                        {
                            object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                            // if(objcnt)
                            if (obj != null)
                            {
                                //continue;
                                if (drmain.Length == listcount)
                                {
                                    Count objCount3 = new Count();
                                    objCount3.displayCnt = 0;
                                    objCount3.displayday = Convert.ToString(dr[1].ToString());
                                    objCount3.feedback = 3;
                                    list.Add(objCount3);
                                }
                            }


                        }
                    }
                }
                else
                {
                    if (timeId == 3)
                    {
                        try
                        {
                            foreach (DataRow dr in a.Rows)
                            {
                                try
                                {
                                    Count objCount1 = new Count();
                                    objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                                    string displayd = Convert.ToString(dr[3].ToString());
                                    if (displayd.Contains(":"))
                                    {
                                        displayd = displayd.Substring(0, displayd.IndexOf(":"));
                                        displayd = displayd.Substring(0, displayd.Length - 2).Trim();
                                    }
                                    objCount1.displayday = displayd;
                                    objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                                    list.Add(objCount1);

                                    DataRow[] drmain = a.Select("displayday='" + objCount1.displayday + "'");
                                    objcount = list.Where(x => x.displayday == objCount1.displayday).ToList();
                                    objcount.Count();
                                    int listcount = objcount.Count();
                                    DataRow[] dr1 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '1'");
                                    if (dr1.Length == 0)
                                    {
                                        object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                                        if (obj != null)
                                        {
                                            // continue;
                                            if (drmain.Length == listcount)
                                            {
                                                Count objCount0 = new Count();
                                                objCount0.displayCnt = 0;
                                                string displayd1 = Convert.ToString(dr[3].ToString());
                                                if (displayd1.Contains(":"))
                                                {
                                                    displayd1 = displayd1.Substring(0, displayd1.IndexOf(":"));
                                                    displayd1 = displayd1.Substring(0, displayd1.Length - 2).Trim();
                                                }
                                                objCount0.displayday = displayd1;
                                                objCount0.feedback = 1;
                                                list.Add(objCount0);
                                            }
                                        }

                                    }
                                    DataRow[] dr2 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '2'");
                                    if (dr2.Length == 0)
                                    {
                                        object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                                        if (obj != null)
                                        {
                                            //continue;
                                            if (drmain.Length == listcount)
                                            {
                                                Count objCount2 = new Count();
                                                objCount2.displayCnt = 0;
                                                string displayd1 = Convert.ToString(dr[3].ToString());
                                                if (displayd1.Contains(":"))
                                                {
                                                    displayd1 = displayd1.Substring(0, displayd1.IndexOf(":"));
                                                    displayd1 = displayd1.Substring(0, displayd1.Length - 2).Trim();
                                                }
                                                objCount2.displayday = displayd1;
                                                objCount2.feedback = 2;
                                                list.Add(objCount2);
                                            }
                                        }

                                    }
                                    DataRow[] dr3 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '3'");
                                    if (dr3.Length == 0)
                                    {
                                        object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                                        // if(objcnt)
                                        if (obj != null)
                                        {
                                            //continue;
                                            if (drmain.Length == listcount)
                                            {
                                                Count objCount3 = new Count();
                                                objCount3.displayCnt = 0;
                                                string displayd1 = Convert.ToString(dr[3].ToString());
                                                if (displayd1.Contains(":"))
                                                {
                                                    displayd1 = displayd1.Substring(0, displayd1.IndexOf(":"));
                                                    displayd1 = displayd1.Substring(0, displayd1.Length - 2).Trim();
                                                }
                                                objCount3.displayday = displayd1;
                                                objCount3.feedback = 3;
                                                list.Add(objCount3);
                                            }
                                        }


                                    }

                                }
                                catch (Exception)
                                {
                                }

                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    else
                    {
                        foreach (DataRow dr in a.Rows)
                        {
                            Count objCount1 = new Count();
                            objCount1.displayCnt = Convert.ToInt32(dr[0].ToString());
                            string displayd = Convert.ToString(dr[1].ToString());
                            if (displayd.Contains(":"))
                            {
                                displayd = displayd.Substring(0, displayd.IndexOf(":"));
                                displayd = displayd.Substring(0, displayd.Length - 1).Trim();
                            }
                            objCount1.displayday = displayd;
                            objCount1.feedback = Convert.ToInt32(dr[2].ToString());
                            list.Add(objCount1);

                            DataRow[] drmain = a.Select("displayday='" + objCount1.displayday + "'");
                            objcount = list.Where(x => x.displayday == objCount1.displayday).ToList();
                            objcount.Count();
                            int listcount = objcount.Count();
                            DataRow[] dr1 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '1'");
                            if (dr1.Length == 0)
                            {
                                object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                                if (obj != null)
                                {
                                    // continue;
                                    if (drmain.Length == listcount)
                                    {
                                        Count objCount0 = new Count();
                                        objCount0.displayCnt = 0;
                                        string displayd1 = Convert.ToString(dr[1].ToString());
                                        if (displayd1.Contains(":"))
                                        {
                                            displayd1 = displayd1.Substring(0, displayd1.IndexOf(":"));
                                            displayd1 = displayd1.Substring(0, displayd1.Length - 1).Trim();
                                        }
                                        objCount0.displayday = displayd1;
                                        objCount0.feedback = 1;
                                        list.Add(objCount0);
                                    }
                                }

                            }
                            DataRow[] dr2 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '2'");
                            if (dr2.Length == 0)
                            {
                                object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                                if (obj != null)
                                {
                                    //continue;
                                    if (drmain.Length == listcount)
                                    {
                                        Count objCount2 = new Count();
                                        objCount2.displayCnt = 0;
                                        string displayd1 = Convert.ToString(dr[1].ToString());
                                        if (displayd1.Contains(":"))
                                        {
                                            displayd1 = displayd1.Substring(0, displayd1.IndexOf(":"));
                                            displayd1 = displayd1.Substring(0, displayd1.Length - 1).Trim();
                                        }
                                        objCount2.displayday = displayd1;
                                        objCount2.feedback = 2;
                                        list.Add(objCount2);
                                    }
                                }

                            }
                            DataRow[] dr3 = a.Select("displayday='" + objCount1.displayday + "' and feedback = '3'");
                            if (dr3.Length == 0)
                            {
                                object obj = list.Where(x => x.displayday == objCount1.displayday && x.feedback == objCount1.feedback).FirstOrDefault();
                                // if(objcnt)
                                if (obj != null)
                                {
                                    //continue;
                                    if (drmain.Length == listcount)
                                    {
                                        Count objCount3 = new Count();
                                        objCount3.displayCnt = 0;
                                        string displayd1 = Convert.ToString(dr[1].ToString());
                                        if (displayd1.Contains(":"))
                                        {
                                            displayd1 = displayd1.Substring(0, displayd1.IndexOf(":"));
                                            displayd1 = displayd1.Substring(0, displayd1.Length - 1).Trim();
                                        }
                                        objCount3.displayday = displayd1;
                                        objCount3.feedback = 3;
                                        list.Add(objCount3);
                                    }
                                }


                            }

                        }
                    }
                }
            }
            return list;
        }

        private void changeYScala(object chart)
        {
            double max = Double.MinValue;
            double min = Double.MaxValue;

            Chart tmpChart = (Chart)chart;

            double leftLimit = tmpChart.ChartAreas[0].AxisX.Minimum;
            double rightLimit = tmpChart.ChartAreas[0].AxisX.Maximum;

            for (int s = 0; s < tmpChart.Series.Count(); s++)
            {
                foreach (DataPoint dp in tmpChart.Series[s].Points)
                {
                    if (dp.XValue >= leftLimit && dp.XValue <= rightLimit)
                    {
                        min = Math.Min(min, dp.YValues[0]);
                        max = Math.Max(max, dp.YValues[0]);
                    }
                }
            }
            //tmpChart.ChartAreas[0].AxisY.Maximum = max;
            tmpChart.ChartAreas[0].AxisY.Maximum = (Math.Ceiling((max / 10)) * 10);
            tmpChart.ChartAreas[0].AxisY.Minimum = (Math.Floor((min / 10)) * 10);
            //tmpChart.ChartAreas[0].AxisY.Minimum = min;
        }

        public void sendmailtest(string subject, string path, string emailId, string storename)
        {
            try
            {
                string server = System.Configuration.ConfigurationSettings.AppSettings["hostName"];
                string port = System.Configuration.ConfigurationSettings.AppSettings["port"];
                string username = System.Configuration.ConfigurationSettings.AppSettings["acctname"];
                string pwd = System.Configuration.ConfigurationSettings.AppSettings["password"];
                //System.Net.Mail.MailMessage Email = new System.Net.Mail.MailMessage("rlogicaltestmail@gmail.com", "rlogicaltestmail@gmail.com");
                //string from = "rlogicaltestmail@gmail.com";
                string from = "noreply@rashays.com";
                using (MailMessage Email = new MailMessage())
                {
                    Email.From = new MailAddress(from);
                    foreach (var address in emailId.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        Email.To.Add(address);
                    }

                    Email.IsBodyHtml = true;
                    Email.AlternateViews.Add(Mail_Body(storename));
                    Email.Subject = subject;
                    System.Net.Mail.SmtpClient mailClient = new System.Net.Mail.SmtpClient();
                    mailClient.EnableSsl = true;
                    mailClient.Host = server;
                    mailClient.Port = Convert.ToInt32(port);
                    Email.BodyEncoding = Encoding.Default;
                    Email.Priority = MailPriority.High;
                    mailClient.UseDefaultCredentials = false;
                    System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential(username, pwd);
                    mailClient.Credentials = basicAuthenticationInfo;
                    try
                    {
                        mailClient.Send(Email);
                    }
                    catch (Exception ex1)
                    {
                        ErrorLog("sendmail mailClient", ex1.ToString(), "RashyasSchedulerService");
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog("sendmail", ex.ToString(), "RashyasSchedulerService");
            }
        }

        private AlternateView Mail_Body(string storename)
        {
            string path = @DateTime.Now.ToShortDateString().Replace("/", "_") + ".jpg";
            LinkedResource Img = new LinkedResource(path, MediaTypeNames.Image.Jpeg);
            //string msgbody = "please find attachment for store " + storename + "";
            Img.ContentId = "MyImage";
            string str = @"  
                <table>  
                     
                    <tr>  
                        <td>  
                          <img src=cid:MyImage  id='img' alt='' width='100px' height='100px'/>   
                        </td>  
                    </tr></table>  
                ";
            AlternateView AV =
            AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            return AV;
        }
        public static int ErrorLog(String MethodName, String ErrorMessage, String PageName)
        {
            SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["RashyasConnectionstring"]);
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    con.Open();
                    cmd.CommandText = "ErrorLogSp";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@MethodName", SqlDbType.VarChar, 100).Value = MethodName;
                    cmd.Parameters.Add("@pageName", SqlDbType.VarChar, 2000).Value = PageName;
                    cmd.Parameters.Add("@errorMessage", SqlDbType.VarChar, 2000).Value = ErrorMessage;
                    // cmd.Parameters.Add("@CodeSourceName", SqlDbType.VarChar, 50).Value = _sourceName;
                    // cmd.Parameters.Add("@opt", SqlDbType.Int, 1).Value = 1;
                    int _result = cmd.ExecuteNonQuery();
                    con.Close();
                    return _result;
                }
            }

        }
    }
}
