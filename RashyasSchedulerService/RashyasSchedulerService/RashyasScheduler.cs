﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;


namespace RashyasSchedulerService
{
    partial class RashyasScheduler : ServiceBase
    {
        public RashyasScheduler()
        {
            InitializeComponent();
        }
        private System.Threading.Thread _thread;
        protected override void OnStart(string[] args)
        {
            try
            {
                // TODO: Add code here to start your service.
                _thread = new System.Threading.Thread(DoWork);

                // Start the thread.
                _thread.Start();
                EventLog.WriteEntry("Successful start.", EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(ex.ToString());
            }
        }
        public void DoWork()
        {
            try
            {
                SetTimer();
                timer2.Enabled = true;
            }
            catch (Exception ee)
            {
                EventLog.WriteEntry(ee.ToString());
            }
        }

        public void SetTimer()
        {
            int msUntilTwelve = 50000;
            timer2.Interval = msUntilTwelve;
        }
        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
        }

        private void timer2_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timer2.Enabled = false;
            GetScheduleReport();
            SetTimer();
            timer2.Enabled = true;
        }

        public void GetScheduleReport()
        {
            var a = SpGetData.GetDataFromSP("GetScheduleData", new object[,] { { }, { } });
            string Mode = "";
            for (int i = 0; i < a.Rows.Count; i++)
            {
                try
                {
                    string StoreId = Convert.ToString(a.Rows[i]["storeid"]);
                    DateTime insertedDate = Convert.ToDateTime(a.Rows[i]["datetime"]);
                    string frequency = Convert.ToString(a.Rows[i]["frequency"]);
                    string repeats = Convert.ToString(a.Rows[i]["repeats"]);
                    string runTime = Convert.ToString(a.Rows[i]["runtime"]);
                    DateTime currentDate = DateTime.Now;
                    string runTimenow = currentDate.ToString("hh:mm tt");
                    string emailId = Convert.ToString(a.Rows[i]["EmailId"]);
                    if (frequency == "Daily")
                    {
                        try
                        {
                            if (runTime == runTimenow)
                            {
                                Mode = "TIME_DATA";
                                var feedbackDaily = new DataTable();
                                feedbackDaily = SpGetData.GetDataFromSP("GetScheduleFeedbackdata", new object[,] { { "Param", "StoreId" }, { -1, StoreId } });

                                if (feedbackDaily.Rows.Count > 0)
                                {
                                    GetChart(Mode, emailId, StoreId, Convert.ToString(feedbackDaily.Rows[0]["store"]));
                                    //  sendmailtest("Daily Schedule", strDaily, emailId);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorLog("GetScheduleReport Daily", ex.ToString(), "RashyasSchedulerService");
                        }

                    }
                    else if (frequency == "Weekly")
                    {
                        try
                        {
                            if (runTime == runTimenow)
                            {
                                string strWeekly = "";
                                var CheckWeekly = SpGetData.GetDataFromSP("WeeklyMail", new object[,] { { "insertedDate", "currentDate" }, { insertedDate, currentDate } });
                                var feedbackweekly = new DataTable();
                                if (Convert.ToString(CheckWeekly.Rows[0]["Column1"]) == "Yes")
                                {
                                    feedbackweekly = SpGetData.GetDataFromSP("GetScheduleFeedbackdata", new object[,] { { "Param", "StoreId" }, { -7, StoreId } });
                                    if (feedbackweekly.Rows.Count > 0)
                                    {
                                        GetChart(Mode, emailId, StoreId, Convert.ToString(feedbackweekly.Rows[0]["store"]));
                                        //  sendmailtest("Daily Schedule", strDaily, emailId);
                                    }
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorLog("GetScheduleReport Weekly", ex.ToString(), "RashyasSchedulerService");
                        }
                    }
                    else
                    {
                        try
                        {
                            if (runTime == runTimenow)
                            {
                                string strMonthly = "";
                                var feedbackMonthly = new DataTable();
                                string CDate = currentDate.ToString("d");
                                //DateTime dt = DateTime.ParseExact(CDate, "MM-dd-yyyy", CultureInfo.InvariantCulture);
                                //IFormatProvider provider = new System.Globalization.CultureInfo("en-US", true);
                                //DateTime eventdate = DateTime.Parse(CDate, provider, System.Globalization.DateTimeStyles.NoCurrentDateDefault);
                                string inDAte = "";
                                if (CDate.Contains("-"))
                                {
                                    CDate = CDate.Substring(0, CDate.LastIndexOf("-"));
                                    CDate = CDate.Substring(CDate.LastIndexOf("-") + 1);


                                    inDAte = insertedDate.ToString("d");
                                    inDAte = inDAte.Substring(0, inDAte.LastIndexOf("-"));
                                    inDAte = inDAte.Substring(inDAte.LastIndexOf("-") + 1);
                                }
                                else
                                {
                                    CDate = CDate.Substring(0, CDate.LastIndexOf("/"));
                                    CDate = CDate.Substring(CDate.LastIndexOf("/") + 1);


                                    inDAte = insertedDate.ToString("d");
                                    inDAte = inDAte.Substring(0, inDAte.LastIndexOf("/"));
                                    inDAte = inDAte.Substring(inDAte.LastIndexOf("/") + 1);
                                }

                                if (inDAte == CDate)
                                {
                                    feedbackMonthly = SpGetData.GetDataFromSP("GetScheduleFeedbackdata", new object[,] { { "Param", "StoreId" }, { -30, StoreId } });
                                    if (feedbackMonthly.Rows.Count > 0)
                                    {
                                        GetChart(Mode, emailId, StoreId, Convert.ToString(feedbackMonthly.Rows[0]["store"]));
                                        //  sendmailtest("Daily Schedule", strDaily, emailId);
                                    }
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorLog("GetScheduleReport Monthly", ex.ToString(), "RashyasSchedulerService");
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLog("GetScheduleReport", ex.ToString(), "RashyasSchedulerService");
                }

            }

        }
        public static int ErrorLog(String MethodName, String ErrorMessage, String PageName)
        {
            SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["RashyasConnectionstring"]);
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    con.Open();
                    cmd.CommandText = "ErrorLogSp";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@MethodName", SqlDbType.VarChar, 100).Value = MethodName;
                    cmd.Parameters.Add("@pageName", SqlDbType.VarChar, 2000).Value = PageName;
                    cmd.Parameters.Add("@errorMessage", SqlDbType.VarChar, 2000).Value = ErrorMessage;
                    // cmd.Parameters.Add("@CodeSourceName", SqlDbType.VarChar, 50).Value = _sourceName;
                    // cmd.Parameters.Add("@opt", SqlDbType.Int, 1).Value = 1;
                    int _result = cmd.ExecuteNonQuery();
                    con.Close();
                    return _result;
                }
            }

        }
        public static int ProcessLog(String MethodName, String ProcessName, String PageName)
        {
            SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["RashyasConnectionstring"]);
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    con.Open();
                    cmd.CommandText = "processLogSp";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@MethodName", SqlDbType.VarChar, 100).Value = MethodName;
                    cmd.Parameters.Add("@ProcessName", SqlDbType.VarChar, 2000).Value = ProcessName;
                    cmd.Parameters.Add("@PageName", SqlDbType.VarChar, 2000).Value = PageName;
                    // cmd.Parameters.Add("@CodeSourceName", SqlDbType.VarChar, 50).Value = _sourceName;
                    // cmd.Parameters.Add("@opt", SqlDbType.Int, 1).Value = 1;
                    int _result = cmd.ExecuteNonQuery();
                    con.Close();
                    return _result;
                }
            }

        }

        private Chart GetChart(string Mode, string emailId, string StoreId, string storename)
        {
            //if (comboBox1.SelectedIndex != -1)
            //{

            Chart chart = null;

            try
            {
                int timeId = 4;
                int questionId = -1;

                DataTable objDt = SpGetData.GetDataFromSP("spGetFeedbackChartData", new object[,] { { "mode", "timeId", "storeId", "questionId" }, { Mode, timeId, StoreId, questionId } });

                // pnChart.Controls.Clear();
                WindowsCharting charting = new WindowsCharting();
                DataTable dt = new DataTable();
                string[] seariesArray = { "happy", "sad", "Neutral" };

                chart = charting.GenerateChart(objDt, 500, 500, "", 11, Mode, emailId, storename);
                //return chart;
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                //do nothing
            }
            return chart;
            //}
            //else
            //{
            //    //MessageBox.Show("please select schedule type");
            //    return null;
            //}
        }

    }
}
